-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 21, 2017 at 08:40 AM
-- Server version: 5.5.57-0+deb8u1
-- PHP Version: 5.6.30-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `marvin`
--


DROP FUNCTION IF EXISTS `CLEANUP`;
DROP FUNCTION IF EXISTS `GET_HEAD_REVISION`;
DROP FUNCTION IF EXISTS `GET_NEXT_ID`;
DROP FUNCTION IF EXISTS `GET_LAST_CREATED_ID`;
DROP FUNCTION IF EXISTS `REVISION_HEAD_P`;


DELIMITER $$
--
-- Functions
--
CREATE DEFINER=`marvin`@`localhost` FUNCTION `CLEANUP`() 
RETURNS int(11)
DETERMINISTIC MODIFIES SQL DATA
BEGIN
  DELETE FROM `LangzeitSpeicher` WHERE `REVISION` > 1 and `DING-ID` >= 0;
DELETE FROM `LangzeitSpeicher` 
WHERE
    `DING-ID` > 999;
UPDATE `marvin`.`_OPTIONS_` 
SET 
    `value` = '1000'
WHERE
    `parameter` = 'NEXT-ID';

RETURN 1;
END$$

CREATE DEFINER=`marvin`@`localhost` FUNCTION `GET_HEAD_REVISION` (`ID` INT UNSIGNED)
RETURNS INT(10) UNSIGNED
NOT DETERMINISTIC READS SQL DATA
BEGIN
  DECLARE head_revision INT;
  SET head_revision = 1;
  
  SELECT `REVISION` FROM `LangzeitSpeicher` WHERE `DING-ID` = ID ORDER BY `REVISION` DESC LIMIT 1 INTO head_revision;
   
  RETURN head_revision;
END$$

CREATE DEFINER=`marvin`@`localhost` FUNCTION `GET_NEXT_ID` ()
RETURNS INT(10) UNSIGNED 
NOT DETERMINISTIC READS SQL DATA # actually MODIFIES SQL DATA, but there is a bug in MySQL #93807
BEGIN
  DECLARE next_id INT;
  SET next_id = 0;

  SELECT `value` from `_OPTIONS_` where `parameter` = 'NEXT-ID' into next_id;

  UPDATE `_OPTIONS_` SET `value` = CAST((1 + next_id) AS CHAR) where `parameter` = 'NEXT-ID';

  RETURN next_id;
END$$

CREATE DEFINER=`marvin`@`localhost` FUNCTION `GET_LAST_CREATED_ID` ()
RETURNS INT(10) UNSIGNED 
NOT DETERMINISTIC READS SQL DATA
BEGIN
  DECLARE next_id INT;
  SET next_id = 0;

  SELECT `value` from `_OPTIONS_` where `parameter` = 'NEXT-ID' into next_id;

  RETURN next_id - 1;
END$$

CREATE DEFINER=`marvin`@`localhost` FUNCTION `REVISION_HEAD_P` (`ID` INT UNSIGNED, `REV` INT UNSIGNED) 
RETURNS TINYINT(4)
NOT DETERMINISTIC
READS SQL DATA
BEGIN
  DECLARE head_revision INT;
  DECLARE revision_count INT;
  SET head_revision = -1;
  SET revision_count = 0;
  
  SELECT `REVISION`, count(`REVISION`) FROM `LangzeitSpeicher` WHERE `DING-ID` = ID INTO head_revision, revision_count;
   
  RETURN ((revision_count > 0) and (head_revision = REV));
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `LangzeitSpeicher`
--

DROP TABLE IF EXISTS `LangzeitSpeicher`;
CREATE TABLE `LangzeitSpeicher` (
  `DING-ID` bigint(20) UNSIGNED NOT NULL COMMENT 'The ID of the Ding',
  `ÄNDERUNGSDATUM` int(10) UNSIGNED NOT NULL COMMENT 'Timestamp of this modification',
  `P-LIST` mediumtext NOT NULL COMMENT 'The content of the Ding''s P-List',
  `REVISION` int(11) DEFAULT 1 COMMENT 'The number of this revision',
  `KLASSE` bigint(20) NOT NULL COMMENT 'The class of this Ding'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Marvin''s LangzeitSpeicher';

--
-- Dumping data for table `LangzeitSpeicher`
--

INSERT INTO `LangzeitSpeicher` (`DING-ID`, `ÄNDERUNGSDATUM`, `P-LIST`, `REVISION`, `KLASSE`) VALUES
(0,3712141817,'<#676:HEAD?,"Ding">',1,-1),
(1,3712141817,'<":DEVICE-ID",1,#676:HEAD?,#677:HEAD?,":PERSISTENT",T>',1,0),
(2,3712141817,'<#676:HEAD?,"Frame">',1,1),
(3,3712141817,'<#676:HEAD?,"K-Line">',1,1),
(7,3712141817,'<#676:HEAD?,"PluginBase">',1,1),
(8,3712141817,'<#676:HEAD?,"Plugin-Response">',1,1),
(9,3712141817,'<#676:HEAD?,"Action">',1,1),
(10,3712141817,'<#676:HEAD?,"Wort">',1,1),

(676,3712157782,'<":GRAPHIE","Name">',1,10),
(677,3712157782,'<":GRAPHIE","Marvin">',1,10),
(678,3712157782,'<":GRAPHIE","Beschreibung">',1,10),
(679,3712157782,'<":GRAPHIE","Einheit">',1,10),

-- All of these should be created by the Componentcommunication Plugin duration installation
(4,3712141817,'<#676:HEAD?,"Device">',1,1),
(5,3712141817,'<#676:HEAD?,"Device-Message">',1,1),
(6,3712141817,'<#676:HEAD?,"Device-Service">',1,1),
(408,3712157782,'<":DEVICE-MESSAGE-ID",0>',1,5),
(409,3712157782,'<":DEVICE-MESSAGE-ID",1>',1,5),
(410,3712157782,'<":DEVICE-MESSAGE-ID",10>',1,5),
(411,3712157782,'<":DEVICE-MESSAGE-ID",11>',1,5),
(412,3712157782,'<":DEVICE-MESSAGE-ID",31>',1,5),
(413,3712157782,'<":DEVICE-MESSAGE-ID",32>',1,5),
(414,3712157782,'<":DEVICE-MESSAGE-ID",208>',1,5),
(415,3712157782,'<":DEVICE-MESSAGE-ID",209>',1,5),
(416,3712157770,'<":DEVICE-MESSAGE-ID",210>',1,5),
(417,3712157782,'<":DEVICE-MESSAGE-ID",220,#676:HEAD?,"CO2">',1,5),
(418,3712157782,'<":DEVICE-MESSAGE-ID",221,#676:HEAD?,"Rauch">',1,5),
(419,3712157782,'<":DEVICE-MESSAGE-ID",230,#676:HEAD?,"Bewegung">',1,5),
(420,3712157782,'<":DEVICE-MESSAGE-ID",240,#676:HEAD?,"Schliesskontakt">',1,5),
(421,3712157782,'<":DEVICE-MESSAGE-ID",252>',1,5),
(422,3712157782,'<":DEVICE-MESSAGE-ID",540,#676:HEAD?,"Spannung",#679:HEAD?,"V">',1,5),
(423,3712157782,'<":DEVICE-MESSAGE-ID",541,#676:HEAD?,"Strom",#679:HEAD?,"A">',1,5),
(424,3712157782,'<":DEVICE-MESSAGE-ID",542,#676:HEAD?,"Leistung",#679:HEAD?,"W">',1,5),
(425,3712157782,'<":DEVICE-MESSAGE-ID",543,#676:HEAD?,"Komplexe Leistung",#679:HEAD?,"VA">',1,5),
(426,3712157782,'<":DEVICE-MESSAGE-ID",544,#676:HEAD?,"Scheinleistung",#679:HEAD?,"W">',1,5),
(427,3712157782,'<":DEVICE-MESSAGE-ID",545,#676:HEAD?,"Leistungsfaktor",#679:HEAD?,"%">',1,5),
(428,3712157782,'<":DEVICE-MESSAGE-ID",546,#676:HEAD?,"Sinusverzerrung">',1,5),
(450,3712157775,'<":DEVICE-MESSAGE-ID",547,#676:HEAD?,"Relais">',1,5),
(429,3712157775,'<":DEVICE-MESSAGE-ID",560,#676:HEAD?,"Temperatur",#679:HEAD?,"°C">',1,5),
(430,3712157780,'<":DEVICE-MESSAGE-ID",561,#676:HEAD?,"Feuchtigkeit",#679:HEAD?,"%RH">',1,5),
(431,3712157782,'<":DEVICE-MESSAGE-ID",562,#676:HEAD?,"Helligkeit">',1,5),
(432,3712157782,'<":DEVICE-MESSAGE-ID",563,#676:HEAD?,"Luftdruck">',1,5),
(433,3712157782,'<":DEVICE-MESSAGE-ID",564,#676:HEAD?,"Lusftstaubkonzentration">',1,5),
(434,3712157782,'<":DEVICE-MESSAGE-ID",565,#676:HEAD?,"Luftqualität">',1,5),
(435,3712157782,'<":DEVICE-MESSAGE-ID",566,#676:HEAD?,"Windgeschwindigkeit",#679:HEAD?,"m/s">',1,5),
(436,3712157782,'<":DEVICE-MESSAGE-ID",567,#676:HEAD?,"Windrichtung",#679:HEAD?,"°">',1,5),
(437,3712157782,'<":DEVICE-MESSAGE-ID",568,#676:HEAD?,"Regenintensität",#679:HEAD?,"mm/h">',1,5),
(438,3712157782,'<":DEVICE-MESSAGE-ID",580,#676:HEAD?,"Zustand">',1,5),
(439,3712157782,'<":DEVICE-MESSAGE-ID",600,#676:HEAD?,"Stromquelle">',1,5),
(440,3712157782,'<":DEVICE-MESSAGE-ID",610,#676:HEAD?,"Intensität",#679:HEAD?,"%">',1,5),
(441,3712157782,'<":DEVICE-MESSAGE-ID",611,#676:HEAD?,"Intensität Rot",#679:HEAD?,"%">',1,5),
(442,3712157782,'<":DEVICE-MESSAGE-ID",612,#676:HEAD?,"Intensität Grün",#679:HEAD?,"%">',1,5),
(443,3712157782,'<":DEVICE-MESSAGE-ID",613,#676:HEAD?,"Intensität Blau",#679:HEAD?,"%">',1,5),
(444,3712157782,'<":DEVICE-MESSAGE-ID",620,#676:HEAD?,"Batteriestatus">',1,5),
(445,3712157782,'<":DEVICE-MESSAGE-ID",621,#676:HEAD?,"Batterieladung",#679:HEAD?,"%">',1,5),
(446,3712157782,'<":DEVICE-MESSAGE-ID",700>',1,5),
(447,3712157782,'<":DEVICE-MESSAGE-ID",701,#676:HEAD?,"Lautstärke">',1,5),
(448,3712157782,'<":DEVICE-MESSAGE-ID",710,#676:HEAD?,"IR Befehl">',1,5),
(449,3712157782,'<":DEVICE-MESSAGE-ID",870,#676:HEAD?,"Laufzeit">',1,5),
(460,3712157782,'<":DEVICE-SERVICE-ID",0>',1,6),
(461,3712157782,'<":DEVICE-SERVICE-ID",1>',1,6),
(462,3712157782,'<":DEVICE-SERVICE-ID",2>',1,6),
(463,3712157782,'<":DEVICE-SERVICE-ID",3>',1,6),
(464,3712157782,'<":DEVICE-SERVICE-ID",4>',1,6),

-- These should be created by the system setup
(452,3712157722,'<":DEVICE-SN",3036884,":DEVICE-ID",13,#676:HEAD?,"DEV013",#678:HEAD?,"Sensor Schlaffzimmer",#5:HEAD?,<#419:HEAD? #429:HEAD? #430:HEAD? #431:HEAD?>>',1,4),
(453,3712157722,'<":DEVICE-SN",3036409,":DEVICE-ID",14,#676:HEAD?,"DEV014",#678:HEAD?,"Sensor Badezimmer",#5:HEAD?,<#419:HEAD? #429:HEAD? #430:HEAD? #431:HEAD?>>',1,4),
(454,3712157722,'<":DEVICE-SN",3035091,":DEVICE-ID",15,#676:HEAD?,"DEV015",#678:HEAD?,"Sensor Küche",#5:HEAD?,<#419:HEAD? #429:HEAD? #430:HEAD? #431:HEAD?>>',1,4),
(455,3712157722,'<":DEVICE-SN",3034901,":DEVICE-ID",16,#676:HEAD?,"DEV016",#678:HEAD?,"Sensor Wohnzimmer",#5:HEAD?,<#419:HEAD? #429:HEAD? #430:HEAD? #431:HEAD?>>',1,4),
(456,3712157722,'<":DEVICE-SN",3037310,":DEVICE-ID",17,#676:HEAD?,"DEV017",#678:HEAD?,"Sensor Hall",#5:HEAD?,<#419:HEAD? #429:HEAD? #430:HEAD? #431:HEAD?>>',1,4),
(457,3712157722,'<":DEVICE-SN",0000000,":DEVICE-ID",18,#676:HEAD?,"DEV018",#678:HEAD?,"Sensor Luftqualität",#5:HEAD?,<#419:HEAD? #429:HEAD? #432:HEAD? #433:HEAD? #434:HEAD?>>',1,4),
(458,3712157722,'<":DEVICE-SN",13309637,":DEVICE-ID",20,#676:HEAD?,"DEV020",#678:HEAD?,"Switch Küche",#5:HEAD?,<#438:HEAD? #450:HEAD?>>',1,4),

-- These should be created be the language-processor plugin 
(600,3712157782,'<#676:HEAD?,"Wortklasse">',1,1),
(667,3712157782,'<#676:HEAD?,"Adjektiv">',1,600),
(668,3712157782,'<#676:HEAD?,"Adverb">',1,600),
(669,3712157782,'<#676:HEAD?,"Artikel">',1,600),
(670,3712157782,'<#676:HEAD?,"Interjektion">',1,600),
(671,3712157782,'<#676:HEAD?,"Konjunktion">',1,600),
(672,3712157782,'<#676:HEAD?,"Nomen">',1,600),
(673,3712157782,'<#676:HEAD?,"Präposition">',1,600),
(674,3712157782,'<#676:HEAD?,"Pronomen">',1,600),
(675,3712157782,'<#676:HEAD?,"Verb">',1,600),

-- And these must be created by each plugin
(81,3712157782,'<#676:HEAD?,"WEBSOCKET-API-PLUGIN">',1,7),
(84,3712141989,'<#676:HEAD?,"WEBSOCKET-API-INSTANCE",":PERSISTENT",T>',1,81),
(82,3712157782,'<#676:HEAD?,"DeviceCommunication-Plugin">',1,7),
(83,3712141989,'<#676:HEAD?,"DeviceCommunication-Instance",":PERSISTENT",T>',1,82),
(85,3712157782,'<#676:HEAD?,"Language-Processor-Plugin">',1,7),
(86,3712141989,'<#676:HEAD?,"Language-Processor-Instance",":PERSISTENT",T>',1,85);


-- --------------------------------------------------------

--
-- Table structure for table `DingAssociations`
--

DROP TABLE IF EXISTS `DingAssociations`;
CREATE TABLE `DingAssociations` (
  `DING-ID` bigint(20) UNSIGNED NOT NULL COMMENT 'The ID of the Ding',
  `UPDATE-CYCLES` int(10) UNSIGNED NOT NULL COMMENT 'A counter of updates on this Ding',
  `ASSOCIATIONS` mediumtext NOT NULL COMMENT 'The list of Associations'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Marvin''s Associations';

--
-- Dumping data for table `DingAssociations`
--

INSERT INTO `DingAssociations` (`DING-ID`, `UPDATE-CYCLES`, `ASSOCIATIONS`) VALUES
(0,0,'<<#1:HEAD?,1.0f0>,<#2:HEAD?,1.0f0>,<#3:HEAD?,1.0f0>,<#4:HEAD?,1.0f0>,<#5:HEAD?,1.0f0>,<#6:HEAD?,1.0f0>,<#7:HEAD?,1.0f0>,<#8:HEAD?,1.0f0>,<#9:HEAD?,1.0f0>,<#10:HEAD?,1.0f0>>'),
(4,0,'<<#452:HEAD?,1.0f0>,<#453:HEAD?,1.0f0>,<#454:HEAD?,1.0f0>,<#455:HEAD?,1.0f0>,<#456:HEAD?,1.0f0>,<#457:HEAD?,1.0f0>>'),
(5,0,'<<#409:HEAD?,1.0f0>,<#410:HEAD?,1.0f0>,<#411:HEAD?,1.0f0>,<#412:HEAD?,1.0f0>,<#413:HEAD?,1.0f0>,<#414:HEAD?,1.0f0>,<#415:HEAD?,1.0f0>,<#416:HEAD?,1.0f0>,<#417:HEAD?,1.0f0>,<#418:HEAD?,1.0f0>,<#419:HEAD?,1.0f0>,<#420:HEAD?,1.0f0>,<#421:HEAD?,1.0f0>,<#422:HEAD?,1.0f0>,<#423:HEAD?,1.0f0>,<#424:HEAD?,1.0f0>,<#425:HEAD?,1.0f0>,<#426:HEAD?,1.0f0>,<#427:HEAD?,1.0f0>,<#428:HEAD?,1.0f0>,<#429:HEAD?,1.0f0>,<#430:HEAD?,1.0f0>,<#431:HEAD?,1.0f0>,<#432:HEAD?,1.0f0>,<#433:HEAD?,1.0f0>,<#434:HEAD?,1.0f0>,<#435:HEAD?,1.0f0>,<#436:HEAD?,1.0f0>,<#437:HEAD?,1.0f0>,<#438:HEAD?,1.0f0>,<#439:HEAD?,1.0f0>,<#440:HEAD?,1.0f0>,<#441:HEAD?,1.0f0>,<#442:HEAD?,1.0f0>,<#443:HEAD?,1.0f0>,<#444:HEAD?,1.0f0>,<#445:HEAD?,1.0f0>,<#446:HEAD?,1.0f0>,<#447:HEAD?,1.0f0>,<#448:HEAD?,1.0f0>,<#449:HEAD?,1.0f0>>'),
(6,0,'<<#460:HEAD?,1.0f0>,<#461:HEAD?,1.0f0>,<#462:HEAD?,1.0f0>,<#463:HEAD?,1.0f0>,<#464:HEAD?,1.0f0>>'),
(677,0,'<<#0:HEAD? 1.0f0>>'),

(1,0,'<>'),
(2,0,'<>'),
(3,0,'<>'),
(7,0,'<>'),
(8,0,'<>'),
(9,0,'<>'),
(10,0,'<>'),
(81,0,'<>'),
(82,0,'<>'),
(83,0,'<>'),
(84,0,'<>'),
(408,0,'<>'),
(409,0,'<>'),
(410,0,'<>'),
(411,0,'<>'),
(412,0,'<>'),
(413,0,'<>'),
(414,0,'<>'),
(415,0,'<>'),
(416,0,'<>'),
(417,0,'<>'),
(418,0,'<>'),
(419,0,'<>'),
(420,0,'<>'),
(421,0,'<>'),
(422,0,'<>'),
(423,0,'<>'),
(424,0,'<>'),
(425,0,'<>'),
(426,0,'<>'),
(427,0,'<>'),
(428,0,'<>'),
(450,0,'<>'),
(429,0,'<>'),
(430,0,'<>'),
(431,0,'<>'),
(432,0,'<>'),
(433,0,'<>'),
(434,0,'<>'),
(435,0,'<>'),
(436,0,'<>'),
(437,0,'<>'),
(438,0,'<>'),
(439,0,'<>'),
(440,0,'<>'),
(441,0,'<>'),
(442,0,'<>'),
(443,0,'<>'),
(444,0,'<>'),
(445,0,'<>'),
(446,0,'<>'),
(447,0,'<>'),
(448,0,'<>'),
(449,0,'<>'),
(452,0,'<>'),
(453,0,'<>'),
(454,0,'<>'),
(455,0,'<>'),
(456,0,'<>'),
(457,0,'<>'),
(458,0,'<>'),
(460,0,'<>'),
(461,0,'<>'),
(462,0,'<>'),
(463,0,'<>'),
(464,0,'<>'),
(665,0,'<>'),
(666,0,'<>'),
(667,0,'<>'),
(668,0,'<>'),
(669,0,'<>'),
(670,0,'<>'),
(671,0,'<>'),
(672,0,'<>'),
(673,0,'<>'),
(674,0,'<>'),
(675,0,'<>'),
(676,0,'<>'),
(678,0,'<>'),
(679,0,'<>');


-- --------------------------------------------------------

--
-- Table structure for table `_OPTIONS_`
--

DROP TABLE IF EXISTS `_OPTIONS_`;
CREATE TABLE `_OPTIONS_` (
  `parameter` varchar(64) NOT NULL,
  `value` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `_OPTIONS_`
--

INSERT INTO `_OPTIONS_` (`parameter`, `value`) VALUES
('NEXT-ID', '1000'),
(':ENABLED-PLUGINS', '<":DEVICE-COMMUNICATION" ":WEBSOCKET-API" ":LANGUAGE-PROCESSOR">'),
(':PLUGIN-PATHS', '<"../plugins">');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `LangzeitSpeicher`
--
ALTER TABLE `LangzeitSpeicher`
  -- ADD PRIMARY KEY (`GUID`),
  ADD PRIMARY KEY `Ding-Revision` (`DING-ID`,`REVISION`),
  ADD KEY `ID` (`DING-ID`),
  ADD KEY `Klasse` (`KLASSE`);

--
-- Indexes for table `_OPTIONS_`
--
ALTER TABLE `_OPTIONS_`
  ADD PRIMARY KEY (`parameter`);


--
-- Indexes for table `_OPTIONS_`
--
ALTER TABLE `DingAssociations`
  ADD PRIMARY KEY (`DING-ID`);
--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `LangzeitSpeicher`
--
-- ALTER TABLE `LangzeitSpeicher`
--   MODIFY `GUID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'The internal GUID', AUTO_INCREMENT=14;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
