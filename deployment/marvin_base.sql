-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 21, 2017 at 08:40 AM
-- Server version: 5.5.57-0+deb8u1
-- PHP Version: 5.6.30-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `marvin`
--

DROP FUNCTION IF EXISTS `GET_HEAD_REVISION`;
DROP FUNCTION IF EXISTS `GET_NEXT_ID`;
DROP FUNCTION IF EXISTS `GET_LAST_CREATED_ID`;
DROP FUNCTION IF EXISTS `REVISION_HEAD_P`;


DELIMITER $$
--
-- Functions
--

CREATE DEFINER=`marvin`@`localhost` FUNCTION `GET_HEAD_REVISION` (`ID` INT UNSIGNED)
RETURNS INT(10) UNSIGNED
NOT DETERMINISTIC READS SQL DATA
BEGIN
  DECLARE head_revision INT;
  SET head_revision = 1;
  
  SELECT `REVISION` FROM `LangzeitSpeicher` WHERE `DING-ID` = ID ORDER BY `REVISION` DESC LIMIT 1 INTO head_revision;
   
  RETURN head_revision;
END$$

CREATE DEFINER=`marvin`@`localhost` FUNCTION `GET_NEXT_ID` ()
RETURNS INT(10) UNSIGNED 
NOT DETERMINISTIC READS SQL DATA # actually MODIFIES SQL DATA, but there is a bug in MySQL #93807
BEGIN
  DECLARE next_id INT;
  SET next_id = 0;

  SELECT `value` from `_OPTIONS_` where `parameter` = 'NEXT-ID' into next_id;

  UPDATE `_OPTIONS_` SET `value` = CAST((1 + next_id) AS CHAR) where `parameter` = 'NEXT-ID';

  RETURN next_id;
END$$

CREATE DEFINER=`marvin`@`localhost` FUNCTION `GET_LAST_CREATED_ID` ()
RETURNS INT(10) UNSIGNED 
NOT DETERMINISTIC READS SQL DATA
BEGIN
  DECLARE next_id INT;
  SET next_id = 0;

  SELECT `value` from `_OPTIONS_` where `parameter` = 'NEXT-ID' into next_id;

  RETURN next_id - 1;
END$$

CREATE DEFINER=`marvin`@`localhost` FUNCTION `REVISION_HEAD_P` (`ID` INT UNSIGNED, `REV` INT UNSIGNED) 
RETURNS TINYINT(4)
NOT DETERMINISTIC
READS SQL DATA
BEGIN
  DECLARE head_revision INT;
  DECLARE revision_count INT;
  SET head_revision = -1;
  SET revision_count = 0;
  
  SELECT `REVISION`, count(`REVISION`) FROM `LangzeitSpeicher` WHERE `DING-ID` = ID INTO head_revision, revision_count;
   
  RETURN ((revision_count > 0) and (head_revision = REV));
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `LangzeitSpeicher`
--

DROP TABLE IF EXISTS `LangzeitSpeicher`;
CREATE TABLE `LangzeitSpeicher` (
  `DING-ID` bigint(20) UNSIGNED NOT NULL COMMENT 'The ID of the Ding',
  `ÄNDERUNGSDATUM` int(10) UNSIGNED NOT NULL COMMENT 'Timestamp of this modification',
  `P-LIST` mediumtext NOT NULL COMMENT 'The content of the Ding''s P-List',
  `REVISION` int(11) DEFAULT 1 COMMENT 'The number of this revision',
  `KLASSE` bigint(20) NOT NULL COMMENT 'The class of this Ding'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Marvin''s LangzeitSpeicher';


-- --------------------------------------------------------

--
-- Table structure for table `DingAssociations`
--

DROP TABLE IF EXISTS `DingAssociations`;
CREATE TABLE `DingAssociations` (
  `DING-ID` bigint(20) UNSIGNED NOT NULL COMMENT 'The ID of the Ding',
  `UPDATE-CYCLES` int(10) UNSIGNED NOT NULL COMMENT 'A counter of updates on this Ding',
  `ASSOCIATIONS` mediumtext NOT NULL COMMENT 'The list of Associations'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Marvin''s Associations';


-- --------------------------------------------------------

--
-- Table structure for table `_OPTIONS_`
--

DROP TABLE IF EXISTS `_OPTIONS_`;
CREATE TABLE `_OPTIONS_` (
  `parameter` varchar(64) NOT NULL,
  `value` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


--
-- Indexes for dumped tables
--

--
-- Indexes for table `LangzeitSpeicher`
--
ALTER TABLE `LangzeitSpeicher`
  -- ADD PRIMARY KEY (`GUID`),
  ADD PRIMARY KEY `Ding-Revision` (`DING-ID`,`REVISION`),
  ADD KEY `ID` (`DING-ID`),
  ADD KEY `Klasse` (`KLASSE`);

--
-- Indexes for table `_OPTIONS_`
--
ALTER TABLE `_OPTIONS_`
  ADD PRIMARY KEY (`parameter`);


--
-- Indexes for table `_OPTIONS_`
--
ALTER TABLE `DingAssociations`
  ADD PRIMARY KEY (`DING-ID`);
--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `LangzeitSpeicher`
--
-- ALTER TABLE `LangzeitSpeicher`
--   MODIFY `GUID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'The internal GUID', AUTO_INCREMENT=14;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
