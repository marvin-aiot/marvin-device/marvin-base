;;;; Copyright (C) 2019 Eric Diethelm

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(in-package :marvin)

(ql:quickload :usocket)
(ql:quickload :trivial-utilities)
(ql:quickload :trivial-repl)
(ql:quickload :trivial-gitlab-api)
(ql:quickload :piggyback-parameters)

(defvar *marvin-plugins-group* 4694788)

(defparameter *setup-steps* '())

(defmacro define-setup-step (name documentation &body body)
  (declare (ignore documentation))
  `(push (cons ,name (trivial-utilities:alambda () ,@body)) *setup-steps*))

(defmacro get-user-response (question &rest args)
  `(progn
     (format t ,question ,@args)
     (let ((ans (read-line)))
       (format t "~%")
       (values ans))))

(defmacro get-user-response-boolean (question &rest args)
  `(progn
     (format t "~a ([Y]es/[N]o) " ,question ,@args)
     (let ((ans (read-line)))
       (format t "~%")
       (and (stringp ans) (or (eq (char ans 0) #\y) (eq (char ans 0) #\Y))))))

(define-setup-step :start
    "Check if marvin is already running
   Yes - Inform the user and goto :exit
   No  - Goto :load-config"
  (let ((running? (uiop:find-symbol* "RUNNING?" :marvin nil)))
    (if (and running? (funcall running?))
	(progn
	  (format t "Marvin is already running. Please stop it and re-execute this.~%")
	  (return-from self :abort)))
    (return-from self :update-code)))

(define-setup-step :update-code
    "Ask is the codebase should be updated now
     Yes - 
     No - "
  ;; @TODO This list of libraries should be stored in a separate file and loaded here.
  (let ((libraries (list (list "ediethelm" "trivial-utilities" "")
			 (list "ediethelm" "trivial-object-lock" "")
			 (list "ediethelm" "trivial-json-codec" "")
			 (list "ediethelm" "trivial-hashtable-serialize" "")
			 (list "ediethelm" "trivial-pooled-database" "")
			 (list "ediethelm" "trivial-timer" "")
			 (list "ediethelm" "trivial-filter" "")
			 (list "ediethelm" "trivial-continuation" "")
			 (list "ediethelm" "trivial-variable-bindings" "")
			 (list "ediethelm" "trivial-monitored-thread" "")
			 (list "ediethelm" "trivial-repl" "")
			 (list "ediethelm" "trivial-gitlab-api" "")
			 (list "ediethelm" "piggyback-parameters" "")
			 (list "marvin-aiot" "marvin-aiot" ""))))
    
    ;; Check if the needed libraries are already installed
    (let ((missing-projects (iterate:iterate
			     (iterate:for (username project group) in libraries)
			     (unless (asdf:find-system project nil)
			       (iterate:collect (list username project group))))))
      
      ;; @TODO What should we do? And how to connect this with the part below?
      (when missing-projects
	(if (get-user-response-boolean "Some necessary libraries for Marvin are missing. Should I install them now?")
	    (progn
	      (iterate:iterate
	       (iterate:for (username project group) in libraries)
	       (trivial-gitlab-api:clone-project username project group (asdf:system-source-directory :marvin-aiot))))
	    (progn
	      (format t "See the necessary libraries in the documentation and manually install them.")
	      (return-from self :abort))))

      (trivial-utilities:awhen (nset-difference libraries missing-projects :test #'string= :key #'second)
	(when (get-user-response-boolean "Should I now update all the previously existing Marvin libraries?")
	  (if (get-user-response-boolean "~%NOTE: Any local changes will be destroyed! Are you sure?")
	      (labels ((reset-repository (project group)
			 (let ((project-path (trivial-gitlab-api:build-project-path          ;; @TODO Here we should actually check where the library is installed, 
					      (uiop:pathname-parent-directory-pathname                   ;;       as this could be in some other location!
					       (asdf:system-source-directory :marvin-aiot))
					      project
					      group))
			       (git-reset "cd ~a && git reset --hard @{u}")
			       (git-clean "cd ~a && git clean -dfx")
			       (git-pull "cd ~a && git pull"))

			   (format t "Resetting ~a in ~a~%" project project-path)
			   (uiop:run-program (format nil git-reset project-path))
			   (uiop:run-program (format nil git-clean project-path))
			   (uiop:run-program (format nil git-pull project-path)))))
		(iterate:iterate
		 (iterate:for (username project group) in it)
		 (declare (ignorable username))
		 (reset-repository project group)))
	      (unless (get-user-response-boolean  "Please continue with caution, since some of the libraries might be in an incompatible version.~%Do you want to continue the installation procedure?")
		(return-from self :abort)))))))
  
  (return-from self :load-config))

(define-setup-step :load-config
    "Try to initialize piggyback-parameters
  If error - ask the user if a new config should be created
    Yes - Goto :setup-db
    No - Ask for the path of of the config file (if empty, stop), copy it to the expected location and goto :start
  If no error - Goto :setup-db"
  (let ((config-file (uiop/common-lisp:merge-pathnames
		      #P"config"
		      (asdf:system-source-directory :marvin-aiot))))
    
    (handler-bind
	((piggyback-parameters:file-not-found-error
	  #'(lambda (c)
	      (declare (ignore c))
	      (if (get-user-response-boolean "No configuration file was found. Do you want me to create one now?")
		  (progn
		    ;; Create the config file
		    (with-open-file (f config-file :direction :output
				       :if-exists :supersede
				       :if-does-not-exist :create))
		    (return-from self :load-config))
		  (return-from self :abort)))))

      (piggyback-parameters:set-configuration-file config-file)))
  (return-from self :setup-db-connection))

(define-setup-step :setup-db-connection
    "Ask for the hostname/ip of the MySQL server
     Check server is reachable
       Yes - Goto :setup-db-root-access
       No - Inform the user and goto :exit"
  (let* ((host (get-user-response "Please enter the hostname/IP of the DB server: "))
	 (sock (ignore-errors
		 (usocket:socket-connect host 3306 :timeout 0.5))))
    (if sock
	(progn
	  (usocket:socket-close sock)

	  ;; Save host and port to config file
	  (setf (piggyback-parameters:get-value :db-host :file-only) host)
	  (return-from self :setup-db-root-access))
	(progn
	  (format t "Could not contact MySQL server at ~a.~%" host)
	  (return-from self :abort)))))

(define-setup-step :setup-db-root-access
    "Ask for the hostname/ip of the MySQL server
     Check server is reachable
       Yes - Goto :create-user
       No - Inform the user and goto :exit
     Ask for root credentials and goto :create-user"
  
  (let ((username (get-user-response "Please enter the username of the DB admin user: "))
	(passwd (get-user-response "Please enter the password of the DB admin user: "))
	(db-connection nil))
    (unwind-protect
	 (progn
	   (setf db-connection (dbi:connect :mysql
					    :username username
					    :password passwd
					    :host (piggyback-parameters:get-value :db-host :file-only)
					    :database-name nil))
	   (unless db-connection
	     (format t "Unable to login to the DB server. Please check your credentials.")
	     (return-from self :abort))

	   (setf (piggyback-parameters:get-value :db-admin-username :memory-only) username)
	   (setf (piggyback-parameters:get-value :db-admin-passwd :memory-only) passwd))

      (when db-connection
	(dbi:disconnect db-connection))))
  
  (return-from self :create-user))

(define-setup-step :create-user
    "Check if a user named 'marvin' already exists
     Yes - Inform the user and goto :exit
     No - Create a new DB user named 'marvin' and generate a password and goto :create-schema"
  (let ((db-connection nil))
    (unwind-protect
	 (progn
	   (setf db-connection (dbi:connect :mysql
					    :username (piggyback-parameters:get-value :db-admin-username :memory-only)
					    :password (piggyback-parameters:get-value :db-admin-passwd :memory-only)
					    :host (piggyback-parameters:get-value :db-host)
					    :database-name nil))

	   ;;(piggyback-parameters:clear-parameter :db-admin-username)
	   ;;(piggyback-parameters:clear-parameter :db-admin-passwd)

	   (unless db-connection
	     (format t "Unable to login to the DB server. Please check your credentials.~%")
	     (return-from self :abort))

	   ;; Check if user 'marvin' exists
	   (log:info (dbi:fetch-all
		      (dbi:execute
		       (dbi:prepare db-connection "select user from mysql.user where user = 'marvin'"))))

	   (when (dbi:fetch-all
		  (dbi:execute
		   (dbi:prepare db-connection "select user from mysql.user where user = 'marvin'")))
	     (format t "Apparently a user named 'marvin' already exists in the database.~%")
	     (return-from self :abort))

	   ;; Create user 'marvin'
	   (setf (piggyback-parameters:get-value :db-username :file-only) "marvin")
	   ;; Create a password
	   (setf (piggyback-parameters:get-value :db-passwd :file-only) (format nil "~x" (sxhash (random 999999))))
	   
	   (let ((create-user-stmt (concatenate 'string
						"CREATE USER 'marvin'@'"
						(if (string-equal (piggyback-parameters:get-value :db-host)
								  "localhost")
						    "localhost"
						    "%")
						"' IDENTIFIED BY '"
						(piggyback-parameters:get-value :db-passwd)
						"'")))
	     (setf (piggyback-parameters:get-value :db-username :file-only) "marvin")
	     (let ((res (dbi:fetch-all
			 (dbi:execute
			  (dbi:prepare db-connection create-user-stmt)))))
	       (log:info "Result from creation: " res))))

      (when db-connection
	(dbi:disconnect db-connection))))
  (return-from self :create-schema))

(define-setup-step :create-schema
    "Check if a schema named 'marvin' exists
     Yes - Inform the user and goto :abort
     No - Create shema according to marvin_base.sql"

  ;; @TODO Find a way to change the 'localhost' entry in the 'DEFINER' of functions found in marvin_base.sql.
  
  (return-from self :install-plugins))

(define-setup-step :install-plugins
    "Ask if any other plugin should be installed
     Yes - Ask for plugin name and goto :install-user-plugins
     No - Goto :start-marvin"
  (let ((all-projects (remove-if #'null (trivial-gitlab-api:discover-asdf-projects *marvin-plugins-group*))))
    (labels ((add-local-project-version (all-projects)
	       (iterate:iterate
		(iterate:for project in all-projects)
		(trivial-utilities:awhen (asdf:find-system (getf project :project-defsys) nil)
		  (setf (getf project :local-version) (asdf:component-version it)))))
	     (list-all-projects (r a)
	       (declare (ignore r a))
	       (iterate:iterate
		(iterate:for project in all-projects)
		(format t "ID: ~7@<~d~> Name: ~24A Head Version: ~7A Installed Version: ~7A~%Description: ~{~<~%~13t~1,90:; ~A~>~}~%~%"
			(getf project :project-id)
			(getf project :project-name)
			(getf project :project-version)
			(trivial-utilities:aif (getf project :local-version)
					       it
					       "n.a.")
			(split-sequence:split-sequence #\SPACE (getf project :project-description)))))
	     (show-project-details (all-projects args)
	       (declare (ignore all-projects args))
	       (format t "No details avaliable at this time."))
	     (select-project (r args)
	       (declare (ignore r))
	       (let ((project-id (car args)))
		 (unless project-id
		   (format t "To select a project for installation or update the Project ID is required (e.g. 'select 1234')~%")
		   (return-from select-project))

		 (let ((project (car (member (parse-integer project-id) all-projects :key #'(lambda (x) (getf x :project-id))))))
		   (unless project
		     (format t "No project with the id ~a could be found.~%" project-id)
		     (return-from select-project))

		   (trivial-utilities:aif (member :action project)
					  (setf (cadr it) :select)
					  (nconc project '(:action :select)))
		   (format t "Project with the id ~a is now selected to install/update.~%" project-id))))
	     (deselect-project (r args)
	       (declare (ignore r))
	       (let ((project-id (car args)))
		 (unless project-id
		   (format t "To deselect a project for installation or update the Project ID is required (e.g. 'deselect 1234')~%")
		   (return-from deselect-project))

		 (let ((project (car (member (parse-integer project-id) all-projects :key #'(lambda (x) (getf x :project-id))))))
		   (unless project
		     (format t "No project with the id ~a could be found.~%" project-id)
		     (return-from deselect-project))

		   (trivial-utilities:awhen (member :action project)
		     (setf (cadr it) nil))
		   (format t "Project with the id ~a is now deselected to install/update.~%" project-id))))
	     (view-actions (r a)
	       (declare (ignore r a))
	       (format t "~%")
	       (iterate:iterate
		(iterate:for project in all-projects)
		(when (not (null (getf project :action)))
		  (format t "~3t~a (~a) - Action: ~a~%"
			  (getf project :project-name)
			  (getf project :project-id)
			  (if (getf project :local-version)
			      "update"
			      "install")))))
	     (apply-actions (r a)
	       (format t "The following actions are going to be performed:~%")
	       (view-actions r a)
	       (when (get-user-response-boolean "Do you which to apply these changes?")
		 (iterate:iterate
		  (iterate:for project in all-projects)
		  (when (not (null (getf project :action)))
		    nil
		    ;; If there is no local-version -> clone :project-repo-url
		    ;; Else -> update project path
		    ;; (asdf:install-op project-name)
		    ;; Add plugin to :ENABLED_PLUGINS in database
		    )))))
      (let ((repl-light
	     (trivial-repl:create-repl
	      :name "Plugin Selector"
	      :entries `(("List" (#\L #\l) "Show a list of all plugins" ,#'list-all-projects)
			 ("Details" (#\D #\d) "Show details of the plugin with id $id" ,#'show-project-details)
			 ("Select" (#\S #\s) "Select plugin with id $id for installation/update" ,#'select-project)
			 ("Remove" (#\R #\r) "Remove or deselect plugin with id $id" ,#'deselect-project)
			 ("View" (#\V #\v) "Show a list of the selected actions" ,#'view-actions)
			 ("Execute" (#\X #\x) "Execute the selected actions" ,#'apply-actions)))))

	(add-local-project-version all-projects)

	;; Create a small repl to show the list of plugins, see plugin details, select plugin for installation, show selected plugins and also deselect selected plugins
	(trivial-repl:run-repl repl-light))))

  (return-from self :start-marvin))

(define-setup-step :start-marvin
    "Ask user if marvin should be started"
  (setf (piggyback-parameters:get-value :installed :file-only) t)
  
  (if (get-user-response-boolean "Should I start Marvin now?")
      (let ((fn (uiop:find-symbol* "INITIALIZE" :marvin nil)))
	(if fn
	    (progn
	      (funcall fn)
	      (return-from self :exit))

	    (progn
	      (log:error "Marvin initialization function missing. Unable to start.")
	      (return-from self :abort))))
      
      (return-from self :exit)))

(define-setup-step :abort
    ""
  (setf (piggyback-parameters:get-value :installed :file-only) nil)
  (format t "The setup procedure of Marvin AIoT was aborted. None of the changes done so far were reverted.~%Please restart the procedure after applying corrective steps.")
  (return-from self :exit))

(defun setup-marvin ()
  ""
  (let ((state :start))
    (loop
       until (or (null state) (eq :exit state) (eq :abort state))
       do (let* ((fn (cdr (assoc state *setup-steps*)))
		 (new-state (if fn (funcall fn) :exit)))
	    (format t "Transition from state ~a to ~a.~%" state new-state)
	    (setf state new-state)))))

(setup-marvin)
