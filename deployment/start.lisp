
(require :bordeaux-threads)
(require :log4cl)
(require :swank)

(defparameter *shutdown-lock* (bordeaux-threads:make-lock))
(defparameter *shutdown-request-cv* (bordeaux-threads:make-condition-variable))

(defun start-swank ()
  (setf swank::*LOOPBACK-INTERFACE* "0.0.0.0")
  (swank-loader:init)
  (swank:create-server :port 4005 :dont-close t))

(ql:quickload :marvin-base)

(defun top-level ()
  (log:config :pattern "%&%<%I%;<;;>;-5p [%D{%H:%M:%S}]%:; [;;];t %g{}{}{:downcase} (%C{}{ }{:downcase})%2.2N - %_%m%>%n")
  (log:config :daily "log/marvin.%Y%m%d.log" :pattern "<%p> [%D{%H:%M:%S}] [%t] (%C{}{ }{:downcase}) - %m%n")

  (start-swank)

  (piggyback-parameters:set-configuration-file
   (uiop/common-lisp:merge-pathnames
    #P"config"
    (asdf:system-source-directory :marvin-base)))

  (let ((installed-fn-p (uiop:find-symbol* "CORRECTLY-INSTALLED?" :marvin nil)))
    (if installed-fn-p
	(if (funcall installed-fn-p)
	    (let ((init-fn (uiop:find-symbol* "INITIALIZE" :marvin nil)))
	      (if init-fn
		  (bordeaux-threads:make-thread
		   #'(lambda ()
		       (log:info "Marvin will be started in 30 seconds.")
		       (loop repeat 30 do (sleep 1))
		       (funcall init-fn)
		       (log:info "Marvin started")))
		  
		  (log:error "Marvin initialization function missing. Unable to start.")))
	    (log:info "Marvin is not yet installed. Please call (install-marvin) to complete installation."))
	(log:error "Unable to find necessary functions of Marvin. Did it load without errors?")))

  (bordeaux-threads:with-lock-held (*shutdown-lock*)
    (bordeaux-threads:condition-wait *shutdown-request-cv* *shutdown-lock*)))

(defun stop-marvin ()
  (log:info "Exiting Marvin")
  (bordeaux-threads:condition-notify *shutdown-request-cv*)
  (bordeaux-threads:make-thread
   #'(lambda ()
       (sleep 60)
       (log:warn "Marvin did not terminate cleanly after 60 seconds. Killing it.")
       (quit)))

  (let ((fn (uiop:find-symbol* "SHUTDOWN" :marvin nil)))
    (if fn
	(funcall fn)
	(log:error "Marvin shutdown function missing. Unable to stop."))))

(top-level)
