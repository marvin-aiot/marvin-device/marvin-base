;;;; Copyright (C) 2019 Eric Diethelm

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(in-package :marvin-base)

(shadowing-import '(trivial-timer:register-timer-call

		    end-point:add-http-end-point end-point:add-ws-end-point
		    end-point:remove-http-end-point end-point:remove-ws-end-point

		    ding:ding-base ding:ding ding:ding-reference ding:id
		    ding:dirty ding:persistent ding:newly-created ding:erstellungsdatum
		    ding:ablaufdatum ding:activation-time ding:klasse ding:p-list
		    ding:eigenschaft ding:surpress-change-notification ding:register-ding
		    ding:with-eigenschaft-cache ding:separate-by-slots ding:get-class-for-id
		    ding:get-id-for-class ding:associations ding:update-cycles

		    working-memory:add-object working-memory:create-object
		    working-memory:find-object-by-id working-memory:get-all-objects-in-memory
		    working-memory:get-objects-of-class working-memory:find-ding
		    working-memory:find-by-params working-memory:find-objects-from-class
		    working-memory:find-all-dings working-memory:release-object

		    k-line:add-frame-to-kline k-line:child-frames k-line:k-line
		    k-line:replace-frames-from-kline k-line:result-frame k-line:continuation-point
		    k-line:executing-response k-line:executing-response k-line:initiator-frame
		    k-line:processee k-line:processing-responses

		    frame:frame frame:k-lines frame:executing-response frame:processing-responses

		    longterm-memory:search-exhausted-condition

		    plugin-base:plugin plugin-base:plugin-queue
		    plugin-base:analyze plugin-base:start-plugin plugin-base:stop-plugin
		    plugin-base:plugin-response plugin-base:plugin plugin-base:cost
		    plugin-base:fit plugin-base:resolution plugin-base:args
		    plugin-base:define-plugin plugin-base:initialize-plugin
		    
		    plugin-handler:register-plugin plugin-handler:install-plugin))

(export '(register-timer-call

	  add-http-end-point add-ws-end-point remove-http-end-point remove-ws-end-point

	  ding-base ding ding-reference ding-base-p ding-p ding-reference-p id dirty
	  persistent newly-created erstellungsdatum ablaufdatum activation-time klasse
	  p-list eigenschaft surpress-change-notification register-ding with-eigenschaft-cache
	  separate-by-slots get-class-for-id get-id-for-class associations update-cycles
	  
	  add-object create-object find-object-by-id get-all-objects-in-memory find-all-dings
	  find-ding get-objects-of-class find-by-params find-objects-from-class release-object

	  add-frame-to-kline child-frames k-line
	  replace-frames-from-kline result-frame continuation-point
	  executing-response executing-response initiator-frame
	  processee processing-responses

	  frame k-lines executing-response processing-responses

	  search-exhausted-condition

	  plugin plugin-queue initialize-plugin analyze start-plugin stop-plugin plugin-response
	  cost fit resolution args define-plugin
	  
	  register-plugin install-plugin))


(defun read-version ()
  (mapcar #'parse-integer (split-sequence:split-sequence #\. (asdf:component-version (asdf:find-system :marvin-base)))))

(defparameter *version* (read-version))
(defparameter *initialized* nil)

(defun initialize ()

  (piggyback-parameters:set-configuration-file
   (uiop/common-lisp:merge-pathnames
    #P"config"
    (asdf:system-source-directory :marvin-base)))

  (unless (correctly-installed?)
    (log:warn "Marvin AIoT was not yet installed. Please call (asdf:operate 'trivial-asdf-extensions:install-op :marvin-base) to complete installation.")
    (return-from initialize nil))

  (log:info "Starting Marvin AIoT.")

  #-non-persistent-working-memory
  (let ((username (piggyback-parameters:get-value :db-username :file-only))
	(passwd (piggyback-parameters:get-value :db-passwd :file-only))
	(schema (piggyback-parameters:get-value :db-schema :file-only))
	(host (piggyback-parameters:get-value :db-host :file-only)))
    (log:info "Initializing DB Connections Pool (~a ~a ~a ~a)"  username passwd schema host)
    (trivial-pooled-database:initialize-connection-pool username passwd schema host))

  (trivial-monitored-thread:start-thread-monitor)

  (end-point:initialize-end-points)

  #-non-persistent-working-memory
  (longterm-memory:initialize-longterm-memory)

  (working-memory:initialize-working-memory)
  ;;(rule-engine:initialize)
  ;;(rule-engine:register-rule-fn)
  (trivial-timer:initialize-timer)
  (ding:initialize-ding)

  (plugin-handler:initialize-plugins)

  ;; Start every plugin
  (plugin-Handler:start-plugins)

  (setup-endpoints)

  (setf *initialized* t)

  (log:info "Marvin AIoT started."))

(defun shutdown ()
  ""
  (log:info "Stopping Marvin AIoT.")

  (setf *initialized* nil)

  (teardown-endpoints)

  (plugin-handler:stop-plugins)
  (trivial-timer:shutdown-timer)
  (working-memory:shutdown-working-memory)

  #-non-persistent-working-memory
  (longterm-memory:shutdown-longterm-memory)

  (trivial-monitored-thread:stop-thread-monitor)
  (end-point:shutdown-end-points)
  (piggyback-parameters:reset)

  (log:info "Marvin AIoT stopped."))

(defun running? ()
  ""
  *initialized*)

(defun correctly-installed? ()
  (not (null (ignore-errors (piggyback-parameters:get-value :installed :file-only)))))

(defun show-welcome-message ()
  ""
  ;; Show a message containing information on the current status of Marvin

  ;; Uptime
  ;; Number of known and operational devices
  ;;
  
  )

(defun make-executable ()
  (when (running?)
    (shutdown))

  #+sbcl
  (sb-ext:save-lisp-and-die "marvin" :toplevel #'initialize :executable t))

(defun setup-endpoints ()
  (end-point:add-http-end-point :version
			"Read the version of Marvin AIoT."
			:v1
			:get
			#'(lambda (resource request)
			    (declare (ignore resource request))
			    (with-output-to-string (stream)
			      (format stream "{ \"version\" : [~{~a~^,~}]}" *version*))))

  (end-point:add-http-end-point :list-parameters
			"List all avaliable configuration parameters."
			:v1
			:get
			#'list-configuration-parameters
			:parents '(configuration))

  (end-point:add-http-end-point :parameter
			"Retrieve the value of a configuration parameter."
			:v1
			:get
			#'read-parameter
			:parents '(configuration))

  (end-point:add-http-end-point :parameter
			"Update the value of a configuration parameter."
			:v1
			:put
			#'update-parameter
			:parents '(configuration))

  (end-point:add-http-end-point :parameter
			"Insert a new configuration parameter with the given value."
			:v1
			:post
			#'create-parameter
			:parents '(configuration)))

(defun teardown-endpoints ()
  (end-point:remove-http-end-point :list-parameters
				   :v1
				   :get
				   '(configuration))

  (end-point:remove-http-end-point :parameter
				   :v1
				   :get
				   '(configuration))

  (end-point:remove-http-end-point :parameter
				   :v1
				   :put
				   '(configuration))

  (end-point:remove-http-end-point :parameter
				   :v1
				   :post
				   '(configuration)))

(defun list-configuration-parameters ()
  )

(defun read-parameter ()
  )

(defun update-parameter ()
  )

(defun create-parameter ()
  )
