# Marvin Manual

###### \[in package MARVIN\]
[![pipeline status](https://gitlab.com/marvin-aiot/marvin-device/marvin-base/badges/master/pipeline.svg)](https://gitlab.com/marvin-aiot/marvin-device/marvin-base/commits/master)


## Description

## Installing marvin


## License Information

This library is released under the GNU General Public License. Please refer to the [LICENSE](https://gitlab.com/marvin-aiot/marvin-device/marvin-base/blob/master/LICENSE "License") to get the full licensing text.

## Contributing to this project

Please refer to the [CONTRIBUTING](https://gitlab.com/marvin-aiot/marvin-device/marvin-base/blob/master/CONTRIBUTING.md "Contributing") document for more information.

