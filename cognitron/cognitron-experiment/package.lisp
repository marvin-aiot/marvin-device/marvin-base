
(uiop:define-package #:experiment
  (:documentation "experiment")
  (:use #:common-lisp)
  (:export #:feature-property
	   #:id
	   #:min-value
	   #:max-value
	   #:mean-value
	   #:examples
	   #:metadata
	   #:feature-id-map
	   #:feature-properties
	   #:experiment
	   #:data-set
	   #:feature-set
	   #:dev-set
	   #:train-set
	   #:test-set
	   #:hints
	   #:models
	   #:create-experiment
	   #:extract-features
	   #:start-experiment
	   #:get-feature-set
	   #:metadata
	   #:get-feature-id
	   #:get-feature-from-id
	   #:add-feature
	   #:get-feature-property
	   #:update-feature-property))

