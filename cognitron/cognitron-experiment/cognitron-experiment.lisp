;;;; Copyright (C) 2019 Eric Diethelm

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(in-package :experiment)


#|

An *EXPERIMENT* transforms a set of data into a classifiaction model.


*HINT*s can be given to an *EXPERIMENT* in order to guide the search.
A *FITNESS-FUNCTION* can be added to evaluate the resulting model.
Filtering of data can be achieved through the *FILTER-FUNCTION*.
Special handling of features can be accomplished by adding a *FEATURE-TRANSFORMATION-FUNCTION*.

If one of the above is not defined, the *EXPERIMENT* will search for a potentially helpfull one. This is part of the meta-parameters search.


Input data:
- Mode I:   ((data . expected-class)+)    -> A set of individualy labeled data
- Mode II:  (((data+) . expected-class)+) -> A set of data labeled for classification training
- Mode III: (data+)                       -> An unlabeled set of data for unsupervised learning

Only mode I is supported at the moment!!

Hints:
- :classification
- :regression
- :anomally-detection
- :prediction
- :temporal
- :spatial

Function signatures:

- Fitness function -> ((test-set test-result) float)
- Filter function -> ((input-data) boolean)
- Transformation function -> ((input-data metadata) (feature+))




|#

(defclass metadata ()
  ((feature-id-map :accessor feature-id-map
		   :initarg :feature-id-map
		   :initform '()
		   :documentation "Contains the mapping from feature name to feature id.")
   (feature-properties :accessor feature-properties
		       :initarg :feature-properties
		       :initform '()
		       :documentation "Contains the mapping from feature id to properties."))
  (:documentation "Metadata regarding features of an experiment."))

(defclass experiment (ding:ding)
  ((data-set :reader data-set
	     :initarg :data-set)
   (dev-set :reader dev-set
	    :initform nil)
   (train-set :reader train-set
	      :initform nil)
   (test-set :reader test-set
	     :initform nil)
   (features-to-ignore :reader features-to-ignore
		       :initarg :ignored-features
		       :initform nil)
   (metadata :reader metadata
	     :initarg :metadata
	     :initform (make-instance 'metadata))
   (hints :reader hints
	  :initarg :hints
	  :initform :none
	  :type list)
   (transform-function :reader transformer
		       :initarg :transformer
		       :type function
		       :documentation "A function to extract the value of a specific feature.")
   (manipulation-function :reader manipulator
		       :initarg :manipulator
		       :type function
		       :documentation "A function to manipulate an element of the dataset.")
   (fitness-function :reader fitness
		     :initarg :fitness)
   (models :reader models
	   :initform (make-hash-table)
	   :documentation "Holds the resulting models from training."))
  (:documentation ""))

(defun create-experiment (data-set &key (features-to-ignore '()) (hints '(:none)) (manipulator #'default-manipulate)
				     (transformer #'default-transform-feature) (fitness #'identity))
  ;; Analyze the data-set and decide on default
  (trivial-utilities:aprog1
      (make-instance 'experiment :id 0 :ignored-features features-to-ignore :hints (trivial-utilities:mklist hints)
		     :transformer transformer :fitness fitness :manipulator manipulator)
    ;;(setf (slot-value it 'feature-set) (mapcar #'(lambda (x) (transformer-default x (metadata it))) data-set))
    (split-examples (mapcar #'(lambda (x) (cons (extract-features (car x) it) (cdr x))) data-set) it)
    (finalize-feature-discovery it)))

(defun default-manipulate (data experiment)
  (declare (ignore experiment))
  data)

(defun default-transform-feature (feature-id feature-value feature-properties)
  (declare (ignore feature-id feature-properties))
  feature-value)

(defun get-feature-id (feature experiment)
  (cdr (assoc feature (feature-id-map (metadata experiment)))))

(defun get-feature-from-id (id experiment)
  (car (rassoc id (feature-id-map (metadata experiment)))))

(defun get-feature-property (id experiment)
  (cdr (assoc id (feature-properties (metadata experiment)))))

(defun add-feature (feature feature-metadata experiment)
  (assert feature-metadata)
  (assert (not (get-feature-id feature experiment)))
  (let ((hash (sxhash feature)))
    (push (cons feature hash) (feature-id-map (metadata experiment)))
    (push (append (list :id hash) feature-metadata) (feature-properties (metadata experiment)))
    hash))

(defun update-feature-property (feature-symb feature-value experiment)
  (let* ((feature-id (trivial-utilities:aif (get-feature-id feature-symb experiment)
						it
						(let ((hash (sxhash feature-symb)))
						  (push (cons feature-symb hash) (feature-id-map (metadata experiment)))
						  hash)))
	 (entry (get-feature-property feature-id experiment)))

    (if (member feature-symb (features-to-ignore experiment) :test #'trivial-utilities:equals)
	(unless entry
	  (push (cons feature-id (list :id feature-id :type :ignore)) (feature-properties (metadata experiment))))
	
	(if (not entry)
	    (progn
	      (setf entry
		    (append (list :id feature-id)
			    (cond ((numberp feature-value)
				   ;; Numeric types
				   (list :min-value feature-value
					 :max-value feature-value
					 :mean-value feature-value
					 :examples 1
					 :type :numeric))
				  ((or (keywordp feature-value) (stringp feature-value))
				   ;; Enumerations (strings, keywords)
				   (list :values (list feature-value)
					 :type (or (and (keywordp feature-value) :keyword)
						   (and (stringp feature-value) :string))))
				  ((null feature-value)
				   ;; This could turn into any type
				   (list :type :unknown))
				  ((typep feature-value 'ding:ding-base)
				   (list :type :ding))
				  (t (list :type :ignore)))))
	      
	      (push (cons feature-id entry) (feature-properties (metadata experiment))))
	    (unless (eq (getf entry :type) :ignore)
	      (cond ((numberp feature-value)
		     ;; Numeric types
		     (assert (eq (getf entry :type) :numeric))
		     (when (< feature-value (getf entry :min-value))
		       (setf (getf entry :min-value) feature-value))

		     (when (> feature-value (getf entry :max-value))
		       (setf (getf entry :max-value) feature-value))

		     (setf (getf entry :mean-value) (/ (+ (* (getf entry :examples) (getf entry :mean-value)) feature-value) (incf (getf entry :examples)))))

		    ((or (keywordp feature-value) (stringp feature-value))
		     ;; Enumerations (strings, keywords)
		     (assert (eq (getf entry :type)
				 (or (and (keywordp feature-value) :keyword)
				     (and (stringp feature-value) :string))))
		     (setf (getf entry :values) (adjoin feature-value (getf entry :values) :test (if (stringp feature-value) #'string= #'eql))))
		    ((and (eq feature-value t) (eq (getf entry :type) :unknown))
		     (setf (getf entry :type) :boolean))
		    ((and (typep feature-value 'ding:ding-base) (eq (getf entry :type) :unknown))
		     (setf (getf entry :type) :ding))
		    ((or (and (null feature-value) (or (eq (getf entry :type) :unknown)
						       (eq (getf entry :type) :boolean)
						       (eq (getf entry :type) :ding)))
			 (and (typep feature-value 'ding:ding-base) (eq (getf entry :type) :ding))
			 (and (eq feature-value t) (eq (getf entry :type) :boolean))
			 (eq (getf entry :type) :ignore))
		     nil)
		    (t
		     (error "Unknown value type of '~a' (a ~a)" feature-value (type-of feature-value)))))))))


(defun find-all-features-in-data (experiment)
  (iterate:iterate
    (iterate:for data in (data-set experiment))
    (iterate:iterate
      (iterate:for feature on (ding:eigenschaften (car data)) by #'cddr)
      (update-feature-property (car feature) (cadr feature) experiment))))

(defun finalize-feature-discovery (experiment)  
  (iterate:iterate
    (iterate:for feature in (feature-properties (metadata experiment)))
    (when (eq (getf (cdr feature) :type) :unknown)
      (setf (getf (cdr feature) :type) :ignore))))

(defun extract-features (data experiment)
  (delete-if #'null
	     (if (consp data)
		 (list (extract-features (car data) experiment) (cdr data))
		 (funcall (manipulator experiment)
			  (iterate:iterate
			    (iterate:for feature on (ding:eigenschaften data) by #'cddr)
			    (update-feature-property (car feature) (cadr feature) experiment)
			    (let* ((feature-value (cadr feature))
				   (feature-id (cdr (assoc (car feature) (feature-id-map (metadata experiment)))))
				   (feature-property (get-feature-property feature-id experiment)))
			      (unless feature-id
				(error "The feature '~a' was not found!" (car feature)))
			      
			      (unless (filter-feature-default feature-id feature-value feature-property experiment)
				(iterate:collecting (cons feature-id (funcall (transformer experiment) feature-id feature-value feature-property))))))
			  experiment))
	     :key #'car))

(defun filter-feature-default (id value feature-property experiment)
  "Apply some logic to determine if this *INPUT-DATA* should be filtered out (returning *T*)."
  (declare (ignore id value experiment))
  (eq (getf feature-property :type) :ignore))

(defun start-experiment (experiment)
  experiment)

(defun split-examples (data experiment &key (proportions '(5 60 35)))
  "Splits the *DATA-SET* of *EXPERIMENT* into *DEV-SET*, *TRAIN-SET* and *TEST-SET* taking the classification distribution into account.  
*PROPORTIONS* define the expected percentage of examples in *DEV-SET*, *TRAIN-SET* and *TEST-SET*, respectively. The sum of the proportions must be 100."
  (assert (eq (length proportions) 3))
  (assert (eq (reduce #'+ proportions) 100))

  (if (member :temporal (hints experiment))
      (setf (slot-value experiment 'train-set) data)
      (let ((examples-per-class (make-hash-table)))
	(iterate:iterate
	  (iterate:for example in data)
	  (trivial-utilities:aif (gethash (cdr example) examples-per-class)
				 (setf (gethash (cdr example) examples-per-class) (push example it))
				 (setf (gethash (cdr example) examples-per-class) (list example))))
	(iterate:iterate
	  (iterate:for (class examples) in-hashtable examples-per-class)
	  (iterate:iterate
	    (iterate:with len = (length examples))
	    (iterate:for example in (alexandria:shuffle examples))
	    (iterate:for idx from 0)
	    (let ((progress (* (/ idx len) 100)))
	      (cond
		((<= progress (first proportions))
		 (push example (slot-value experiment 'dev-set)))
		((<= (first proportions) progress (second proportions))
		 (push example (slot-value experiment 'train-set)))
		((<= (third proportions) progress)
		 (push example (slot-value experiment 'test-set)))))))

	(setf (slot-value experiment 'dev-set) (alexandria:shuffle (slot-value experiment 'dev-set)))
	(setf (slot-value experiment 'train-set) (alexandria:shuffle (slot-value experiment 'train-set)))
	(setf (slot-value experiment 'test-set) (alexandria:shuffle (slot-value experiment 'test-set))))))

(defun get-feature-set (experiment dataset-source)
  ""
  (declare (type (member :dev :train :test :full) dataset-source))
  (ecase dataset-source
    (:dev
     (dev-set experiment))
    (:train
     (train-set experiment))
    (:test
     (test-set experiment))
    (:full
     (append (dev-set experiment)
	     (train-set experiment)
	     (test-set experiment)))))


;; A simple classification demo
;;
;; data-set contains two-dimensional coordinates, and the classifiaction given states if any dimension leis outside of [-0.5 +0.5]
;;

#|

Visualization map of the classification

1.0   0 0 0 0 0 0 0 0 0 0 0
0.8   0 0 0 0 0 0 0 0 0 0 0
0.6   0 0 0 0 0 0 0 0 0 0 0
0.4   0 0 0 1 1 1 1 0 0 0 0
0.2   0 0 0 1 1 1 1 0 0 0 0
0.0   0 0 0 1 1 1 1 0 0 0 0
-0.2  0 0 0 1 1 1 1 0 0 0 0
-0.4  0 0 0 1 1 1 1 0 0 0 0
-0.6  0 0 0 0 0 0 0 0 0 0 0
-0.8  0 0 0 0 0 0 0 0 0 0 0
-1.0  0 0 0 0 0 0 0 0 0 0 0


|#

(defclass 2d-data (ding:ding)
  ((x :reader x
      :initarg :x
      :type float)
   (y :reader y
      :initarg :y
      :type float)
   (z :reader z
      :initarg :z
      :type float)))


(defun create-dev-dataset ()
  (loop for i upto 10000
     collect (let ((data (make-instance '2d-data
					:id i
					:x (coerce (1- (/ (random 200) 100)) 'double-float)
					:y (coerce (1- (/ (random 200) 100)) 'double-float)
					:z (coerce (1- (/ (random 200) 100)) 'double-float))))
	       (cons data (if (or (< -0.25d0 (x data) +0.25d0)
				  (< -0.25d0 (y data) +0.25d0)
				  (< -0.25d0 (z data) +0.25d0))
			      1
			      0)))))

(defun test ()
  (start-experiment (create-experiment (create-dev-dataset) :hints :classification)))

#|
(defun train-decision-tree (experiment)
  (random-forest:create-decision-tree (get-feature-set experiment :train) (feature-properties (metadata experiment)) :depth 8))

(defun test-decision-tree (experiment decision-tree)
  (/ (iterate:iterate
       (iterate:for elm in (get-feature-set experiment :full))
       (iterate:counting (eq (cdr elm) (random-forest:classify (list (car elm)) decision-tree))))
     (length (get-feature-set experiment :full))))

|#
