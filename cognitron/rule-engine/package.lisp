(uiop:define-package #:rule-engine
  (:documentation "")
  (:use #:common-lisp)
  (:import-from :trivial-utilities #:equals)
  (:export #:request-shutdown #:trigger #:assimilate))
