;;;; Copyright (C) 2019 Eric Diethelm

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(in-package :rule-engine)

(5am:def-suite :rule-engine-tests
  :description "Cognitron Rule Engine tests"
  #+marvin :in
  #+marvin :marvin-tests)
(5am:in-suite :rule-engine-tests)


(defparameter engine nil "")

(defun initialize ()
  (setf engine (make-instance 'rule-engine))
  (setf (slot-value engine 'initialized) t))

(defun assimilate-rule (name antecedent consequent entry-point)
  (assimilate (make-instance 'rule
                             :name name
			     :entry-point entry-point
                             :antecedent antecedent
                             :consequent consequent)
	      engine))

(5am:test assimilate
  (initialize)
  (let ((?X (make-instance 'trivial-variable-bindings:place-holder :name "X"))
        (?Y (make-instance 'trivial-variable-bindings:place-holder :name "Y")))
    (let ((node1 `(:value ,?X ,?Y))
          (node2 `(:lt ,?Y 100 t))
          (node3 `(:gt ,?Y 1000 t))
          (node4 `(:lt ,?Y 1000 t))
          (node5 `(:gt ,?Y 100 t))
          (node6 `(:set (:too-low ,?X t)))
          (node7 `(:set (:too-high ,?X t)))
          (node8 `(:set (:too-low ,?X nil)))
          (node9 `(:set (:too-high ,?X nil))))
      (assimilate-rule "R-1" (list node1 node2) (list node6) ?X)
      (assimilate-rule "R-2" (list node1 node3) (list node7) ?X)
      (assimilate-rule "R-3" (list node1 node4 node5) (list node8) ?X)
      (assimilate-rule "R-4" (list node1 node4 node5) (list node9) ?X)

      (5am:is (eq (length (rules engine)) 4)))))


(5am:test foreward-search
  (initialize)

  (let ((?X (make-instance 'trivial-variable-bindings:place-holder :name "X"))
        (?Y (make-instance 'trivial-variable-bindings:place-holder :name "Y"))
        (obj (make-instance 'ding:ding :id 99)))
    (let ((node1 `(:value ,?X ,?Y))
          (node2 `(:lt ,?Y 100 t))
          (node3 `(:gt ,?Y 1000 t))
          (node4 `(:lt ,?Y 1000 t))
          (node5 `(:gt ,?Y 100 t))
          (node6 `(:set (:too-low ,?X t)))
          (node7 `(:set (:too-high ,?X t)))
          (node8 `(:set (:too-low ,?X nil)))
          (node9 `(:set (:too-high ,?X nil))))
      (assimilate-rule "R-1" (list node1 node2) (list node6) ?X)
      (assimilate-rule "R-2" (list node1 node3) (list node7) ?X)
      (assimilate-rule "R-3" (list node1 node4 node5) (list node8) ?X)
      (assimilate-rule "R-4" (list node1 node4 node5) (list node9) ?X)

      (ding:without-change-notification
        (setf (ding:eigenschaft obj :value) 1230)
        (foreward-search obj engine))

      (5am:is (eq (ding:eigenschaft obj :too-high) t))
      (5am:is (eq (ding:eigenschaft obj :too-low) nil)))))

(5am:test assume
  (initialize)
  (let ((?X (make-instance 'trivial-variable-bindings:place-holder :name "X"))
        (?Y (make-instance 'trivial-variable-bindings:place-holder :name "Y"))
        (obj (make-instance 'ding:ding :id 99)))
    (let ((node1 `(:value ,?X ,?Y))
          (node2 `(:lt ,?Y 100 t))
          (node3 `(:gt ,?Y 1000 t))
          (node4 `(:lt ,?Y 1000 t))
          (node5 `(:gt ,?Y 100 t))

          (assert1 `(:assert (:too-low ,?X)))
          (assert2 `(:assert (:too-high ,?X)))
          (revoke1 `(:revoke (:too-low ,?X)))
          (revoke2 `(:revoke (:too-high ,?X)))
          (asserted1 `(:asserted (:too-low ,?X)))
          (asserted2 `(:asserted (:too-high ,?X)))
          (revoked1 `(:revoked (:too-low ,?X)))
          (revoked2 `(:revoked (:too-high ,?X))))
      (assimilate-rule "R-1" (list node1 node2 revoked1) (list assert1) ?X)
      (assimilate-rule "R-2" (list node1 node3 revoked2) (list assert2) ?X)
      (assimilate-rule "R-3" (list node1 node4 node5 asserted1) (list revoke1) ?X)
      (assimilate-rule "R-4" (list node1 node4 node5 asserted2) (list revoke2) ?X)

      (ding:without-change-notification
        (setf (ding:eigenschaft obj :value) 1200)
        (foreward-search obj engine))

      (5am:is (eq (length (assertions engine)) 1))
      (5am:is (equals (car (assertions engine)) `(:too-high ,obj))))))

(5am:test call
  (initialize)

  (let ((?X (make-instance 'trivial-variable-bindings:place-holder :name "X"))
        (?M (make-instance 'trivial-variable-bindings:place-holder :name "M"))
        ;;(?V (make-instance 'trivial-variable-bindings:place-holder :name "V"))
        (bindings (make-instance 'trivial-variable-bindings:bindings))
	(obj (make-instance 'ding:ding :id 99)))

    (assimilate-rule "R-call"
		     `((:eq 1 1 t))
                     ;; `((:value ,?X ,?V)
                     ;;   (:eq 1 ,?V))
                     `((:call ,?M "cadr" (a v x)))
                     ;;`((:call ,?M '#cadr (a v x))) @TODO Add option to pass a function
                     ?X)

    
      (ding:without-change-notification
        (setf (ding:eigenschaft obj :value) 1)
        (foreward-search obj engine bindings))

    (5am:is (equals (trivial-variable-bindings:bound-variable-value ?M bindings) 'v))))
    
(5am:test backward-search
  (initialize)

  (let ((?M (make-instance 'trivial-variable-bindings:place-holder :name "M"))
        (?V (make-instance 'trivial-variable-bindings:place-holder :name "V"))
        (?X (make-instance 'trivial-variable-bindings:place-holder :name "X"))
        (?Y (make-instance 'trivial-variable-bindings:place-holder :name "Y"))
        (?Z (make-instance 'trivial-variable-bindings:place-holder :name "Z"))
        (?P (make-instance 'trivial-variable-bindings:place-holder :name "P"))
        (eric (make-instance 'ding:ding :id 90))
        (alex (make-instance 'ding:ding :id 91))
        (thelma (make-instance 'ding:ding :id 92))
        (rolf (make-instance 'ding:ding :id 93))
        (bindings (make-instance 'trivial-variable-bindings:bindings)))

    (assimilate-rule "R-Parents"
		     `((:mother ,?X ,?M) (:mother ,?Z ,?M)
                            (:vater ,?X ,?V) (:vater ,?Z ,?V))
                     `((:set (:siblings ,?X ,?Z)))
                     ?X)
    (assimilate-rule "R-Siblings-Gender-M"
		     `((:siblings ,?X ,?Y) (:gender ,?Y :male))
                     `((:set (:brother ,?X ,?Y)))
                     ?X)
    (assimilate-rule "R-Siblings-Gender-F"
		     `((:siblings ,?X ,?Y) (:gender ,?Y :female))
                     `((:set (:sister ,?X ,?Y)))
                     ?X)
    (assimilate-rule ""
		     `((:brother ,?X ,?Y))
                     `((:set (:siblings ,?X ,?Y)))
                     ?X)
    (assimilate-rule "R-Cummutative"
		     `((:siblings ,?X ,?Y))
                     `((:set (:siblings ,?Y ,?X)))
                     ?X)

    (ding:without-change-notification
      (setf (ding:eigenschaft eric :gender) :male)
      (setf (ding:eigenschaft alex :gender) :male)
      (setf (ding:eigenschaft thelma :gender) :female)
      (setf (ding:eigenschaft rolf :gender) :male)

      (setf (ding:eigenschaft eric :mother) thelma)
      (setf (ding:eigenschaft eric :vater) rolf)
      (setf (ding:eigenschaft alex :mother) thelma)
      (setf (ding:eigenschaft alex :vater) rolf)

      (backward-search `(:brother ,eric ,?P) engine bindings))

    (5am:is (equals (trivial-variable-bindings:bound-variable-value ?P bindings) alex))))

;;;; Light control in kitchen
;; R-K1 If luminosity is below 300, then (low-light kitchen) is assumed (also, it is possibly necessary to turn on the light)
;; R-K2 If the light switch is pressed, then (low-light kitchen) and (presence kitchen) are assumed 
;; R-K3 If there is movement in the kitchen, then (presence kitchen) is assumed
;; R-K4 If there is no movement in the kitchen for over 60 seconds and movement was detected in the floor not longer than 60 seconds ago, then the assumed (presence kitchen) is revoked
;; R-K5 If (presence kitchen) and (low-light kitchen) are assumed, then (auto-lights-on kitchen) is assumed
;; R-K6 If (auto-lights-on kitchen) is assumed and (presence kitchen) is not assumed (revoked), then revoke (auto-lights-on kitchen)
;; R-K7 If (auto-lights-on kitchen) is assumed, then set (state light-device-of-kitchen 1) -> component-communication plugin listenes to change notifications on devices
;; R-K8 If (auto-lights-on kitchen) is revoked, then set (state light-device-of-kitchen 0) -> component-communication plugin listenes to change notifications on devices
;; R-K9 If (auto-lights-on kitchen) is assumed and the light switch is pressed, then (auto-lights-on kitchen) is revoked

;;; Constants
;; Kitchen - #[58:HEAD]
;; Kitchen sensors - #[54:HEAD]
;; Switch kitchen - Device 19 Message 580
;; Luminosity kitchen - Device 15 Message 562
;; Relais Kitchen - Device 19 Message 610

#|


;; R-K1
(make-rule "R-K1"
           ((message ?Message #[31:ANY])
            (device ?Message #[54:ANY])
            (value ?Message ?Helligkeit)
            (lt ?Helligkeit 300 T))
           (assume (low-light #[81:HEAD])))

;; R-K2.1
(make-rule  "R-K2.1"
            ((message ?Message #[40:ANY])
             (device ?Message #[58:ANY])
             (value ?Message 1))
            (assume (presence  #[81:HEAD])))

;; R-K2.2
(make-rule  "R-K2.2"
            ((message ?Message #[40:ANY])
             (device ?Message #[58:ANY])
             (value ?Message 1))
            (assume (low-light #[81:HEAD])))


;; R-K3
(make-rule  "R-K3"
            ((message ?Message #[19:ANY])
             (device ?Message #[54:ANY])
             (value ?Message 1))
            (assume (presence  #[81:HEAD])))


;; R-K4
(make-rule  "R-K4"
            ((message ?Message #[19:ANY])
             (device ?Message #[54:ANY])
             (value ?Message 0)
             (erstellungsdatum ?Message ?ts)
             (now ?now)
             (minus ?now ?ts ?delta)
             (lt ?delta 60))
            (revoke (presence  #[81:HEAD])))

;; R-K5
(make-rule  "R-K5"
            ((assumed (presence #[81:HEAD])) (assumed (low-light #[81:HEAD])))
            (assume (auto-lights-on #[81:HEAD])))

;; R-K6
(make-rule  "R-K6"
            ((assumed (auto-lights-on #[81:HEAD])) (revoked (presence #[81:HEAD])))
            (revoke (auto-lights-on #[81:HEAD])))

;; R-K7
(make-rule  "R-K7"
            ((assumed (auto-lights-on #[81:HEAD])) (not (eq (state #[40:HEAD]) 1)))
            (set (state #[40:HEAD]) 1))

;; R-K8
(make-rule  "R-K8"
            ((revoked (auto-lights-on #[81:HEAD])) (not (eq (state #[40:HEAD]) 0)))
            (set (state #[40:HEAD]) 0))

;; R-K9
(make-rule  "R-K9"
            ((assumed (auto-lights-on #[81:HEAD]))
             (message ?Message #[40:ANY])
             (device ?Message #[58:ANY])
             (value ?Message 1))
            (revoke (auto-lights-on #[81:HEAD])))








;; When there is presence of someone in a room, the minimum luminosity of that room must be X, which depends on time of day and possibly other factors
;; When there is noone present in a room, the minimum luminosity of that room is 0
;; When the luminosity in a room falls below a certain threshold, then it is necessary to turn the lights in that room on.
(make-rule  "R-X1"
            ((message ?Lux_Message #[31:ANY])
             (device ?Lux_Message ?Lux_Device)
             (value ?Lux_Message ?Helligkeit)
             (lt ?Helligkeit <threshold for the room> T))
            (assume (necessary (turn-on (room-of ?Lux-Device) light)))) ;; @TODO Das ist noch komplett falsch


;; Wenn die Helligkeit in einem Raum unter X fällt, und Bewegung in dem Raum innerhalb einer Minute detektiert wird, dann schalte das Licht in dem Raum ein.
'((message ?Lux-Message #[31:ANY])
  (device ?Lux-Message ?Lux-Device)
  (value ?Lux-Message ?Helligkeit)

  (klasse ?Raum #[97:ANY])
  (beinhaltet ?Raum ?Lux-Device)

  (lux-threshold ?Raum ?LTh)
  (lt ?Helligkeit ?LTh T)

  (beinhaltet ?Raum ?Mov-Device)

  (klasse ?Mov-Message #[19:ANY])
  (device ?Mov-Message ?Mov-Device)
  (value ?Mov-Message 1)

  (erstellungsdatum ?Lux-Message ?Lux-TS)
  (erstellungsdatum ?Mov-Message ?Mov-TS)

  (minus ?Lux-TS ?Mov-TS ?Delta-TS)
  (abs ?Delta-TS ?Delta-TS-Abs)
  (lt ?Delta-TS-Abs 60 T))





'((:eq (:eigenschaft :klasse ?X) #[2:ANY])
  (:eq (:eigenschaft :message ?X) #[31:ANY])
  (:eq (:eigenschaft :device ?X) ?Z)
  (:lt (:eigenschaft :value ?X) 300)

  (:eq (:eigenschaft :value ?Y) 1)
  (:eq (:eigenschaft :klasse ?Y) #[2:ANY])
  (:eq (:eigenschaft :message ?Y) #[19:ANY]))

|#

