;;;; Copyright (C) 2022 Eric Diethelm

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.


#|

Necessary improvements

1- Assumptions
   > Checking for asserted assumptions might contain unbound variables
     This makes it possible to use assumptions as temporary variables
     e.g. (:assume (:last-movement <kitchen> <10.02.2020 11:37>))
     and then query ((:asserted (:last-movement <kitchen> ?TS))
                     (:now ?TN)
                     (:minus ?TN ?TS ?TDIFF)
                     (:gt ?TDIFF 30 t))

     Open question: Should re-asserting overwrite the existing assumption?

2- Eigenschaften and Rule consequences
   > Eigenschaften should be written just as consequences/propositions are
     This is necessary for the backtracking algorithm
     e.g. (:bruder <eric> ?X)
     This could mean (:eigenschaft <eric> :bruder ?X), but also match a rule on how to identify the brother of someone.

|#

(in-package :rule-engine)

;;--------------------- Class definitions ---------------------

(defclass rule (ding:ding)
  ((name :reader name
         :initarg :name
         :initform ""
         :type string
         :documentation "The name of the rule.")
   (entry-point :reader entry-point
                :initarg :entry-point
                :type trivial-variable-bindings:place-holder
                :initform (error "An entry point variable must be defined.")
                :documentation "The variable used as an entry point to the rule.")
   (antecedent :reader antecedent
               :initarg :antecedent
               :initform nil
               :documentation "The antecedent or premises.")
   (consequent :reader consequent
               :initarg :consequent
               :initform nil
               :documentation "The consequent or conclusion.")
   (world-change :reader world-change
                 :initarg :world-change
                 :initform nil
                 :documentation "The expected change in the world (internal and/or external) of the rule's consequent."))
  (:documentation "The class that defines a rule."))

(defclass rule-engine ()
  ((initialized :reader init-p
                :initform nil)
   (shutdown-requested :reader shutdown-requested
                       :initform nil)
   (rules :accessor rules
          :initform nil)
   (assertions :accessor assertions ;; @REVIEW What if assertions were non-saveable (to DB) objects in Working-Memory?
               :initform nil)
   (goals :accessor goals
          :initform nil)
   (queue :type chanl:bounded-channel
          :reader queue
          :initform (make-instance 'chanl:bounded-channel :size 5)
          :documentation "A message queue to receive triggers to process."))
  (:documentation ""))


;;--------------------- Global variables ---------------------

(defparameter *current-engine* nil "Indicates the current rule-engine being used.")

;;--------------------- Equality checks ---------------------


;;--------------------- Pretty printing ---------------------

(defmethod print-object ((var rule) out)
  (print-unreadable-object (var out :type nil)
    (format out "~A" (name var))))


;;---------------------

(defmethod request-shutdown ((engine rule-engine))
  (setf (slot-value engine 'shutdown-requested) t))


;;---------------------

(defun replace-variables (proposition bindings)
  (iterate:iterate
    (iterate:for elm in proposition)
    (iterate:collecting
        (cond ((consp elm)
               (replace-variables elm bindings))
              ((typep elm 'trivial-variable-bindings:place-holder)
               (trivial-variable-bindings:bound-variable-value elm bindings))
              (t
               elm)))))

(defun factp (proposition)
  (cond ((null proposition)
         nil)
        ((consp (car proposition))
         (and (factp (car proposition))
              (factp (cdr proposition))))
        ((typep (car proposition) 'trivial-variable-bindings:place-holder)
         nil)
        (t t)))

;;---------------------

(defun trigger (obj engine)
  (chanl:send (queue engine) obj))

(defun query-with-eigenschaft-translation (filter bindings)
  (handler-bind
      ;; @TODO Also signal when the eigenschaft is not present in the object.
      ((trivial-filter:query-failure/unknown-filter-method
         (lambda (condition)
           (let ((proposition (replace-variables (trivial-filter:proposition condition) (trivial-filter:bindings condition))))
             (if (typep (second proposition) 'ding:ding)
                 (progn
                   (log:info "Translating for :eigenschaft query of '~a'" proposition)
                   (if (eql (second proposition) (third proposition))
                       (break))
                   (invoke-restart 'trivial-filter:use-value
                                   (let ((bindings (trivial-utilities:clone (trivial-filter:bindings condition))))
                                     ;; When the proposition is unknown we try to rewrite it as an Eigenschaft
                                     (handler-bind
                                         ((working-memory:query-failure/invalid-object-property
                                            (lambda (c)
                                              (declare (ignore c))
                                              (signal 'trivial-filter:query-failure/unknown-filter-method
                                                      :proposition proposition
	                                              :bindings (trivial-filter:bindings condition)
	                                              :continuation (trivial-filter:continuation condition)))))
                                         (trivial-filter:query `(:eigenschaft ,(second proposition)
                                                                          ,(first proposition)
                                                                          ,(third proposition))
                                                           :initial-bindings bindings
                                                           :emit-signal-on-failure t)))))
                 (progn
                   (log:info "Unknown filter method: '~a'" proposition)
                   (signal condition)))))))
    (trivial-filter:query filter :initial-bindings bindings :emit-signal-on-failure t)))

(defun process-trigger/consequent (consequent result engine)
  (unless consequent
    (return-from process-trigger/consequent nil))
  
  (let ((*current-engine* engine))
    (iterate:iterate
      (iterate:while (trivial-continuation:result result))
      (trivial-filter:query consequent :initial-bindings (trivial-continuation:result result))
      (trivial-continuation:cc/continue result))))

(defun process-trigger/propositions (propositions bindings engine)
  (unless propositions
    (return-from process-trigger/propositions nil))

  (let ((*current-engine* engine))
    (handler-bind
        ;; @TODO Wrap this handling of :eigenschaft into an own function 'query', overloading the from trivial-filter
        ;;       But also signal when the eigenschaft is not present in the object.
        ((trivial-filter:query-failure
           (lambda (condition)
             ;;(declare (ignore condition))
             ;;(log:error "Query terminated: ~a" (type-of condition))
             
             ;; @TODO Would this be the location to start backward searching?
             (log:info "Invoking backward search for '~a'." (replace-variables (trivial-filter:proposition condition) (trivial-filter:bindings condition)))
             (invoke-restart 'trivial-filter:use-value (backward-search (replace-variables (trivial-filter:proposition condition) (trivial-filter:bindings condition))
                                                                        engine
                                                                        (trivial-filter:bindings condition)))
             ;;(invoke-restart 'trivial-filter:terminate)
             )))
      (query-with-eigenschaft-translation `(:and ,@(mapcar #'(lambda (proposition)
                                                               (replace-variables proposition bindings))
                                                           propositions))
                                          bindings))))

;; @TODO Some form of limiting the size of filters should be used (cut list by filter cost, by length etc.)

;; 1) Call (trivial-filter:query) on every node with obj, collecting those with non-nil result (result is the binding of variables)
;; 2) For every node collect their matchers (and remove duplicates)
;; 3) Iterate for each matcher until point 7
;; 4) Use matcher mapping table to translate between obj in the initial node (from point 2)
;; 5) Solve each node (consider backtracking) -> here is the mistery!!!
;; 6) If all nodes of a matcher can be solved (there is at least one valid binding for all), push it's consequent to the "todo" stack
;; 7) Execute each consequent on the "todo" stack

(defun solve-antecedents (object engine bindings)
  (remove-if #'(lambda (x) (null (trivial-continuation:result (car x))))
                            (iterate:iterate
                              (iterate:for rule in (rules engine))
                              (iterate:collecting
                                  (list (process-trigger/propositions (antecedent rule)
                                                                      (or bindings
                                                                          (trivial-utilities:aprog1
                                                                              (make-instance 'trivial-variable-bindings:bindings)
                                                                            (setf (trivial-variable-bindings:bound-variable-value
                                                                                   (entry-point rule)
                                                                                   it)
                                                                                  object)))
                                                                      engine)
                                        rule)))))

(defun foreward-search (object engine &optional bindings)
  (let ((results (solve-antecedents object engine bindings)))
    (iterate:iterate
      (iterate:for (result rule) in results)
      (iterate:iterate
        (iterate:while (trivial-continuation:result result))
        (process-trigger/consequent (cons :and (consequent rule)) result engine)
        (trivial-continuation:cc/continue result)))))

;; @TODO Matching must start with an empty bindings and with query already replaced
(defun match (query proposition bindings)
  (when (and (null query) (null proposition))
    (return-from match (values t bindings)))

  (unless (eq (length query) (length proposition))
    (log:trace "Length mismatch")
    (return-from match (values nil nil)))

  (cond ((and (typep (car query) 'trivial-variable-bindings:place-holder)
              (typep (car proposition) 'trivial-variable-bindings:place-holder))
         ;; Do nothing.
         )
        ((typep (car query) 'trivial-variable-bindings:place-holder)
         (let ((var (trivial-variable-bindings:bound-variable-value (car query) bindings)))
           (unless (and (equals var (car query))
                        (eq var (car proposition)))
             (log:trace "Variable unification error: ~a != ~a." var (car proposition))
             (return-from match (values nil nil)))))

        ((typep (car proposition) 'trivial-variable-bindings:place-holder)
         (setf (trivial-variable-bindings:bound-variable-value (car proposition) bindings) (car query)))
        ((not (eq (car query) (car proposition)))
         (log:trace "car mismatch ~a != ~a." (car query) (car proposition))
         (return-from match (values nil nil))))

  (return-from match (match (cdr query) (cdr proposition) bindings)))

(defun backward-search (query engine &optional bindings)
  ;; Collect every consequent that matches the query
  (let ((r (iterate:iterate
             (iterate:for rule in (rules engine))
             (unless (member rule (goals engine)) 
               (let ((bind (trivial-utilities:clone bindings))) ;; @TODO Here a fresh bindings must be created and query fully variable-replaced 
                 (when (some #'(lambda (p)
                                 (match query (if (member (car p) '(:set :assert))
                                                  (cadr p)
                                                  p)
                                        bind))
                             (consequent rule))
                   (iterate:collecting (list rule bind))))))))

    ;;(log:info r)

    (handler-bind
        ;; @TODO Wrap this handling of :eigenschaft into an own function 'query', overloading the from trivial-filter
        ;;       But also signal when the eigenschaft is not present in the object.
        ((trivial-filter:query-failure
           (lambda (c)
             (declare (ignore c))
             ;;(log:error "Query terminated: ~a" (type-of c))
             ;; @TODO Would this be the location to start backward searching?
             (invoke-restart 'trivial-filter:terminate))))
      (let ((*current-engine* engine))
        (let ((p (iterate:iterate
                   (iterate:for (rule bindings) in r)
                   (push rule (goals engine))
                   (iterate:collecting
                    ;; @TODO Handle trivial-filter:query-failure and start a new backward search
                    ;;       And we must also keep track of which rules are being used, as to not get trapped in recursion
                    (process-trigger/propositions (antecedent rule)
                                                  bindings
                                                  engine))
                   (pop (goals engine)))))
          ;;(log:info p)
          ;;(break)
          (when p
            ;; @TODO We need a mapping from 'forward-search' to 'backward-search' worlds and translating back
            (return-from backward-search (car p)))
          
          
          ))))
  
  (make-instance 'trivial-continuation:continuation-result
		 :operation :terminate
		 :result nil
		 :continuation nil))

(defun assimilate (rule engine)
  "Process the given *rule* and create nodes and matchers. This is the naive implementation."
  (declare (type rule rule)
           (type rule-engine engine))
    (push rule (rules engine)))

#|

Concept of FACTS

A fact is a proposition with no variables.
It has no method defined via create-filter-method.
It is eighter true (asserted) or false (retracted or unknown - Closed World Model).

(:assert (:is-dark kitchen)) -> Pushes (:is-dark kitchen) to the list of facts
(:revoke (:is-dark kitchen)) -> Removes (:is-dark kitchen) to the list of facts

(:asserted (:is-dark kitchen)) -> Searches for (:is-dark kitchen) in the list of facts
(:asserted (:is-dark ?where)) -> Searches for lists with first element :is-dark in the list of facts and binds the second element the ?where. This has possible continuation.

|#
