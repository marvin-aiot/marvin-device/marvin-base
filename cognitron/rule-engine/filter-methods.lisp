;;;; Copyright (C) 2022 Eric Diethelm

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.



(in-package :rule-engine)

(trivial-filter:create-filter-method :asserted
  ;; :asserted expects 1 agruments:
  ;;  1. Fact
  (let ((fact (replace-variables (first args) bindings))
        (value bindings))

    ;; *fact* might contain variables, but these must be resolved via *bindings*
    (unless (and (factp fact)  ;; @TODO This is not true!!
                 (member fact (assertions *current-engine*) :test #'equals))
      (setf value nil))

    (trivial-continuation:cc/create-return value nil :operation (if value :combine :terminate))))


(trivial-filter:create-filter-method :revoked
  ;; :revoked expects 1 agruments:
  ;;  1. Fact
  (let* ((fact (replace-variables (first args) bindings))
         (value (cond ((not (factp fact))
                       (log:error "'~a' is not a FACT to be asserted!" fact)
                       nil)
                      ((member fact (assertions *current-engine*) :test #'equals)
                       nil)
                      (t
                       bindings))))

    (trivial-continuation:cc/create-return value nil :operation (if value :combine :terminate))))


(trivial-filter:create-filter-method :assert
  ;; :assert expects 1 agruments:
  ;;  1. Fact
  (let* ((fact (replace-variables (first args) bindings))
         (value (cond ((not (factp fact))
                       (log:error "'~a' is not a FACT to be asserted!" fact)
                       nil)
                      ((member fact (assertions *current-engine*) :test #'equals)
                       (log:error "'~a' was already asserted!" fact)
                       nil)
                      (t
                       ;; @REVIEW Could this also trigger a processing round based on modified assertions?
                       (push fact (assertions *current-engine*))
                       bindings))))

    (trivial-continuation:cc/create-return value nil :operation (if value :combine :terminate))))


(trivial-filter:create-filter-method :revoke
  ;; :revoke expects 1 agruments:
  ;;  1. Fact
  (let* ((fact (replace-variables (first args) bindings))
         (value (cond ((not (factp fact))
                       (log:error "'~a' is not a FACT to be revoked!" fact)
                       nil)
                      ((member fact (assertions *current-engine*) :test #'equals)
                       (log:error "'~a' was not asserted!" fact)
                       nil)
                      (t
                       ;; @REVIEW Could this also trigger a processing round based on modified assertions?
                       (setf (assertions *current-engine*) (delete fact (assertions *current-engine*) :test #'equals))
                       bindings))))

    (trivial-continuation:cc/create-return value nil :operation (if value :combine :terminate))))


(trivial-filter:create-filter-method :assert-silent
  ;; :assume-silent expects 1 agruments:
  ;;  1. Fact
  (let* ((fact (replace-variables (first args) bindings))
        (value nil))

    ;; *fact* might contain variables, but these must be resolved via *bindings*
    (cond ((not (factp fact))
           (log:error "'~a' is not a FACT to be asserted!" fact))
          ((member fact (assertions *current-engine*) :test #'equals)
           ;; @REVIEW Could this also trigger a processing round based on modified assertions?
           (push fact (assertions *current-engine*))
           (setf value bindings)))

    (trivial-continuation:cc/create-return value nil :operation (if value :combine :terminate))))


(trivial-filter:create-filter-method :revoke-silent
  ;; :revoke-silent expects 1 agruments:
  ;;  1. Fact
  (let* ((fact (replace-variables (first args) bindings))
        (value (cond ((not (factp fact))
                      (log:error "'~a' is not a FACT to be revoked!" fact)
                      nil)
                     ((member fact (assertions *current-engine*) :test #'equals)
                      ;; @REVIEW Could this also trigger a processing round based on modified assertions?
                      (setf (assertions *current-engine*) (delete fact (assertions *current-engine*) :test #'equals))
                      bindings))))

    (trivial-continuation:cc/create-return value nil :operation (if value :combine :terminate))))


(trivial-filter:create-filter-method :set
  ;; :set expects 1 agruments:
  ;;  1. eigenschaft expression
  (let* ((expr (replace-variables (first args) bindings))
        (value (cond ((not (factp expr))
                      (log:error "'~a' is not a valid expression to be set!" expr)
                      nil)
                     ((not (and (typep (second expr) 'ding:ding)
                                (eq (length expr) 3)))
                      (log:error "'~a' does not define an eigenschaft to be set!" expr)
                      nil)
                     (t
                      (let ((obj (second expr))
                            (prop (first expr))
                            (val (third expr)))
                        (setf (ding:eigenschaft obj prop) val)
                        bindings)))))

    (trivial-continuation:cc/create-return value nil :operation (if value :combine :terminate))))


(trivial-filter:create-filter-method :create
  ;; :create expects 1 agruments:
  ;;  1. creation expression (as passed to marvin:create-object)
  (let* ((expr (replace-variables (first args) bindings))
         (value (cond ((not (factp expr))
                       (log:error "'~a' is not a valid create expression!" expr)
                       nil)
                      ((not (eq 0 (mod (length expr) 2)))
                       (log:error "'~a' does not define a create expression!" expr)
                       nil)
                      (t
                       (working-memory:create-object expr)
                       bindings))))

    (trivial-continuation:cc/create-return value nil :operation (if value :combine :terminate))))


(trivial-filter:create-filter-method :call
  ;; :call expects n agruments:
  ;;  1. an unbound variable to hold the return value of the function call
  ;;  2. the name of the function to be called
  ;;  3-n. arguments to the function
  (let ((retval (trivial-filter:extract-value (first args) bindings))
        (fun (trivial-filter:extract-value (second args) bindings))
        (args (replace-variables (cddr args) bindings)))
    (let ((value (cond ((not (typep retval 'trivial-variable-bindings:place-holder))
                        (log:error "The return value must be a variable, but it is ~a." retval)
                        nil)
                       ((not (typep fun 'string))
                        (log:error "")
                        nil)
                       ((some #'(lambda (arg) (typep arg 'trivial-variable-bindings:place-holder)) args)
                        (log:error "")
                        nil)
                       (t
                        (let ((pos (position ":" fun :test #'string-equal))
                              (fn nil))

                          (setf fn
                                (if pos
                                    (fboundp (find-symbol (string-upcase (subseq fun 0 pos)) (string-upcase (subseq fun (1+ pos)))))
                                    (fboundp (find-symbol (string-upcase fun)))))

                          (if fn
                              (handler-case (progn
                                              (setf (trivial-variable-bindings:bound-variable-value retval bindings)
                                                    (apply fn args))
                                              bindings)
                                (condition (c)
                                  (log:error "During execution of (~a ~{~a~}) -> ~S" fun args c)
                                  nil))
                              (progn
                                (log:error "'~a' does not designate a function." fun)
                                nil)))))))

      (trivial-continuation:cc/create-return value nil :operation (if value :combine :terminate)))))


#|

(trivial-filter:create-filter-method :start-timer
  ;; :start-timer expects 3 agruments:
  ;; 1. an unbound variable to hold the created timer's id
  ;; 2. the time delay
  ;; 3. the action to be taken when the time delay passes (equivalent to a rule's consequent)
(let ((retval (trivial-filter:extract-value (first args) bindings))
        (delay (trivial-filter:extract-value (second args) bindings))
        (action (trivial-filter:extract-value (third args) bindings)))
  (error "Not implemented yet.")))


(trivial-filter:create-filter-method :start-recurrent-timer
  ;; :start-recurrent-timer expects 3 agruments:
  ;; 1. an unbound variable to hold the created timer's id
  ;; 2. the time delay between repetitions
  ;; 3. the action to be taken when the time delay passes (equivalent to a rule's consequent)
(let ((retval (trivial-filter:extract-value (first args) bindings))
        (delay (trivial-filter:extract-value (second args) bindings))
        (action (trivial-filter:extract-value (third args) bindings)))
  (error "Not implemented yet.")))


(trivial-filter:create-filter-method :cancel-timer
  ;; :cancel-timer expects 1 agruments:
  ;; 1. timer ID
(let ((timer-id (trivial-filter:extract-value (first args) bindings)))
  (error "Not implemented yet."))

(trivial-filter:create-filter-method :set-alarm
  ;; :set-alarm expects 3 agruments:
  ;; 1. an unbound variable to hold the created alarms's id
  ;; 2. the time/date to be triggered
  ;; 3. the action to be taken when the time comes (equivalent to a rule's consequent)
(let ((retval (trivial-filter:extract-value (first args) bindings))
        (time-stamp (trivial-filter:extract-value (second args) bindings))
        (action (trivial-filter:extract-value (third args) bindings)))
  (error "Not implemented yet."))

(trivial-filter:create-filter-method :cancel-alarm
  ;; :cancel-alarm expects 1 agruments:
  ;; 1. alarm ID
(let ((alarm-id (trivial-filter:extract-value (first args) bindings)))
  (error "Not implemented yet."))

|#
