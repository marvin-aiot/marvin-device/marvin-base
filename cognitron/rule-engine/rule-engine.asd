;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-

;;;; Copyright (C) 2019 Eric Diethelm

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(defsystem :rule-engine
  :name "rule-engine"
  :description ""
  :long-description ""
  :version "0.2.0"
  :author "Eric Diethelm <ediethelm@yahoo.com>"
  :licence "GPL-3.0-or-later"
  :in-order-to ((test-op (test-op :rule-engine/test)))
  :depends-on (:log4cl
	       :iterate
	       :chanl
	       :trivial-utilities
	       :trivial-variable-bindings
	       :trivial-monitored-thread
	       :ding
	       :frame
	       :working-memory)
  :components ((:file "package")
	       (:file "rule-engine")
	       (:file "filter-methods")))


(defsystem :rule-engine/test
  :name "rule-engine/test"
  :description "Test cases for Cognitron Rule Engine."
  :long-description ""
  :author "Eric Diethelm <ediethelm@yahoo.com>"
  :licence "GPL-3.0-or-later"
  :depends-on (:rule-engine
	       fiveam)
  :perform (test-op (o s) (uiop:symbol-call :fiveam  '#:run! :rule-engine-tests))
  :components ((:file "test-rule-engine")))

