;;;; Copyright (C) 2019 Eric Diethelm

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(in-package :nearest-neighbors)

(export 'euclidean-distance)
(export 'euclidean-distance-sparse-vector)
(export 'find-gravity-centers)
(export 'k-nn)
(export 'nn-partition)

(defun sparsep (seq)
  (every #'(lambda (x) (and (consp x) (not (consp (cdr x))))) seq))

(deftype sparse-feature-sequence ()
  `(satisfies sparsep))

(deftype feature-vector ()
  `vector)

(deftype sparse-feature-vector ()
  `(and vector
	(satisfies sparsep)))

(deftype feature-list ()
  `list)

(deftype sparse-feature-list ()
  `(and list
	(satisfies sparsep)))

(defun euclidean-distance (seq1 seq2 zooming-factor)
  "Calculate the sinh Euclidean distance of two given (sparse or not) feature vectors or lists."
  (labels ((calc-dist (a b) (expt (sinh (* zooming-factor (- a b))) 2)))
    (cond ((and (typep seq1 'feature-vector) (typep seq2 'feature-vector))
	   (iterate:iterate (iterate:for f1 in-vector seq1)
			    (iterate:for f2 in-vector seq2)
			    (iterate:sum (calc-dist f1 f2))))
	  ((and (typep seq1 'feature-list) (typep seq2 'feature-list))
	   (iterate:iterate (iterate:for f1 in seq1)
			    (iterate:for f2 in seq2)
			    (iterate:sum (calc-dist f1 f2))))
	  ((and (typep seq1 'sparse-feature-vector) (typep seq2 'sparse-feature-vector))
	   (iterate:iterate (iterate:for (f1 . v1) in-vector seq1)
			    (iterate:for (f2 . v2) = (find f1 seq2 :key #'car))
			    (iterate:sum (calc-dist v1 v2))))
	  ((and (typep seq1 'sparse-feature-list) (typep seq2 'sparse-feature-list))
	   (iterate:iterate (iterate:for (f1 . v1) in seq1)
			    (iterate:for (f2 . v2) = (find f1 seq2 :key #'car))
			    (iterate:sum (calc-dist v1 v2))))
	  (t (error "Invalid types.")))))

(defun nn-partition (nn-dist-limit seq &key (distance-fn #'euclidean-distance))
  "Partition each (class #(feature vector)) in @seq by it's smallest distance to any other element in @seq.
   Inverts the order of the elements."
  (log:info "Starting NN partitioning of ~a elements." (length seq))
  (let ((res (loop for fv on seq by #'cdr
		if (< nn-dist-limit
		      (loop for fv2 in (cdr fv)
			 minimizing (funcall distance-fn (second (car fv)) (second fv2))))
		  collect (car fv) into positive
		else
		  collect (car fv) into negative
		end
		  
		finally (return (list positive negative)))))
    
    (log:info "Resulting partitions: ~a/~a" (length (first res)) (length (second res)))
    (return-from nn-partition res)))

(defun k-nn (seq cosmos &key (k 3) (class-key #'car) (feature-key #'cdr) (distance-fn  #'euclidean-distance))
  (declare (type fixnum k))
  
  (unless seq
    (return-from k-nn nil))

  (unless cosmos
    (warn "The variable 'cosmos' is set to nil")
    (return-from k-nn nil))
  
  ;;(mapcar #'second
	  (subseq (sort (iterate:iterate (iterate:for fv in-vector cosmos)
					 (iterate:collect (cons (funcall class-key fv)
								(funcall distance-fn (funcall feature-key fv) seq))))
			#'< :key #'cdr)
		  0 (min k (length cosmos))));)



;;;; Black-Hole algorithmus

;; element: ((1 2 3 4 5 6 7) 1)
;; features: (first element)
;; mass: (second element)

(defun find-gravity-centers (search-space distance-threshold
			     &key (solution-space '()) (previous-changes nil) (distance-fn  #'euclidean-distance) (sort-fn #'>) (sort-key #'first) (feature-key #'first) (mass-key #'second))
  
  (declare (type list search-space solution-space)
	   (type double-float distance-threshold))
  
  (unless search-space
    (return-from find-gravity-centers (values solution-space previous-changes)))

  (push (car search-space) solution-space)
  
  (loop for element in (cdr search-space)
     do
       (let ((element-features (funcall feature-key element)))
	 
	 (declare (type list element)
		  (type vector element-features))
	 
	 (let ((best-match (first (sort (loop for fv in solution-space
					   collect (list (funcall distance-fn (funcall feature-key fv) element-features) fv))
					sort-fn :key sort-key))))	       
	   (if (< (car best-match) distance-threshold)
	       (let* ((solution-mass (funcall mass-key (second best-match)))
		      (element-mass (funcall mass-key element))
		      (denominator (+ solution-mass element-mass))
		      (best-features (funcall feature-key (second best-match))))
		 (log:info "Found new center: ~a < ~a" (car best-match) distance-threshold)
		 (setf (car (member (second best-match) solution-space))
		       (list (map 'vector #'(lambda (x)
						(trivial-utilities:awhen (find (car x) best-features :key #'car)
						  (cons (car x)
							(/ (+ (* solution-mass (cdr x)) (* element-mass (cdr it))) denominator))))
				  (funcall feature-key element))
			     denominator))
		 (setf previous-changes t))
	       (progn
		 (push element solution-space)
		 (setf previous-changes t))))))


  (return-from find-gravity-centers (values (reverse solution-space) previous-changes)))

      
