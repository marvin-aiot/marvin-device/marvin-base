(uiop:define-package #:cognitron-base
  (:documentation "")
  (:use #:common-lisp)
  (:export #:with-learning-inhibited
	   #:learning-inhibited))

