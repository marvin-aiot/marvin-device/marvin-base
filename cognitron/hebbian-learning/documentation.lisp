;;;; Copyright (C) 2019 Eric Diethelm

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(in-package :hebbian-learning)

(defsection @hebbian-learning-manual (:title "Cognitron Hebbian Learning Manual")
  "[![pipeline status](https://gitlab.com/marvin-aiot/marvin/badges/master/pipeline.svg)](https://gitlab.com/marvin-aiot/marvin/commits/master)"
  (@hebbian-learning-description section)
  (@hebbian-learning-license section)
  (@hebbian-learning-contributing section))

(defsection @hebbian-learning-description (:title "Description")
  "")

  (defsection @hebbian-learning-license (:title "License Information")
  "This library is released under GPLv3. Please refer to the [LICENSE](https://gitlab.com/marvin-aiot/marvin/blob/master/LICENSE 'License') to get the full licensing text.")

(defsection @hebbian-learning-contributing (:title "Contributing to this project")
  "Please refer to the [CONTRIBUTING](https://gitlab.com/marvin-aiot/marvin/blob/master/CONTRIBUTING 'Contributing') document for more information.")

