;;;; Copyright (C) 2019 Eric Diethelm

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(in-package :hebbian-learning)

;; Implements Hebbian learning (by time-based association)

(defvar *learning-rate* 1.0)
(defvar *age-coeff* 1.02)                      ; Hit limit at ~30 
(defvar *length-associations-coeff* 1.03)      ; Hit limit at ~24
(defvar *update-cycles-coeff* 1.15)            ; Hit limit at ~5
(defvar *time-delay-coefficient* 1000)         ; Allow +/- PI * 1000 ms


(proclaim '(inline get-hebbian-age))
(defun get-hebbian-age (obj &optional (now (get-universal-time)))
  (- (ding:erstellungsdatum obj) now))

;;;; @TODO Adopt this code. It seems very out-of-date!
(defun hebbian-learning (obj objects-in-memory)
  (declare (type ding:ding obj)
	   (type list objects-in-memory))

  (let ((associations (ding:associations obj))
	(activation-time (ding:activation-time obj)))

    (log:debug "Processing Hebbian learning for ~a (:id ~a)." obj (ding:id obj))

    ;; Calculate the plasticity (modification coefficient) of obj based on update-cycles, age and number of elements in associations

    (flet ((get-activation-difference (obj1)
	     (- activation-time 
		(ding:activation-time obj1))))

      (let ((plasticity (calculate-plasticity obj))
	    (modified nil))
	(loop for mem in objects-in-memory
	   ;;  Calculate the plasticity (modification coefficient) of mem

	   unless (trivial-utilities:equals obj mem)
	   do
	     ;;(log:debug "Learning against ~a (:id ~a)" mem (Marvin.Univers:eigenschaft mem :id))
	     
	     (let ((coeff-a
		    (* plasticity
		       (/
			(1+
			 (cos
			  (max (- pi)
			       (min pi
				    (/ (get-activation-difference mem)
				       *time-delay-coefficient*)))))
			2)))
		   (previous (assoc mem associations :test #'(lambda (x y) (trivial-utilities:equals x y))))
		   (mem-obj-previous (assoc obj (ding:associations mem)
					    :test #'(lambda (x y) (trivial-utilities:equals x y))))
		   (mem-obj-associations (ding:associations mem)))
	      
	       (unless (zerop coeff-a) 
		 (log:debug "New object: ~a~%Associated object: ~a~%Hebbian coefficient: ~a~%Existing association: ~a~%Hebbian association: ~a"
			    (ding:eigenschaft obj :id) (ding:eigenschaft mem :id) coeff-a previous mem-obj-previous)

		 (if previous
		     (setf (second (assoc mem associations :test #'(lambda (x y) (trivial-utilities:equals x y))))
			   (+ coeff-a
			      (second previous)))
		     (push (list mem coeff-a) (ding:associations obj)))

		 (if mem-obj-previous
		     (setf (second (assoc obj mem-obj-associations :test #'(lambda (x y) (trivial-utilities:equals x y))))
			   (+ coeff-a
			      (second mem-obj-previous)))
		     (push (list obj coeff-a) (ding:associations mem)))

		 (setf modified t))))

	(when modified
	  (incf (ding:update-cycles obj))
	  (setf (ding:dirty obj) (logior (ding:dirty obj) 2))))))

  (values))


(defun calculate-plasticity (obj)
  (declare (type ding:ding obj))

  (when (cognitron-base:learning-inhibited)
    (return-from calculate-plasticity 0.0))
  
  (let* ((associations (ding:associations obj))
	 (update-cycles (ding:update-cycles obj))
	 (age (- (ding:erstellungsdatum obj) (get-universal-time))))

    (* *learning-rate*
       (- 1
	  (tanh
	   (-
	    (/
	     (+ (expt *age-coeff* age)
		(expt *length-associations-coeff* (length associations))
		(expt *update-cycles-coeff* update-cycles))
	     3)
	    1))))))
