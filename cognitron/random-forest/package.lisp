(uiop:define-package #:random-forest
  (:documentation "")
  (:use #:common-lisp)
  (:export #:create-decision-tree
	   #:classify
	   #:analyze-tree))
