;;;; Copyright (C) 2019 Eric Diethelm

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(in-package :random-forest)

#|


Input example
((((4611685991406299852 . 0.32d0) (4611685991704465017 . 0.8d0)) . 1)
 (((4611685991406299852 . -0.45d0) (4611685991704465017 . 0.87d0)) . 1)
 (((4611685991406299852 . -1.0d0) (4611685991704465017 . 0.65d0)) . 0)
 (((4611685991406299852 . -0.87d0) (4611685991704465017 . 0.86d0)) . 0)
 (((4611685991406299852 . -0.97d0) (4611685991704465017 . 0.18d0)) . 1)
 (((4611685991406299852 . -0.49d0) (4611685991704465017 . 0.33d0)) . 1))



|#

(defclass leaf ()
  ((classification :reader classification
		   :initarg :classification)
   (confidence :reader confidence
	       :initarg :confidence
	       :type double-float)
   (examples :reader examples
	     :initarg :examples))
  (:documentation ""))

(defmethod print-object ((obj leaf) out)
  (with-slots (classification confidence) obj
    (print-unreadable-object (obj out :type nil)
      (format out "[~A ~A]" classification confidence))))

(defclass decision ()
  ((operation :reader operation
	      :initarg :operation
	      :type function)
   (operator :reader operator
	     :initarg :operator)
   (op-args :reader op-args
	    :initarg :op-args)
   (true-branch :reader true-branch
		:initarg :true-branch
		:type (or decision leaf))
   (false-branch :reader false-branch
		 :initarg :false-branch
		 :type (or decision leaf))
   (entropy :reader entropy
	    :initarg :entropy
	    :type double-float))
  (:documentation ""))

(defmethod print-object ((obj decision) out)
  (with-slots (operator op-args entropy) obj
    (print-unreadable-object (obj out :type nil)
      (format out "[~A ~A ~A]" operator op-args entropy))))

(defparameter *operations* (make-hash-table))

(defmacro define-operation (operator)
  `(lambda (feature-set args)  (,operator (cdar (member (first args) (car feature-set) :key #'first)) (second args))))

(progn
  (setf (gethash '>= *operations*) (define-operation >=))
  (setf (gethash '< *operations*) (define-operation <))
  (setf (gethash '<= *operations*) (define-operation <=))
  (setf (gethash '== *operations*) (define-operation =))
  (setf (gethash '> *operations*) (define-operation >)))  

(defun gini-index (data-set classes)
  ;; In our example *CLASSES* would be '(0 1)
  (if (every #'(lambda (x) (eq (cdr x) (cdar data-set))) (cdr data-set))
      0.0d0
      (reduce #'-
	      (iterate:iterate
		(iterate:with total = (length data-set))
		(iterate:for class in classes)
		(iterate:collecting (let ((p (/ (count-if #'(lambda (x) (eq (cdr x) class)) data-set) total)))
				      (if (> p 0)
					  (* p (log p))
					  0.0d0))))
	      :initial-value 0.0d0)))

(defun calculate-entropy (branch-sets classes)
  (iterate:iterate
    (iterate:for branch in branch-sets)
    (iterate:with total = (reduce #'(lambda (x y) (+ (length y) x)) branch-sets :initial-value 0))
    (iterate:accumulate (* (/ (length branch) total) (gini-index branch classes))
			by #'+ initial-value 0.0d0)))


(defun information-gain (branch-sets classes)
  (- (gini-index (reduce #'append branch-sets :initial-value '()) classes)
     (calculate-entropy branch-sets classes)))


(defun apply-dataset-operation (operation data-set args)
  (trivial-utilities:aif (gethash operation *operations*)
			 (trivial-utilities:partition #'(lambda (x) (funcall it x args)) data-set)
			 (progn
			   (warn "No operation for '~a' found!" operation)
			   nil)))

(defun apply-featureset-operation (operation feature-set args)
  (trivial-utilities:aif (gethash operation *operations*)
			 (funcall it feature-set args)
			 (progn
			   (warn "No operation for '~a' found!" operation)
			   nil)))

(defun quadrant (n min max)
  (+ (- min 0.025d0)
     (* (/ (+ 0.1d0 (- max min)) 4) n)))

(defun find-feature-split (data-set operation feature-id full-range search-range classes)
  "Creates a *DECISION* on the given *FEATURE*. *RANGE* limits the possibilities of the threshold value."
  (car (sort
	(iterate:iterate
	  (iterate:for i from 1 to 3)
	  (iterate:for threshold = (quadrant i (car search-range) (cdr search-range)))
	  (iterate:for res = (apply-dataset-operation operation data-set (list feature-id threshold)))
	  (iterate:collecting (list :ig (information-gain res classes)
				    :fid feature-id
				    :op operation
				    :fr full-range
				    :sr (cons (car search-range)
					      (cdr search-range))
				    :th threshold
				    :tb (car res)
				    :fb (cadr res))))
	#'> :key #'(lambda (x) (getf x :ig)))))

(defun calculate-classification/confidence (dataset)
  (iterate:iterate
    (iterate:with counter = '())
    (iterate:for data in dataset)
    (trivial-utilities:aif (assoc (cdr data) counter)
			   (incf (cdr it))
			   (push (cons (cdr data) 1) counter))
    (iterate:finally (return
		       (let ((class (car (sort counter #'> :key #'cdr)))
			     (total (reduce #'+ counter :key #'cdr)))
			 (values (car class)
				 (coerce (/ (cdr class) total) 'double-float)))))))


(defun split-features (data-set operation classes metadata)
  (iterate:iterate
    (iterate:for fmd in metadata)
    (unless (eq (getf (cdr fmd) :type) :ignore)
      (iterate:collecting (find-feature-split data-set
					      operation
					      (getf (cdr fmd) :id)
					      (cons (getf (cdr fmd) :min-value)
						    (getf (cdr fmd) :max-value))
					      (cons (getf (cdr fmd) :min-value)
						    (getf (cdr fmd) :max-value))
					      classes)))))

(defun find-next-move (data-set metadata classes &key (operation :any) (depth 0) (depth-limit 4))
  (let ((best (car (sort
		    (remove-if #'(lambda (x) (or (null (getf x :tb)) (null (getf x :fb))))
			       (if (eq operation :any)
				   (iterate:iterate
				     (iterate:for (op-key op) in-hashtable *operations*)
				     (iterate:appending
				      (split-features data-set op-key classes metadata)))
				   (split-features data-set operation classes metadata)))
		    #'> :key #'(lambda (x) (getf x :ig))))))
    
    (when best
      (setf best
	    (car (sort
		  (list best
			(find-feature-split data-set (getf best :op) (getf best :fid) (getf best :fr) (cons (car (getf best :sr)) (getf best :th)) classes)
			(find-feature-split data-set (getf best :op) (getf best :fid) (getf best :fr) (cons (getf best :th) (cdr (getf best :sr))) classes))
		  #'> :key #'(lambda (x) (getf x :ig))))))

    (flet ((make-branch (best metadata classes branch depth depth-limit)
	     (if (and (< depth depth-limit)
		      (getf best branch)
		      (> (length (getf best branch)) 1)
		      (not (every #'(lambda (x) (eq (cdr x) (cdar (getf best branch)))) (getf best branch))))
		 (find-next-move (getf best branch)
				 metadata
				 classes
				 :operation operation
				 :depth-limit depth-limit
				 :depth (1+ depth))
		 (multiple-value-bind (classification confidence)
		     (calculate-classification/confidence (getf best branch))
		   (make-instance 'leaf
				  :classification classification
				  :confidence confidence
				  :examples (length (getf best branch)))))))
      
      (let ((tb (when (getf best :tb) (make-branch best metadata classes :tb depth depth-limit)))
	    (fb (when (getf best :fb) (make-branch best metadata classes :fb depth depth-limit))))
	(if (or (null tb) (null fb) (and (typep tb 'leaf) (typep fb 'leaf) (eq (classification tb) (classification fb))))
	    (multiple-value-bind (classification confidence)
		(calculate-classification/confidence data-set)
	      (make-instance 'leaf
			     :classification classification
			     :confidence confidence
			     :examples (length data-set)))	    
	    (make-instance 'decision
			   :operation (gethash (getf best :op) *operations*)
			   :operator (getf best :op)
			   :op-args (list (getf best :fid) (getf best :th))
			   :true-branch tb
			   :false-branch fb
			   :entropy (calculate-entropy (list (getf best :tb) (getf best :fb)) classes)))))))

(defun create-decision-tree (data-set metadata &key (depth 4))
  (let ((classes (iterate:iterate
		   (iterate:for e in data-set)
		   (iterate:adjoining (cdr e)))))
    (find-next-move data-set metadata classes :depth-limit depth)))

(defgeneric apply-decision (feature-set node))

(defmethod apply-decision (feature-set (node leaf))
  (classification node))

(defmethod apply-decision (feature-set (node decision))
  (apply-decision feature-set
		  (if (funcall (operation node) feature-set (op-args node))
		      ;;(apply-featureset-operation (operation node) feature-set (op-args node))
		      (true-branch node)
		      (false-branch node))))

(defun classify (feature-set decision-tree)
  (apply-decision feature-set decision-tree))

(defgeneric analyze-node (node)
  (:documentation "Return values: max-depth min-depth num-leafes num-decisions"))

(defmethod analyze-node ((node decision))
  (multiple-value-bind (max-tb min-tb leafes-tb decisions-tb) (analyze-node (true-branch node))
    (multiple-value-bind (max-fb min-fb leafes-fb decisions-fb) (analyze-node (false-branch node))
      (values (1+ (max max-tb max-fb)) (1+ (min min-tb min-fb)) (+ leafes-tb leafes-fb) (+ 1 decisions-tb decisions-fb)))))

(defmethod analyze-node ((node leaf))
  (values 0 0 1 0))
    

(defun analyze-tree (tree)
  ;; Report maximal depth
  ;; Report minimal depth
  ;; Report number of leafes
  ;; Report number of decisions
  ;;
  (multiple-value-bind (max min leafes decisions) (analyze-node tree)
    (format t "~%Tree analytics~%~%Maximal depth: ~a~%Minimal depth: ~a~%Number of leafes: ~a~%Number of decisions: ~a~%" max min leafes decisions)
    (values max min leafes decisions)))
