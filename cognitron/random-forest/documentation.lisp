;;;; Copyright (C) 2019 Eric Diethelm

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(in-package :random-forest)

(defsection @random-forest-manual (:title "random-forest Manual")
  "[![pipeline status](https://gitlab.com/ediethelm/random-forest/badges/master/pipeline.svg)](https://gitlab.com/ediethelm/random-forest/commits/master)"
  (@random-forest-description section)
  (@random-forest-license section)
  (@random-forest-contributing section))

(defsection @random-forest-description (:title "Description")
  "")

(defsection @random-forest-license (:title "License Information")
  "This library is released under GPLv3. Please refer to the [LICENSE](https://gitlab.com/ediethelm/random-forest/blob/master/LICENSE 'License') to get the full licensing text.")

(defsection @random-forest-contributing (:title "Contributing to this project")
  "Please refer to the [CONTRIBUTING](https://gitlab.com/ediethelm/random-forest/blob/master/CONTRIBUTING 'Contributing') document for more information.")

