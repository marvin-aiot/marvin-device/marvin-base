;;;; Copyright (C) 2019 Eric Diethelm

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(in-package :trivial-utilities)

(defsection @trivial-utilities-manual (:title "Trivial Utilities Manual")
  "[![pipeline status](https://gitlab.com/ediethelm/trivial-utilities/badges/master/pipeline.svg)](https://gitlab.com/ediethelm/trivial-utilities/commits/master)"
  (@trivial-utilities-description section)
  (@commonly-used-types section)
  (@from-onlisp section)
  (@from-pcl section)
  (@other-useful-stuff section)
  (@trivial-utilities-license section)
  (@trivial-utilities-contributing section))

(defsection @trivial-utilities-description (:title "Description")
  "A collection of useful types, functions and macros.")

(defsection @commonly-used-types (:title "Commonly used types")
  (negative-fixnum type)
  (non-positive-fixnum type)
  (non-negative-fixnum type)
  (positive-fixnum type))

(defsection @from-onlisp (:title "From Graham's On Lisp")
  (aif macro)
  (awhen macro)
  (aunless macro)
  (aprog1 macro)
  (blambda macro)
  (alambda macro)
  (flatten generic-function)
  (flatten (method () (t)))
  (mkstr function)
  (symb function)
  (single function)
  (mklist function))

(defsection @from-pcl (:title "From Seibel's Practical Common Lisp")
  (once-only macro))

(defsection @other-useful-stuff (:title "Other useful stuff")
  (partition function)
  "  
Example usage of *PARTITION*  

```lisp
(partition #'evenp '(1 2 3 4 5 6))
((2 4 6) (1 3 5))
```"
  (demultiplex function)
  "  
Example output for *DEMULTIPLEX*  

```lisp
(demultiplex '((a b c) (x) (1 2)))
=> ((A X 1) (A X 2) (B X 1) (B X 2) (C X 1) (C X 2))
```")


(defsection @trivial-utilities-license (:title "License Information")
  "This library is released under GPLv3. Please refer to the [LICENSE](https://gitlab.com/ediethelm/trivial-utilities/blob/master/LICENSE 'License') to get the full licensing text.")

(defsection @trivial-utilities-contributing (:title "Contributing to this project")
  "Please refer to the [CONTRIBUTING](https://gitlab.com/ediethelm/trivial-utilities/blob/master/CONTRIBUTING 'Contributing') document for more information.")

