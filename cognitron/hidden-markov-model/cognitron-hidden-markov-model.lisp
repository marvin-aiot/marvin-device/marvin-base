;;;; Copyright (C) 2019 Eric Diethelm

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(in-package :rule-engine)

(export 'n-gram)

(defun n-gram (seq &key (n 2) (bindings (make-hash-table :test #'equal)))
  (when (< (length seq) n)
    (return-from n-gram (the hash-table bindings)))

  (loop for head on seq
     while (> (length head) n)
     do
       (let ((bind (gethash (subseq head 0 n) bindings)))
	 (log:info "~A -> ~A" (subseq head 0 n) bind)
	 
	 (if bind
	     (setf (gethash (subseq head 0 n) bindings) (1+ bind))
	     (setf (gethash (subseq head 0 n) bindings) 1))))

  (return-from n-gram bindings))



(defun n-gram-probability (n-gram-counts)
  (loop for x being the hash-keys of n-gram-counts
     with l = (hash-table-count n-gram-counts)
     do (setf (gethash x n-gram-counts) (/ (gethash x n-gram-counts) l)))

  (return-from n-gram-probability n-gram-counts))

(defun calc-trans-matrix (audio-file max-dist &key (n 2))
  "Given an array of observation states, calculate the transition probabilities a(i, j) such that:
   1. 0 <= a(i, j) <= 1 for every i and j element of S
   2. sum(a(i, j)) == 1 for every i element of S"
  (n-gram-probability audio-file max-dist :n n))

(defun calc-emission-vector ()
  )

(declaim (inline get-transition-probability))
(defun get-transition-probability (i j A)
  ""
  (declare (type (vector float 2) A)
	   (type fixnum i j))
  (aref A i j))

(declaim (inline get-initial-probability))
(defun get-initial-probability (i P)
  ""
  (declare (type (vector float 1) P)
	   (type fixnum i)
	   (ignore i P))
  )

(declaim (inline get-emission-probability))
(defun get-emission-probability (v i P)
  ""
  (declare (type (vector float 1) P)
	   (type fixnum i v)
	   (ignore v i p))
  )




(defun viterbi-search (O S A B P &aux (Psi '()))
  "The Viterbi search algorithm.
   O: The observation sequence
   S: The hidden state space
   A: The state transition probabilities matrix
   B: The emission probabilities
   P: The initial probabilities set"
  (declare (ignore O S A B P Psi))
  )
