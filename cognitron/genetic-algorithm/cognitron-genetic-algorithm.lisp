;;;; Copyright (C) 2019 Eric Diethelm

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(eval-when (:compile-toplevel :load-toplevel :execute)
  (unless (find-package :Marvin.Cognitron)
    (make-package :Marvin.Cognitron :use '(:common-lisp :Marvin.Utils)))
  
  (in-package :Marvin.Cognitron))



(defun evolute (problem-description population-size)
  (declare (ignore problem-description population-size))
  #|
    problem-descriptor is a DSL to describe the problem, structure of the solution, valid values etc.
    genotype-to-phenotype-fn converts a solution into e.g. a SVM Model

  |#
  )




#|

Fitness function -> a method resulting in a number.
Variables -> 
Methods -> 


Parameters
- Mutation rate
- Crossover rate
- Crossover block size
- Population size


Selection
 by Fitness rank
 by Diversity rank


The population of potential solutions consists of a predefined number of individual solutions.
Each solution is described by one or more Chormosomes, each containing values of the problem's variables. 
The initial population is created by an dedicated algorithm, which might be different for each type of problem.

During each Epoch, the variables of the problem are initialized with the values found in the Chromosome(s), the problem is evaluated and it's fitness determined by a Fitness Function (it's Fitness Value - FV).

After an Epoch, individuals from the population are selected to seed the population of the next Epoch.
This selection process is based on the result of the Fitness Function and also on the Diversity - the variation present in an individual when compared to all the previously selected individuals.



;; Selection of individuals to seed next Epoch
let alpha be the set of individuals forming the population of the next Epoch
let omega be the set of individuals from the last Epoch

let sorted-ff be a list of individuals from the last Epoch sorted by their Fitness Value
let sorted-div be an empty list (of individuals sorted by their Diversity)

remove the first element of sorted-ff and insert it into alpha

do while length of alpha < maximal number of individuals to take from previous Epoch
  do for each individual in sorted-ff
    calculate the individuals Diversity Value (DV) compared to the individuals in alpha and store it in sorted-div

  let target-ff|div be the 2D point defined by {FV of first element of sorted-ff, DV of first element in sorted-div}

  let best-elm be nil

  For every element in sorted-ff and sorted-div
    calculate the distance {FV, DV} to target-ff|div
    if best-elm is nil or the distance is less the the distance of best-elm
      set best-elm = (element distance)

    if FV > distance of best-elm of DV > distance of best-elm
      break loop

  if best-elm if nil
    break loop

  insert best-elm into alpha and remove it from sorted-ff and sorted-div

  
;; Generate new population from seed

|#

;; Data structures:
;; Problem 
;; Solution -> list of values (var1 var2 var3 ... varN)


