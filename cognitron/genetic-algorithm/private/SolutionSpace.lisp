;;;; Copyright (C) 2019 Eric Diethelm

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(eval-when (:compile-toplevel :load-toplevel :execute)
  (unless (find-package :Marvin.Cognitron.GA)
    (make-package :Marvin.Cognitron.GA :use '(:common-lisp :Marvin.Utils)))
  
  (in-package :Marvin.Cognitron.GA))


(defvar test-problem
  (defproblem "Test problem"
      :description ""
      :incubate-fn #'identity ;; Function for 'genotype-to-phenotype conversion'. 
      :fitness-fn #'identity
      :components ((:component "Data Input"
			       :type "DIN"
			       :outputs (data)
			       :configurations ((:configuration "default"
								:constants ()
								:variables ())))

		   (:component "Fitness Output"
			       :type "FTO"
			       :inputs (fitness)
			       :configurations ((:configuration "default"
								:constants ()
								:variables ())))

		   (:component "Split Data (2-way)"
			       :type "SPD2"
			       :inputs (data)
			       :outputs (train-data test-data)
			       :configurations ((:configuration "default"
								:constants ()
								:variables ((:variable "ratio-left/right"
										       :description ""
										       :type :float
										       :range-min -1.0
										       :range-max 1.0)))))
		   (:component "Split Data (3-way)"
			       :type "SPD3"
			       :inputs (data)
			       :outputs (dev-data train-data test-data)
			       :configurations ((:configuration "default"
								:constants ()
								:variables ((:variable "dev-relative"
										       :description ""
										       :type :float
										       :range-min 0.0
										       :range-max 1.0)
									    (:variable "train-relative"
										       :description ""
										       :type :float
										       :range-min 0.0
										       :range-max 1.0)
									    (:variable "test-relative"
										       :description ""
										       :type :float
										       :range-min 0.0
										       :range-max 1.0)))))
		   (:component "Remove Columns"
			       :type "RMC"
			       :inputs (data)
			       :outputs (data)
			       :configurations ((:configuration "default"
								:constants ()
								:variables ((:variable "columns"
										       :description ""
										       :type :list
										       :constraint (member of columns in Data))))))

		   (:component "Replace Values"
			       :type "RPV"
			       :inputs (data)
			       :outputs (data)
			       :configurations ((:configuration "default"
								:constants ()
								:variables ())))
		   (:component "Machine Learning Algorithm"
			       :type "MLA"
			       :inputs ((train-data data) (test-data data))
			       :outputs (model)
			       :configurations ((:configuration "SVM"
								:constants ()
								:variables ((:variable "c"
										       :description ""
										       :type :float)
									    (:variable "gamma"
										       :description ""
										       :type :float)))
						(:configuration "KNN"
								:constants ()
								:variables ((:variable "distance-fn"
										       :description ""
										       :type :any))))) ;;(member '(euclidian manhattan edit)))))))

		   (:component "Evaluate Model"
			       :type "EVM"
			       :inputs ((data test-data) model)
			       :outputs ((model-performance fitness))
			       :configurations ((:configuration "default"
								:constants ()
								:variables ()))))))



