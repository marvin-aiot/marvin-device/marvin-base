;;;; Copyright (C) 2019 Eric Diethelm

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(eval-when (:compile-toplevel :load-toplevel :execute)
  (unless (find-package :Marvin.Cognitron.GA)
    (make-package :Marvin.Cognitron.GA :use '(:common-lisp :Marvin-Utils)))
  
  (in-package :Marvin.Cognitron.GA))


;; @TODO How could we make this work with multiple or tree-structured Chromosomes?
(defun calc-specimen-diversity (specimen-a specimen-b &aux (result 0))
  "Calculates the Diversity Value of two solutions."
  (declare (type specimen specimen-a specimen-b))
  
  (cond ((not (specimen-match-p specimen-a specimen-b))
	 (incf result *diversity-species-mismatch-addition*))
	(t
	 (incf result
	       (iterate:iterate (iterate:for chromosome-a in-vector (specimen-chromosomes specimen-a))
				(iterate:for chromosome-b in-vector (specimen-chromosomes specimen-b))
				(iterate:sum (calc-chromosome-diversity chromosome-a chromosome-b))))))

  (return-from calc-specimen-diversity result))


(defun calc-population-diversity (specimen population)
  "Returns the smallest Diversity Value of the solution to any individual in the population."
  (declare (type specimen specimen))
  
  (when (or (not (listp population)) (null specimen))
    (error "Invalid parameters."))

  (when (null population)
    (return-from calc-population-diversity 0))

  (loop for individual in population
       minimizing (calc-specimen-diversity specimen individual)))

