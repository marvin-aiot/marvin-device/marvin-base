;;;; Copyright (C) 2019 Eric Diethelm

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(in-package :Marvin.Cognitron.GA)

(defun assexual-reproduction (specimen progeny-size problem &aux (pool nil) (brood nil))
  (declare (type specimen specimen)
	   (type (integer 1 *) progeny-size))

  (dotimes (n (* 2 progeny-size))
    (let ((offspring (deep-copy-specimen specimen (get-next-specimen-id)))
	  (modifications 0))
      (declare (type (integer 0 *) modifications))
      
      (loop
	 until (>= modifications 2)
	 do (incf modifications (apply-specimen-mutation offspring problem)))

      (push offspring pool)))

  (log:info (length pool) (length brood))

  (loop
     while (< (length brood) progeny-size)
     do (loop
	   for elm in pool
	   with best = nil
	   with dv = 0
	   while (< (length brood) progeny-size)
	     
	   do (setf dv (min (calc-specimen-diversity elm specimen)
			    (if (null brood)
				most-positive-fixnum
				(calc-population-diversity elm brood))))
	     
	   when (or (null best) (> dv (cdr best)))
	   do (setf best (cons elm dv))
	   end
	     
	   finally (push (car best) brood)))

  (return-from assexual-reproduction brood))


  
      

  
  
