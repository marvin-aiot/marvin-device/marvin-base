;;;; Copyright (C) 2019 Eric Diethelm

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(eval-when (:compile-toplevel :load-toplevel :execute)
  (unless (find-package :Marvin.Cognitron.GA)
    (make-package :Marvin.Cognitron.GA :use '(:common-lisp :Marvin-Utils)))
  
  (in-package :Marvin.Cognitron.GA))


(defstruct problem
  (name "" :type string)
  (description "" :type string) 
  incubate-fn
  fitness-fn
  outputs-to-persist
  components)
  ;;modules)

#|
(defstruct module
  (name "" :type string)
  (description "" :type string) 
  (type "" :type string)
  (problem nil :type (or nil problem))
  components)
|#

(defstruct component
  (name "" :type string)
  (description "" :type string)
  (type "" :type string)
  (problem nil :type (or nil problem))
  ;;(module "" :type string)
  inputs
  outputs
  configurations)

(defstruct component-configuration
  (name "" :type string)
  (description "" :type string)
  operation
  constants
  variables)

(defstruct configuration-variable
  (name "" :type string)
  (description "" :type string)
  (type :undefined :type (member :undefined :float :integer :enum :list :link :any))
  range-min
  range-max
  values)

(defstruct configuration-constant
  (name "" :type string)
  (description "" :type string)
  (type :undefined :type (member :undefined :float :integer :enum :list :link :string :any))
  value)


(defun create-lambda-list (obj keywords)
  (loop
     for keyword in keywords
     when (member keyword obj)
     append (list keyword (second (member keyword obj)))))

(defun get-component-by-type (type problem)
  (loop for elm in (problem-components problem)
     until (string= (component-type elm) type)
     finally (return elm)))

(defun get-configuration-by-name (name component)
  (loop for elm in (component-configurations component)
     until (string= (component-configuration-name elm) name)
     finally (return elm)))

(defun get-component-configuration (component-type configuration-name problem)
  (get-configuration-by-name configuration-name
			     (get-component-by-type component-type problem)))

(defun get-variable-description (component-type configuration-name variable-index problem)
  (nth variable-index (component-configuration-variables
		       (get-component-configuration component-type
						    configuration-name
						    problem))))

(defun get-constant-description (component-type configuration-name constant-index problem)
  (nth constant-index (component-configuration-constants
		       (get-component-configuration component-type
						    configuration-name
						    problem))))


(defun defconst (name &key type value (description ""))
  ""
  (make-configuration-constant :name name
			       :type type
			       :description description
			       :value value))

(defun defvariable (name &key type (description "") (range-min nil) (range-max nil) (values nil))
  ""
  (make-configuration-variable :name name
			       :type type
			       :description description
			       :range-min range-min
			       :range-max range-max
			       :values values))

(defmacro defconfiguration (name &key (constants nil) (variables nil) (description ""))
  ""
  `(make-component-configuration :name ,name
				 :description ,description
				 :constants ,(append '(list) (loop for const in constants
								collect `(defconst ,(second const) ,@(create-lambda-list const '(:type :value :description)))))
				 :variables ,(append '(list) (loop for var in variables
								collect `(defvariable ,(second var) ,@(create-lambda-list var '(:type :description :range-min :range-max :values)))))))

(defmacro defcomponent (name problem &key type configurations (description "") (inputs nil) (outputs nil))
  ""
  `(make-component :name ,name
		  :type ,type
		  :problem ,problem
		  :description ,description
		  :inputs ',inputs
		  :outputs ',outputs
		  :configurations ,(append '(list) (loop for cnf in configurations
				     collect `(defconfiguration ,(second cnf) ,@(create-lambda-list cnf '(:constants :variables :description)))))))

#|
(defun defmodule (name problem &key type components (description ""))
  ""
  (aprog1
   (make-module :name name
		:problem problem
		:type type
		:description description
		:components nil)

   (setf (module-components it)
	 (loop for cmp in components
	    collect `(defcomponent ,(second cmp) ,it ,@(create-lambda-list cmp '(:type :configurations :inputs :outputs :description)))))))
|#

(defmacro defproblem (name &key incubate-fn fitness-fn outputs-to-persist components (description ""))
  ""
;;  (let ((it (gensym)))
    `(aprog1
       (make-problem :name ,name
			      :description ,description
			      :incubate-fn ,incubate-fn
			      :fitness-fn ,fitness-fn
			      :outputs-to-persist ,outputs-to-persist
			      :components nil)
       (setf (problem-components it)
	     ,(append '(list) (loop for cmp in components
				 ;;collect `(defcomponent ,(second cmp) it ,@(create-lambda-list cmp '(:type :configurations :inputs :outputs :description)))
				 collect `(defcomponent ,(second cmp) it ,@(create-lambda-list cmp '(:type :configurations :inputs :outputs :description))))))))


#|

(defun get-module-by-type (type problem)
  (loop for elm in (problem-modules problem)
     until (string= (module-type elm) type)
     finally (return elm)))

(defun get-component-configuration (module-type component-type configuration-name problem)
  (get-configuration-by-name configuration-name
			     (get-component-by-type component-type
						    (get-module-by-type module-type problem))))

(defun get-variable-description (module-type component-type configuration-name variable-index problem)
  (nth variable-index (component-configuration-variables
		       (get-component-configuration module-type
						    component-type
						    configuration-name
						    problem))))

(defun get-constant-description (module-type component-type configuration-name constant-index problem)
  (nth constant-index (component-configuration-constants
		       (get-component-configuration module-type
						    component-type
						    configuration-name
						    problem))))
|#
