;;;; Copyright (C) 2019 Eric Diethelm

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(in-package :Marvin.Cognitron.GA)


(defvar *num-specimen* 0)
(defun get-next-specimen-id ()
  (declare (type (integer 0 *) *num-specimen*))
  (incf *num-specimen*))

(defparameter *max-num-parents* 10)


(defparameter *diversity-species-mismatch-addition* 10000)
(defparameter *diversity-gene-mismatch-addition* 100)
(defparameter *diversity-allele-constants-mismatch-addition* 50)
(defparameter *diversity-allele-constant-value-mismatch-addition* 20)
(defparameter *diversity-allele-variable-value-mismatch-addition* 10)


(defparameter *mutation-half-chance* 5)
(defparameter *mutation-value-modification-limit* 25)

(defparameter *temperature* 155)