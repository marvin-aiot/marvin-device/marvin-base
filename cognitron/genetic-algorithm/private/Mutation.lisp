;;;; Copyright (C) 2019 Eric Diethelm

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(eval-when (:compile-toplevel :load-toplevel :execute)
  (unless (find-package :Marvin.Cognitron.GA)
    (make-package :Marvin.Cognitron.GA :use '(:common-lisp :Marvin-Utils)))
  
  (in-package :Marvin.Cognitron.GA))




(defun apply-value-mutation (var var-desc)
  (log:info "Applying mutation on value ~a~%~a" var var-desc)

  (unless (member (configuration-variable-type var-desc) '(:integer :float))
    (return-from apply-value-mutation var))
  
  (if (> (random 100) 50)
      (+ var (/ (* var *mutation-value-modification-limit* *temperature* (random 100)) 1000000))
      (- var (/ (* var *mutation-value-modification-limit* *temperature* (random 100)) 1000000))))

(defun apply-allele-mutation (allele problem &aux (mutations 0))
  ""
  (declare (type allele allele))
  ;; @TODO Should we keep track of what was changed of at least that something was changed?
  ;; @TODO2 At the moment only variables of type number are modified.
  (loop
     for variable on (allele-variables allele)
     and n = 0 then (1+ n)
     and rc = (random 100) then (random 100)

     when (or (< rc *mutation-half-chance*) (> rc (- 100 *mutation-half-chance*)))
     do (setf (car variable) (apply-value-mutation (car variable) (get-variable-description (allele-type allele) (allele-configuration allele) n problem)))
       (incf mutations))

  (return-from apply-allele-mutation mutations))

(defun apply-gene-mutation (gene problem)
  (declare (type gene gene))
  (apply-allele-mutation (gene-locus gene) problem))

(defun apply-chromosome-mutation (chromosome problem)
  (declare (type chromosome chromosome))
  (iterate:iterate (iterate:for gene in-vector (chromosome-genes chromosome))
		   (iterate:sum (apply-gene-mutation gene problem))))


(defun apply-specimen-mutation (specimen problem)
  (declare (type specimen specimen))
  (iterate:iterate (iterate:for chromosome in-vector (specimen-chromosomes specimen))
     (iterate:sum (apply-chromosome-mutation chromosome problem))))
