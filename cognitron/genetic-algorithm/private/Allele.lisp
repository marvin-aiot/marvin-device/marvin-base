;;;; Copyright (C) 2019 Eric Diethelm

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(eval-when (:compile-toplevel :load-toplevel :execute)
  (unless (find-package :Marvin.Cognitron.GA)
    (make-package :Marvin.Cognitron.GA :use '(:common-lisp :Marvin-Utils)))
  
  (in-package :Marvin.Cognitron.GA))

(deftype dominance () '(member dominant recessive)) 

(defstruct allele
  (type nil :type string)                               ; e.g. "MLA"
  (configuration nil :read-only t)                      ; e.g. "KNN"
  (gene nil :read-only t)
  (dominance 'recessive :type dominance :read-only t)
  (constants nil :type list :read-only t)
  (variables nil :type list))

(defun deep-copy-allele (allele)
  (make-allele :type (allele-type allele)
	       :configuration (allele-configuration allele)
	       :gene (allele-gene allele)
	       :dominance (allele-dominance allele)
	       :constants (copy-list (allele-constants allele))
	       :variables (copy-list (allele-variables allele))))
  
(defstruct gene
  (type nil :type string)
  (chromosome nil :read-only t) ;; Wanted to set :type chromosome, but chromosome is not defined yet..
  (locus nil :type (or null allele)))

(defun deep-copy-gene (gene)
  (make-gene :type (gene-type gene)
	     :chromosome (gene-chromosome gene)
	     :locus (deep-copy-allele (gene-locus gene))))
   
(defun genes-match-p (gene-a gene-b)
  ""
  (declare (type gene gene-a gene-b))
  (string= (gene-type gene-a) (gene-type gene-b)))

(defun genes-homozygote-p (gene-a gene-b)
  ""
  (declare (type gene gene-a gene-b))
  (and (genes-match-p gene-a gene-b)
       (not (null (gene-locus gene-a)))
       (not (null (gene-locus gene-b)))
       (eq (allele-dominance (gene-locus gene-a))
	   (allele-dominance (gene-locus gene-b)))))

(defun calc-allele-diversity (allele-a allele-b &aux (diversity-value 0.0))
  ""
  (declare (type allele allele-a allele-b))

  (cond ((not (string= (allele-type allele-a) (allele-type allele-b)))
	 (incf diversity-value *diversity-gene-mismatch-addition*))
	((/= (length (allele-constants allele-a)) (length (allele-constants allele-b)))
	 (incf diversity-value *diversity-allele-constants-mismatch-addition*))
	(t
	 (flet ((calc-value-diversity (values-a values-b mismatch-addition handle-numbers-as-non-numbers &aux (dv 0.0))
		  (loop
		     for var-in-a in values-a
		     for var-in-b in values-b
		     do
		       (incf dv
			     (cond ((and (not handle-numbers-as-non-numbers) (numberp var-in-a) (numberp var-in-b) (not (zerop (+ var-in-a var-in-b))))
				    (expt (/ (- var-in-a var-in-b) (+ var-in-a var-in-b)) 2))
				   ((and (numberp var-in-a) (numberp var-in-b))
				    (abs (- var-in-a var-in-b))) ;; @TODO Verify this!
				   ((not (equal var-in-a var-in-b))
				    mismatch-addition)
				   (t 0)))

		     finally (return dv))))
	   
	   (incf diversity-value (calc-value-diversity (allele-constants allele-a) (allele-constants allele-b) *diversity-allele-constant-value-mismatch-addition* t))
	   (incf diversity-value (calc-value-diversity (allele-variables allele-a) (allele-variables allele-b) *diversity-allele-variable-value-mismatch-addition* nil)))))

	   (return-from calc-allele-diversity diversity-value))

	   

(defun get-variable-by-name (variable-name allele)
  (declare (type string variable-name)
	   (type allele allele))

  (loop
     for var in (allele-variables allele)
     when (string= variable-name (car var))
     return (cdr var)))

(defun (setf get-variable-by-name) (value variable-name allele)
  (declare (type string variable-name)
	   (type allele allele))

  (loop
     for var in (allele-variables allele)
     when (string= variable-name (car var))
     do (progn
	  (setf (cdr var) value)
	  (return value))))
