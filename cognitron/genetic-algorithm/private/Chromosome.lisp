;;;; Copyright (C) 2019 Eric Diethelm

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(eval-when (:compile-toplevel :load-toplevel :execute)
  (unless (find-package :Marvin.Cognitron.GA)
    (make-package :Marvin.Cognitron.GA :use '(:common-lisp :Marvin-Utils)))
  
  (in-package :Marvin.Cognitron.GA))


(defstruct chromosome
  (type "" :type string :read-only t)
  specimen
  module
  (ascendent-solution-id 0 :type fixnum)
  (genes nil :type (vector gene)))

(defun deep-copy-chromosome (chromosome ascendent-solution-id)
  (make-chromosome :type (chromosome-type chromosome)
		   :ascendent-solution-id ascendent-solution-id
		   :genes (make-array (length (chromosome-genes chromosome))
					      :element-type 'gene
					      :initial-contents (iterate:iterate
								   (iterate:for gene in-vector (chromosome-genes chromosome))
								   (iterate:collect (deep-copy-gene gene))))))

(defun chromosomes-match-p (chromosome-a chromosome-b)
  (every #'genes-match-p (chromosome-genes chromosome-a) (chromosome-genes chromosome-b)))

(defun calc-chromosome-diversity (chromosome-a chromosome-b)
    ""
    (declare (type chromosome chromosome-a chromosome-b))

    (reduce #'+ (mapcar #'(lambda (gene-a gene-b) (calc-allele-diversity (gene-locus gene-a) (gene-locus gene-b)))
			(chromosome-genes chromosome-a)
			(chromosome-genes chromosome-b)))
    
    (iterate:iterate (iterate:for gene-a in-vector (chromosome-genes chromosome-a))
		   (iterate:for gene-b in-vector (chromosome-genes chromosome-b))
		   (iterate:sum (calc-allele-diversity (gene-locus gene-a) (gene-locus gene-b)))))


(defun get-gene-by-type (gene-type chromosome)
  (declare (type string gene-type)
	   (type chromosome chromosome))

  (iterate:iterate
     (iterate:for gene in-vector (chromosome-genes chromosome))
     (when (string= (gene-type gene) gene-type)
       (return gene))))
