;;;; Copyright (C) 2019 Eric Diethelm

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(eval-when (:compile-toplevel :load-toplevel :execute)
  (unless (find-package :Marvin.Cognitron.GA)
    (make-package :Marvin.Cognitron.GA :use '(:common-lisp :Marvin.Utils)))
  
  (in-package :Marvin.Cognitron.GA))

		       
;; Create some Alleles

(defparameter test-allele-0  (make-allele :dominance 'dominant  :type "TST1" :constants '(a b c 1 2 3) :variables '(1 2 3 4)))
(defparameter test-allele-1  (make-allele :dominance 'dominant  :type "TST1" :constants '(a b c 1 2 3) :variables '(1 1 3 4)))
(defparameter test-allele-2  (make-allele :dominance 'recessive :type "TST1" :constants '(e f g 2 3 4) :variables '(1 2 3 4)))
(defparameter test-allele-3  (make-allele :dominance 'dominant  :type "TST1" :constants '(e f g 1 2 3) :variables '(1 2 3 4)))
(defparameter test-allele-4  (make-allele :dominance 'recessive :type "TST2" :constants '() :variables '(1 1 1 1)))
(defparameter test-allele-5  (make-allele :dominance 'recessive :type "TST2" :constants '() :variables '(1 2 1 2)))
(defparameter test-allele-6  (make-allele :dominance 'dominant  :type "TST2" :constants '() :variables '(2 2 2 2)))
(defparameter test-allele-7  (make-allele :dominance 'recessive :type "TST2" :constants '() :variables '(2 1 2 1)))
(defparameter test-allele-8  (make-allele :dominance 'dominant  :type "TST3" :constants '() :variables '(3 2 1 2)))
(defparameter test-allele-9  (make-allele :dominance 'dominant  :type "TST3" :constants '() :variables '(2 3 1 2)))
(defparameter test-allele-10 (make-allele :dominance 'recessive :type "TST4" :constants '() :variables '(3 3 1 3)))
(defparameter test-allele-11 (make-allele :dominance 'recessive :type "TST4" :constants '() :variables '(3 4 2 3)))
(defparameter test-allele-12 (make-allele :dominance 'dominant  :type "TST1" :constants '() :variables '(1 2 3 2)))
(defparameter test-allele-13 (make-allele :dominance 'recessive :type "TST1" :constants '() :variables '(4 3 4 1)))
(defparameter test-allele-14 (make-allele :dominance 'dominant  :type "TST2" :constants '() :variables '(3 4 2 4)))
(defparameter test-allele-15 (make-allele :dominance 'dominant  :type "TST2" :constants '() :variables '(1 2 1 4)))
(defparameter test-allele-16 (make-allele :dominance 'recessive :type "TST3" :constants '() :variables '(3 4 1 2)))
(defparameter test-allele-17 (make-allele :dominance 'recessive :type "TST3" :constants '() :variables '(4 3 2 1)))

;; Create some Genes

(defparameter test-gene-0  (make-gene :type "TST1" :locus test-allele-0))
(defparameter test-gene-1  (make-gene :type "TST1" :locus test-allele-1))
(defparameter test-gene-2  (make-gene :type "TST1" :locus test-allele-2))
(defparameter test-gene-3  (make-gene :type "TST1" :locus test-allele-3))
(defparameter test-gene-4  (make-gene :type "TST2" :locus test-allele-4))
(defparameter test-gene-5  (make-gene :type "TST2" :locus test-allele-5))
(defparameter test-gene-6  (make-gene :type "TST2" :locus test-allele-6))
(defparameter test-gene-7  (make-gene :type "TST2" :locus test-allele-7))
(defparameter test-gene-8  (make-gene :type "TST3" :locus test-allele-8))
(defparameter test-gene-9  (make-gene :type "TST3" :locus test-allele-9))
(defparameter test-gene-10 (make-gene :type "TST4" :locus test-allele-10))
(defparameter test-gene-11 (make-gene :type "TST4" :locus test-allele-11))
(defparameter test-gene-12 (make-gene :type "TST1" :locus test-allele-12))
(defparameter test-gene-13 (make-gene :type "TST1" :locus test-allele-13))
(defparameter test-gene-14 (make-gene :type "TST2" :locus test-allele-14))
(defparameter test-gene-15 (make-gene :type "TST2" :locus test-allele-15))
(defparameter test-gene-16 (make-gene :type "TST3" :locus test-allele-16))
(defparameter test-gene-17 (make-gene :type "TST3" :locus test-allele-17))


;; Create some Chromosomes

(defparameter test-chromosome-0 (make-chromosome :genes (make-array 3 :element-type 'gene
								    :initial-contents (list test-gene-0 test-gene-4 test-gene-8))
						 :ascendent-solution-id 1
						 :type "CH01"))

(defparameter test-chromosome-1 (make-chromosome :genes (make-array 3 :element-type 'gene
								    :initial-contents (list test-gene-1 test-gene-5 test-gene-9))
						 :ascendent-solution-id 1
						 :type "CH02"))

(defparameter test-chromosome-2 (make-chromosome :genes (make-array 3 :element-type 'gene
								    :initial-contents (list test-gene-2 test-gene-6 test-gene-10))
						 :ascendent-solution-id 2
						 :type "CH03"))

(defparameter test-chromosome-3 (make-chromosome :genes (make-array 3 :element-type 'gene
								    :initial-contents (list test-gene-3 test-gene-7 test-gene-11))
						 :ascendent-solution-id 2
						 :type "CH04"))

(defparameter test-chromosome-4 (make-chromosome :genes (make-array 3 :element-type 'gene
								    :initial-contents (list test-gene-12 test-gene-14 test-gene-16))
						 :ascendent-solution-id 3
						 :type "CH01"))

(defparameter test-chromosome-5 (make-chromosome :genes (make-array 3 :element-type 'gene
								    :initial-contents (list test-gene-13 test-gene-15 test-gene-17))
						 :ascendent-solution-id 3
						 :type "CH02"))

;; Create some Solutions


(defparameter test-specimen-0
  (make-specimen :id 1
		 :chromosomes (make-array 2
					  :element-type 'chromosome
					  :initial-contents (list test-chromosome-0 test-chromosome-1))))

(defparameter test-specimen-1
  (make-specimen :id 2
		 :chromosomes (make-array 2
					  :element-type 'chromosome
					  :initial-contents (list test-chromosome-2 test-chromosome-3))))

(defparameter test-specimen-2
  (make-specimen :id 3
		 :chromosomes (make-array 2
					  :element-type 'chromosome
					  :initial-contents (list test-chromosome-4 test-chromosome-5))))

(log:info "The specimens 0 and 1 are~a compatible." (if (specimen-match-p test-specimen-0 test-specimen-1) "" " not"))
(log:info "The specimens 0 and 2 are~a compatible." (if (specimen-match-p test-specimen-0 test-specimen-2) "" " not"))
(log:info "The specimens 1 and 2 are~a compatible." (if (specimen-match-p test-specimen-1 test-specimen-2) "" " not"))
(terpri)

(let ((dv01 (calc-allele-diversity test-allele-0 test-allele-1))
      (dv02 (calc-allele-diversity test-allele-0 test-allele-2))
      (dv12 (calc-allele-diversity test-allele-1 test-allele-2))
      (dv23 (calc-allele-diversity test-allele-2 test-allele-3)))
  (log:info " Allele Diversity Value for alleles 0 and 1: ~a" dv01)
  (log:info " Allele Diversity Value for alleles 0 and 2: ~a" dv02)
  (log:info " Allele Diversity Value for alleles 1 and 2: ~a" dv12)
  (log:info " Allele Diversity Value for alleles 2 and 3: ~a" dv23)
  (terpri))

(let ((dv01 (calc-specimen-diversity test-specimen-0 test-specimen-1))
      (dv02 (calc-specimen-diversity test-specimen-0 test-specimen-2))
      (dv12 (calc-specimen-diversity test-specimen-1 test-specimen-2)))
  (log:info " Specimen Diversity Value for specimens 0 and 1: ~a" dv01)
  (log:info " Specimen Diversity Value for specimens 0 and 2: ~a" dv02)
  (log:info " Specimen Diversity Value for specimens 1 and 2: ~a" dv12)
  (terpri))


;; @TODO Is this necessary? Could this be a specific gene?
(defun simple-test-fitness-fn (result expected)
  (unless (eq (length result) (length expected))
    (return-from simple-test-fitness-fn 0.0))
  
  (return-from simple-test-fitness-fn
    (- 100
       (loop
	  for val in result
	  for ex in expected
	  summing (abs (- val ex))))))

(defun simple-test-incubator (specimen)
  (declare (type specimen specimen)
	   (ignore specimen))

  ;;#'(lambda (data)
  ;;  (progn
  ;;    ()))
  )

(proclaim '(inline get-random))
(defun get-random (list)
  (nth (random (1- (length list))) list))

(defun get-possile-configurations (component)
  (declare (type component component))
  (loop
     for cnf in (component-configurations component)
     collect (component-configuration-name cnf)))

(defun find-valid-link (component problem)
  (declare (ignore component problem)))

(defun make-gene-from-problem (component problem configuration)
  (declare (type problem problem)
	   (type component component)
	   (type component-configuration configuration))

  (let ((comp-type (component-type component)))
    (aprog1 (make-gene :type comp-type
		       :locus nil)
      (setf (gene-locus it)
	    (make-allele :type comp-type
			 :configuration configuration
			 :gene it
			 :constants (loop
				       for cons in (component-configuration-constants configuration)
				       collect (cons (configuration-constant-name cons)
						     (configuration-constant-value cons)))
			 :variables (loop
				       for var in (component-configuration-variables configuration)
				       collect (cons (configuration-variable-name var)
						     (ccase (configuration-variable-type var)
						       ((:integer :float)
							(+ (random (- (configuration-variable-range-max var)
								      (configuration-variable-range-min var)))
							   (configuration-variable-range-min var)))
						       (:enum
							(get-random (configuration-variable-values var)))
						       (:list (configuration-variable-values var))
						       (:link (find-valid-link component problem))))))))))


(defun make-possible-gene-alleles (component problem)
  (declare (type problem problem)
	   (type component component))

  (mapcar #'(lambda (conf) (make-gene-from-problem component problem conf)) (component-configurations component)))

(defun make-possible-chromsome-genes (problem)
  (declare (type problem problem))

  (mapcar #'(lambda (comp) (make-possible-gene-alleles comp problem)) (problem-components problem)))


(defun check-operation-matches-inputs (operation component)
  "Check that operation has exactliy one argument for each input declared in the component, plus the one (this) as the last argument.
   &key, &optional and &aux are not allowed."
  (eq (1+ (length (component-inputs component)))
      (length (sb-introspect:function-lambda-list operation))))

(defun make-initial-population (problem population-size)
  (declare (ignore problem population-size))
  )

(defparameter simple-test-problem
  (defproblem "Simple test problem"
      :description ""
      :incubate-fn #'simple-test-incubator
      :fitness-fn #'simple-test-fitness-fn
      :outputs-to-persist '(("TOAO" . value))
      :components ((:component "Data Input"
			       :type "DIN"
			       :description ""
			       :outputs (data)
			       :configurations ((:configuration "default"
								:description ""
								:constants ()
								:variables ())))

		   (:component "Fitness Output"
			       :type "FTO"
			       :description ""
			       :inputs (fitness)
			       :configurations ((:configuration "default"
								:description ""
								:constants ()
								:variables ())))

		   (:component "The one and only"
			       :type "TOAO"
			       :description ""
			       :inputs (data)
			       :outputs (value)
			       :configurations ((:configuration "default"
								:description ""
								:constants ((:constant "n"
										       :description ""
										       :type :integer
										       :value 12))
								:variables ((:variable "a"
										       :description ""
										       :type :integer
										       :range-min 0
										       :range-max 10)))))
		   (:component "This has two configurations"
			       :type "2C"
			       :description ""
			       :inputs (data)
			       :outputs (value)
			       :operation #'(lambda (data this) (get-variable-by-name "a" this))
			       :configurations ((:configuration "A1"
								:description ""
								:constants ((:constant "n"
										       :description ""
										       :type :integer
										       :value 12))
								:variables ((:variable "a"
										       :description ""
										       :type :integer
										       :range-min 0
										       :range-max 100)))
						(:configuration "A2"
								:description ""
								:constants ((:constant "n"
										       :description ""
										       :type :integer
										       :value 6))
								:variables ((:variable "b"
										       :description ""
										       :type :integer
										       :range-min 100
										       :range-max 1000)
									    (:variable "c"
										       :description ""
										       :type :integer
										       :range-min 1001
										       :range-max 2000))))))))


(defparameter simple-test-gene (make-gene-from-problem
				(get-component-by-type "TOAO" simple-test-problem)
				simple-test-problem
				(get-configuration-by-name "default" (get-component-by-type "TOAO" simple-test-problem))))


(defun prepare-component-combination-matrix (problem)
  (declare (type problem problem))

  ;; For every component of the problem, calculate a data flow probability matrix based on the declared inputs and outputs of the components

  ;; Input/Output format is
  ;; (data-description*)
  ;; data-description -> (alias+)

  ;; Examples
  ;; 1. (data model) -> two connections, one called 'data', the other 'model'
  ;; 2. ((data test-data)) -> one connection called either 'data' or 'test-data'

  ;; Output connections have high probability to match to equally named input connections of other components. Differently named connections
  ;; have a low probability to match.

  (loop
     for input-component in (problem-components problem)
     collect (list input-component
		   (loop
		      for input in (component-inputs input-component)
		      for n = 0 then (1+ n)
		      collect (list n
				    (loop
				       for output-component in (problem-components problem)
				       unless (eq input-component output-component)
				       collect (loop
						  for output in (component-outputs output-component)
						  for m = 0 then (1+ m)
						  collect (list (if (intersection (mklist input) (mklist output))
								    1
								    0)
								output-component m))
					 
							  )))))

  ;; Expected output
  ;; ((component-a (input-1 (0.85 component-b output-1)
  ;;                        (0.10 component-c output-1)
  ;;                        (0.05 component-b output-2))
  ;;               (input-2 (0.50 component-c output-2)
  ;;                        (0.35 component-e output-1)
  ;;                        (0.15 component-d output-2)))
  ;;  (component-b (input-1 (0.55 component-b output-1)
  ;;                        (0.25 component-c output-1)
  ;;                        (0.20 component-b output-2))
  ;;               (input-2 (0.60 component-c output-2)
  ;;                        (0.45 component-e output-1)
  ;;                        (0.15 component-d output-2))))
  )

(defvar simple-test-data
  '((2 1) (3 1) (6 3) (4 2) (10 5) (12 6) (8 4) (14 7)))
