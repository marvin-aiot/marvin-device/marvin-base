;;;; Copyright (C) 2019 Eric Diethelm

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(eval-when (:compile-toplevel :load-toplevel :execute)
  (unless (find-package :Marvin.Cognitron.GA)
    (make-package :Marvin.Cognitron.GA :use '(:common-lisp :Marvin.Utils)))
  
  (in-package :Marvin.Cognitron.GA))


;; @TODO Rename file to Speciment.lisp


(defun chromosome-pair-p (obj)
  (and (consp obj)
       (typep (car obj) 'chromosome)
       (typep (cdr obj) 'chromosome)))

(defun make-chromosome-pair (chromosome-a chromosome-b)
  (cons chromosome-a chromosome-b))

(deftype chromosome-pair () '(satisfies chromosome-pair-p))

(defun chromosome-pairs-match-p (cp-a cp-b)
  (or
   (and (chromosomes-match-p (car cp-a) (car cp-b))
	(chromosomes-match-p (cdr cp-a) (cdr cp-b)))
   (and (chromosomes-match-p (car cp-a) (cdr cp-b))
	(chromosomes-match-p (cdr cp-a) (car cp-b)))))

(defstruct specimen
  (id 0 :type positive-fixnum :read-only t)
  (chromosomes nil :type (vector chromosome) :read-only t)
  (fitness-value 0.0 :type float)
  (normalized-fitness-value 0.0 :type float)
  (diversity-value 0.0 :type float))

(defun deep-copy-specimen (solution new-specimen-id)
  ;; @TODO Note that Fitness Value and Diversity Value are not copied!
  (make-specimen :id new-specimen-id
		    :chromosomes (make-array (length (specimen-chromosomes solution))
					     :element-type 'chromosome
					     :initial-contents (iterate:iterate
								  (iterate:for chromosome in-vector (specimen-chromosomes solution))
								  (iterate:collect (deep-copy-chromosome chromosome new-specimen-id))))))

(defun specimen-match-p (specimen-a specimen-b)
  (and
   (eq (length (specimen-chromosomes specimen-a)) (length (specimen-chromosomes specimen-b)))
   (every #'chromosomes-match-p (specimen-chromosomes specimen-a) (specimen-chromosomes specimen-b))))


(defun calc-solution-diversity (specimen-a specimen-b)
    ""
    (declare (type specimen specimen-a specimen-b))

    (iterate:iterate (iterate:for chromosome-a in-vector (specimen-chromosomes specimen-a))
		   (iterate:for chromosome-b in-vector (specimen-chromosomes specimen-b))
		   (iterate:sum (calc-chromosome-diversity chromosome-a chromosome-b))))


(defun get-chromosome-by-type (chromosome-type specimen)
  (declare (type string chromosome-type)
	   (type specimen specimen))

  (iterate:iterate
     (iterate:for chromosome in-vector (specimen-chromosomes specimen))
     (when (string= (chromosome-type chromosome) chromosome-type)
       (return chromosome))))
