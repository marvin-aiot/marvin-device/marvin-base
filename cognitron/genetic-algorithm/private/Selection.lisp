;;;; Copyright (C) 2019 Eric Diethelm

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(eval-when (:compile-toplevel :load-toplevel :execute)
  (unless (find-package :Marvin.Cognitron.GA)
    (make-package :Marvin.Cognitron.GA :use '(:common-lisp :Marvin.Utils)))
  
  (in-package :Marvin.Cognitron.GA))



(defun select-seed-individuals (omega &aux (fully-sorted-ff (sort (copy-list omega) #'> :key #'specimen-fitness-value)) (sorted-ff (cdr fully-sorted-ff)) (sorted-div (copy-list omega)) (alpha (list (car fully-sorted-ff))) (target-ff/div '(1 1)) (best-elm nil))
  ""
  (unless omega
    (error "Invalid parameter."))
  
  (loop
     until (or (null sorted-ff) (>= (length alpha) *max-num-parents*))
     with max-fv = 0
     with max-dv = 0
     do
       (setf max-fv (specimen-fitness-value (car sorted-ff)))
       
       (setf max-dv
	     (loop
		for elm in sorted-ff
		do (setf (specimen-diversity-value elm) (calc-population-diversity elm alpha))
		maximizing (specimen-diversity-value elm)))

     ;; Normalize data
     ;; @TODO It is possible to add multiplication factors to the normalization in order to
     ;;       influence FV/DV importance.
       (loop
	  for elm in sorted-ff
	  do
	    (setf (specimen-diversity-value elm) (/ (specimen-diversity-value elm) max-dv))
	    (setf (specimen-normalized-fitness-value elm) (/ (specimen-fitness-value elm) max-fv)))

     ;;(sort sorted-div #'> :key #'get-dv)

     ;;(setf target-ff/div (list 1 1))

       (setf best-elm nil)
       
       (loop
	  for elm in sorted-ff
	  with elm-distance
	  do
	    (setf elm-distance (calc-distance target-ff/div (list (specimen-normalized-fitness-value elm) (specimen-diversity-value elm))))
	    
	    (when (or (null best-elm) (< elm-distance (second best-elm)))
	      (setf best-elm (list elm elm-distance)))

	  ;;(when (or (> (- max-fv (specimen-normalized-fitness-value elm)) (second best-elm)) (> (- max-dv (specimen-diversity-value elm)) (second best-elm)))
	  ;;  (return))                   this is sorted -> ok                                this is not sorted: not ok
	    )

       (unless best-elm
	 (return))

       (log:info "Best element found: ~a" (car best-elm))
       (push (car best-elm) alpha)

       (setf sorted-ff (remove (car best-elm) sorted-ff))
       (setf sorted-div (remove (car best-elm) sorted-div)))

  (return-from select-seed-individuals alpha))
