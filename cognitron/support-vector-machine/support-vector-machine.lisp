;;;; Copyright (C) 2019 Eric Diethelm

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(in-package :svm)

#|


Input example
((((4611685991406299852 . 0.32d0) (4611685991704465017 . 0.8d0)) . 1)
 (((4611685991406299852 . -0.45d0) (4611685991704465017 . 0.87d0)) . 1)
 (((4611685991406299852 . -1.0d0) (4611685991704465017 . 0.65d0)) . 0)
 (((4611685991406299852 . -0.87d0) (4611685991704465017 . 0.86d0)) . 0)
 (((4611685991406299852 . -0.97d0) (4611685991704465017 . 0.18d0)) . 1)
 (((4611685991406299852 . -0.49d0) (4611685991704465017 . 0.33d0)) . 1))



|#


(defun convert-dataset (data-set metadata)
  (declare (ignore metadata))
  (iterate:iterate
    (iterate:for data in data-set)
    (iterate:collect
	(list (cdr data)
	      (iterate:iterate
		(iterate:for feature in (car data))
		(iterate:collect feature result-type vector))))))
      


(defun find-libsvm-problem-params (&key (problem problem) (verification-set verification-set) (min-c 1.0) (max-c 5.0) (c-step 0.2) (min-gamma -5.0) (max-gamma -1.0) (gamma-step 0.2))
    (iterate:iter :outer
		  (iterate:for log-c :from min-c :to max-c :by c-step)
		  (iterate:iter (iterate:for log-gamma :from min-gamma :to max-gamma :by gamma-step)
				(iterate:in :outer (iterate:collecting
						     (list* log-c
							    log-gamma
							    (let* ((params (libsvm:make-parameter
									    :c (exp log-c)
									    :gamma (exp log-gamma)))
								   (model (libsvm:train problem params)))
							      (quality model verification-set log-c log-gamma))))))))

(defun create-svm-model (data-set metadata)
  )

