# Cognitron: Support Vector Machine Manual


## License Information

This library is released under the GNU General Public License. Please refer to the [LICENSE](https://gitlab.com/marvin-aiot/marvin-aiot/blob/master/LICENSE "License") to get the full licensing text.

## Contributing to this project

Please refer to the [CONTRIBUTING](https://gitlab.com/marvin-aiot/marvin-aiot/blob/master/CONTRIBUTING "Contributing") document for more information.

