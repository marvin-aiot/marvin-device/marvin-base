
(require :vgplot)

(export 'n-gram-probability)

(defparameter *positive-sets* '("audio/silence.wav" "audio/silence2.wav" "audio/silence3.wav" "audio/silence4.wav"))
(defparameter *negative-sets* '("audio/voice1.wav" "audio/voice2.wav" "audio/voice3.wav" "audio/ahh.wav" "audio/ehh.wav"))

(defun classify (model feat-vec)
  (< 0.5 (Marvin.Utils:logistic (libsvm:predict model feat-vec))))

(defun classify-set (model data-set &key (as-new-feature nil))
  (if (null as-new-feature)
      (mapcar #'(lambda (x) (classify model x)) data-set)
      (progn
	(mapcar #'(lambda (y) (vector-push-extend (cons as-new-feature (if (classify model y) 1 0)) y) y) data-set)
	data-set)))

(defun train (&key (positive-sets nil) (negative-sets nil)
		  (min-c 0.2) (max-c 1.5) (c-step 0.1)
		  (min-gamma 0.1) (max-gamma 0.8) (gamma-step 0.1))
  (Marvin.Cognitron:train (collect-training-data positive-sets negative-sets) :min-c min-c :max-c max-c :c-step c-step :min-gamma min-gamma :max-gamma max-gamma :gamma-step gamma-step))

(defun collect-training-data (positive-sets negative-sets)
  (unless (and positive-sets negative-sets)
    (return-from collect-training-data nil))
  
  (append
   (loop for s in positive-sets
      append (Marvin.Cognitron:prepare-cl-libsvm-features 1 (Marvin.Plugins.AudioListener:proc-wav s))
      do (log:info "File '~a' loaded." s))
   (loop for s in negative-sets
      append (Marvin.Cognitron:prepare-cl-libsvm-features -1 (Marvin.Plugins.AudioListener:proc-wav s))
      do (log:info "File '~a' loaded." s))))



(defun cmd (wav-file-name &optional (do-plot nil) (output-file-name nil)
	    &aux (scaling-params (Marvin.Utils:load-hashtable "scale-params.txt")))
  (when do-plot
    (let ((wav (Marvin.Plugins.AudioListener:deserialize-wav wav-file-name)))
      (vgplot:plot (loop for x from 0 to (length wav) collect (* x 0.00004)) wav)))

  (let ((feat (loop for s in (remove-if #'null (loop for n on
						    (Marvin.Cognitron:scale-feature-vectors
						     (mapcar #'second
							     (Marvin.Cognitron:prepare-cl-libsvm-features 0
													  (Marvin.Plugins.AudioListener:proc-wav wav-file-name)))
						     scaling-params)
						  collect (Marvin.Plugins.AudioListener:update-delta-features n :features-to-ignore '(1))))
		 collect
		   (loop for x being the elements of s collect (cdr x)))))

    (when output-file-name
      (Marvin.Utils:export-csv feat output-file-name))
    
    feat))

(defun nn (audio-file max-dist &optional (scaling-params (Marvin.Utils:load-hashtable "scale-params.txt")))
  (Marvin.Cognitron:find-gravity-centers
   (mapcar #'(lambda (e) (list (second e) 1))
	   (Marvin.Cognitron:scale-feature-vectors
	    (Marvin.Cognitron:prepare-cl-libsvm-features
	     0
	     (Marvin.Plugins.AudioListener:proc-wav audio-file))
	    scaling-params))
   max-dist
   :distance-fn #'Marvin.Cognitron:euclidean-distance-sparse-vector
   :sort-fn #'<))


(defun nn-classify (audio-file max-distance &optional (model (libsvm:load-model "silence-model.svm")) (scaling-params (Marvin.Utils:load-hashtable "scale-params.txt")))
  (unless audio-file
    (return-from nn-classify nil))

  (let ((neigh (mapcar #'(lambda (e) (list (second e) 1))
		       (Marvin.Cognitron:scale-feature-vectors
			(Marvin.Cognitron:prepare-cl-libsvm-features
			 0
			 (Marvin.Plugins.AudioListener:proc-wav audio-file))
			scaling-params)))
	(n 0)
	(groups 0)
	(prev-groups 0)
	(res nil))

    (setf groups (length neigh))
    (setf prev-groups (1+ (length neigh)))
	
    (loop while (< groups prev-groups)
       do
	 (progn
	   (setf res (Marvin.Cognitron:find-gravity-centers
		      neigh
		      max-distance 
		      :distance-fn #'Marvin.Cognitron:euclidean-distance-sparse-vector
		      :sort-fn #'<))
	   (setf prev-groups groups)
	   (setf groups (length res))

	   (log:info "New NN groups ~A (before ~A)." groups prev-groups)))

    (let* ((cosmos (mapcar #'(lambda (z) (prog1 (append (list n) z) (setf n (1+ n)))) res))
           (tmp (mapcar #'(lambda (f) (append f (list (Marvin.Cognitron:k-nn (first f) cosmos :feature-key #'second)))) neigh))
	   (silence (classify-set model (mapcar #'first tmp))))

      (return-from nn-classify
	(values
	 (mapcar #'(lambda (x y) (append x (list y)))
		(mapcar #'(lambda (x) (append (butlast x) (loop for y being the elements of (car (last x)) append (list (car y) (cdr y)))))
			(mapcar #'(lambda (x) (append (loop for y being the elements of (car x) collect (cdr y)) (last x))) tmp))
		silence)
	 cosmos)))))


(defun nn-quality (cosmos seq &aux (res (length cosmos)))
  "Lower values are better.
   1. Congruence between SVM silence detection and NN group;
   2. Mean and deviation of distances in group;
   3. Mean distance to 2nd neighbours;
   4. Number of groups."

  (let ((cong (make-hash-table))
	(plist '())
	(tmp 0)
	(avg 0))
    (loop for x in seq
       do (progn
	    (if (nth-value 1 (gethash (nth 13 x) cong))
		(unless (eq (nth 19 x) (gethash (nth 13 x) cong))
		  (setf (getf plist (nth 13 x)) 10))
		(setf (gethash (nth 13 x) cong) (nth 19 x)))))

    (setf tmp (loop for y on plist by #'cddr
		 summing (second y)))

    (setf avg (loop for x in seq
		 summing (nth 14 x)))
    
    (log:info "Cosmos size: ~A; insonsistencies: ~A; average: ~A" res (/ tmp 10) (/ avg (length seq)))    

    (return-from nn-quality (+ tmp res (/ avg (length seq))))))

(defun nn-search (audio-file &key (min-dist 10.0d0) (max-dist 250.0d0) (step 25.0d0))
  (iter (for x :from 10.0d0 :to 100.0d0 :by 10.0d0)
	(collecting (multiple-value-bind (seq cosmos) (nn-classify audio-file x) (let ((qual (nn-quality cosmos seq))) (log:info "x: ~A | Quality: ~A" x qual) qual)))))



(defun x (file &aux (data (proc-wav file)) scaled features classes res )

  (setf scaled
	(mapcar #'(lambda (y &aux (x (second y)))
		    (concatenate 'vector  
				 (list (cons (car (elt x 0)) (- (/ (cdr (elt x 0)) (/ 442 2)) 1)))
				 (list (cons (car (elt x 1)) (- (/ (- (cdr (elt x 1)) 3.218) (/ (- 490.647 3.218) 2)) 1)))
				 (list (cons (car (elt x 2)) (- (/ (- (cdr (elt x 2)) 17) (/ (- 2371 17) 2)) 1)))
				 (subseq x 3)))
		(Marvin.Cognitron:prepare-cl-libsvm-features 0 (mapcar #'(lambda (s d) (funcall s d))
								       (mapcar #'(lambda (vec) (let* ((min (reduce #'min (cdddr vec) :initial-value 0))
												      (max (reduce #'max (cdddr vec) :initial-value 0))
												      (mean (/ (- max min) 2)))
												 #'(lambda (x) (append (subseq x 0 3)
														       (mapcar #'(lambda (w) (- (/ (- w min) mean) 1))
															       (cdddr x))))))
									       data)
								       data))))

  
  (setf features
	(remove-if #'null
		   (loop
		      for f on (remove-if #'null
					  (loop
					     for f on scaled
					     collect (update-delta-features f :k 3)))
		      collect (update-delta-features f :k 3 :features-to-ignore '(1 2 3 4 5 6 7 8 9 10 11 12 13 14 15)))))
  

  
  ;;(append (list (loop for x across (car res) collect (first x)))
  ;;	  (mapcar #'(lambda (y) (loop for x across y collect (cdr x))) (cdr res)))


  #|
  (setf classes
	(loop
	   for n = 0 then (1+ n)
	   for v in (Marvin.Cognitron:find-gravity-centers
		     (mapcar #'(lambda (x) (list x 1)) features)
		     0.35d0
		     :distance-fn #'Marvin.Cognitron:euclidean-distance-sparse-vector
		     :sort-fn #'<)
	   collect (list (first v) n)))

  (setf res
	(loop
	   for elm in features
	   collect (let ((r (car (Marvin.Cognitron:k-nn elm classes :feature-key #'first :class-key #'second :k 1))))
		     (concatenate 'vector
				  elm
				  (list (cons 99998 (car r)))
				  (list (cons 99999 (cdr r)))))))
  |#
  
  ;;(export-csv (append (list (loop for x across (car res) collect (format nil "F-~a" (car x))))
  ;;		      (mapcar #'(lambda (x) (loop for y across x collect (cdr y))) res))
  ;;	      #P"/home/eric/Development/Marvin/audio-results/abc-clusters.csv" :decimal-places 6)
  )

(defun train-each-class (data-seq
			 &key
			   (nn-dist-limit 0.25)
			 &aux
			   (classes '())
			   (parted '())
			   (training-set '())
			   (verification-set '())
			   (min-c -5.0)
			   (max-c 5.0)
			   (c-step 1.0)
			   (min-gamma -1.0)
			   (max-gamma 3.0)
			   (gamma-step 1.0))
  (if data-seq
      (progn
	(log:info "Processing a new data-set.")
	
	;; Collect every known class
	(loop for (class nil) in data-seq
	   unless (member class classes)
	   do (push class classes))

	(setf parted (Marvin.Cognitron:nn-partition nn-dist-limit data-seq))

	(log:info "Raw data loaded and NN sorted.")
	(log:info "~a (~2$%) elements were removed from training because of NN distance limit of ~a."
		  (length (second parted))
		  (* 100 (/ (length (second parted)) (+ (length (first parted)) (length (second parted)))))
		  nn-dist-limit)

	(multiple-value-bind (train verify)
	    (Marvin.Cognitron:split-training-data (randomize-order (first parted)))
	  (setf training-set train)
	  (setf verification-set (randomize-order (append verify (second parted)))))

	(Marvin.Utils:save-to-file training-set "training-set.txt")
	(Marvin.Utils:save-to-file verification-set "verification-set.txt"))
      
      (progn
	  (log:info "Re-using existing data-set.")
	  (setf training-set (Marvin.Utils:read-from-file "training-set.txt"))
	  (setf verification-set (Marvin.Utils:read-from-file "verification-set.txt"))))

  (loop for class in classes
     unless (equal class 'IGNORE)
     collect (let ((verif-data (loop for (c f) in verification-set collecting (list (if (equal c class) 1 -1) f)))
		   (problem (Marvin.Cognitron:set-libsvm-problem (loop for (c f) in training-set collecting (list (if (equal c class) 1 -1) f)))))
	       (log:info "Training class '~a'" class)
	       (break)
	       (list class (Marvin.Cognitron:find-libsvm-problem-params :problem problem
									:verification-set verif-data
									:min-c min-c
									:max-c max-c
									:c-step c-step
									:min-gamma min-gamma
									:max-gamma max-gamma
									:gamma-step gamma-step))))

  (export-csv (loop for (class results) in * append (loop for vec in results collect (append (list class) vec))) "confusion-results.csv"))
  


(defparameter class-search-space ;; ((class (c-min c-max gamma-min gamma-max))*)
  '((SIL (-2 2 -2 2))
    (T (2 6 -2 2))
    (TZ (2 10 -2 2))
    (B (0 4 -2 2))
    (CH (8 12 -8 -12))
    (D (6 14 -2 2))
    ))
