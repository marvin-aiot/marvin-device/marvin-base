;;;; Copyright (C) 2019 Eric Diethelm

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(in-package :svm)

(export 'scale-feature-vectors)
(export 'calculate-scaling-params)

(defun calculate-scaling-params (seq &aux (res (make-hash-table)) (f-min (make-hash-table)) (f-max (make-hash-table)))
  "Given a sequence of (sparse/labeled) feature vectors @seq, calculate the scaling parameters such that every feature is 
   mapped to a value in the range [-1 1]. 
   The result is a hash table containing a list as (offset factor) for each feature. 
   The formula (1- (/ (- value offset) factor) can be used to map a value to [-1 1]."
  (loop for s in seq
     do
       (loop for f being the elements of (second s)
	  do
	    (let* ((key (car f))
		   (val (cdr f))
		   (min-val (gethash key f-min))
		   (max-val (gethash key f-max)))
	      
	      (when (or (not min-val) (< val min-val))
		(setf (gethash key f-min) val))

	      (when (or (not max-val) (> val max-val))
		(setf (gethash key f-max) val)))))

  (loop for key in (remove-duplicates
		    (append (alexandria:hash-table-keys f-min)
			    (alexandria:hash-table-keys f-max)))
     do
       (let ((min (gethash key f-min))
	     (max (gethash key f-max)))

	 (if (and min max)
	     (setf (gethash key res) (list min (/ (- max min) 2)))
	     (error "The key '~a' is not consistent for scaling" key))))

  (return-from calculate-scaling-params res))


(defun scale-feature-vector (seq scaling-params)
  ""
  (declare (type vector seq))
  (declare (type hash-table scaling-params))
  
  (loop for n from 0 to (1- (length seq))
     do
       (let* ((feature (aref seq n))
	      (key (car feature))
	      (value (cdr feature))		   
	      (params (gethash key scaling-params))
	      (offset (first params))
	      (factor (second params)))

	 (setf (cdr feature) (1- (/ (- value offset) factor)))))

  (return-from scale-feature-vector seq))

(defun scale-feature-vectors (seq scaling-params)
  "Apply the function @scale-feature-vector to every element of @seq, scaling it the the given @scaling-params."
  (declare (type hash-table scaling-params))
  (loop for s in seq
     do (scale-feature-vector s scaling-params))
  
  (return-from scale-feature-vectors seq))

