;;;; Copyright (C) 2019 Eric Diethelm

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(in-package :svm)

(export 'prepare-cl-libsvm-features)
(export 'train)
(export 'set-libsvm-problem)
(export 'split-training-data)
(export 'find-libsvm-problem-params)

(defun prepare-cl-libsvm-feature (feature-vector &aux (res (make-array (length feature-vector) :fill-pointer 0 :initial-element '(nil . nil) :element-type 'cons :adjustable t)))
  (loop for n being the elements of feature-vector
     for i = 1 then (1+ i)
     do (vector-push (cons i (coerce n 'double-float)) res))

  (return-from prepare-cl-libsvm-feature (the (vector cons) res)))

(defun prepare-cl-libsvm-features (class features)
  (loop for x being the elements of features
     collect
       (list class
	     (prepare-cl-libsvm-feature x))))

(defun load-features-csv (file-name scaling-params &key (separator #\,))
  (with-open-file (fstream file-name)
    (let ((line (read-line fstream nil)))
      line)
    (loop for line = (read-line fstream nil fstream)
       until (eq line fstream)
       collect (scale-feature-vector (mapcar #'read-from-string (cl-ppcre:split separator line)) scaling-params) into res
       finally (return res))))

(defun set-libsvm-problem (training-data)
  (libsvm:make-problem (map 'vector 'first training-data)
		       (map 'vector 'second training-data)))

(defun split-training-data (data &key (relation (/ 2 3)) &aux (split-pos (round (* relation (length data)))))
  (values (subseq data 0 split-pos) (subseq data split-pos)))

(defun quality (model test-data log-c log-gamma)
  "Use the Matthews Correlation Coefficient to measure how well the model does"
  (iterate:iter (iterate:for (target input) :in test-data)
	(let ((p (if (< 0.5 (Marvin.Utils:logistic (libsvm:predict model input)))
		     1 -1)))
	  (cond ((and (= p 1) (/= target p))
		 (iterate:summing 1 :into false-positives))
		((and (= p -1) (/= target p))
		 (iterate:summing 1 :into false-negatives))
		((and (= p 1) (= target p))
		 (iterate:summing 1 :into true-positives))
		((and (= p -1) (= target p))
		 (iterate:summing 1 :into true-negatives))))
	
	(iterate:finally
	 (let ((quality
		;; Compute quality of model
		(if (= 0 (- (* true-positives true-negatives)
			    (* false-positives false-negatives)))
		    0d0
		    (/ (- (* true-positives true-negatives)
			  (* false-positives false-negatives))
		       (sqrt
			(float
			 (* (+ true-positives false-positives)
			    (+ true-positives false-negatives)
			    (+ true-negatives false-positives)
			    (+ true-negatives false-negatives))
			 0d0))))))
	   
	   ;; Print some output so we know what it's doing
	   (format
	    t "log-c = ~3$, log-gamma = ~3$ TP = ~A, TN = ~A, FP = ~A, FN = ~A Quality = ~3$%~%~%"
	    log-c log-gamma
	    true-positives true-negatives false-positives false-negatives
	    (* 100 quality))
	   (return-from quality (list quality true-positives true-negatives false-positives false-negatives #|model|#))))))



(defparameter *distances* nil)

(let ((problem nil) (training-set nil) (verification-set nil))
  (defun train (data-set &key
		  (nn-dist-limit 0.3)
		  (min-c 1.0) (max-c 5.0) (c-step 0.2)
		  (min-gamma -2.0) (max-gamma 2.0) (gamma-step 0.2))
    ""
    (if data-set
	(progn
	  (log:info "Processing a new data-set.")
	  (let* ((scale-params (calculate-scaling-params data-set))
		 (parted (nn-partition nn-dist-limit
				       (scale-feature-vectors data-set scale-params))))

	  (log:info "Raw data loaded and NN sorted.")
	  (log:info "~a (~2$%) elements were removed from training because of NN distance limit of ~a."
		    (length (second parted))
		    (* 100 (/ (length (second parted)) (+ (length (first parted)) (length (second parted)))))
		    nn-dist-limit)
	  
	  (multiple-value-bind (train verify)
	      (split-training-data (randomize-order (first parted)))
	    (setf training-set train)
	    (setf verification-set (randomize-order (append verify (second parted))))
	    (setf problem (set-libsvm-problem training-set)))

	  (Marvin.Utils:save-hashtable scale-params "scale-params.txt")
	  (Marvin.Utils:save-to-file training-set "training-set.txt")
	  (Marvin.Utils:save-to-file verification-set "verification-set.txt")
	  (libsvm:save-problem problem "problem.svm")))

	(progn
	  (log:info "Re-using existing data-set.")
	  (setf training-set (Marvin.Utils:read-from-file "training-set.txt"))
	  (setf verification-set (Marvin.Utils:read-from-file "verification-set.txt"))
	  (setf problem (libsvm:load-problem "problem.svm"))))

    (log:info "Starting SVM model training.")
    (log:info "Training set: Total ~a; Positive: ~a; Negative: ~a" (length training-set) 0 0)
    (log:info "Verification set Total ~a; Positive: ~a; Negative: ~a"  (length verification-set) 0 0)
	      
    (save-to-file (butlast (find-libsvm-problem-params :min-c min-c :max-c max-c :c-step c-step :min-gamma min-gamma :max-gamma max-gamma :gamma-step gamma-step)) "confusion-results.txt"))

  (defun find-libsvm-problem-params (&key (problem problem) (verification-set verification-set) (min-c 1.0) (max-c 5.0) (c-step 0.2) (min-gamma -5.0) (max-gamma -1.0) (gamma-step 0.2))
    (iterate:iter :outer
		  (iterate:for log-c :from min-c :to max-c :by c-step)
		  (iterate:iter (iterate:for log-gamma :from min-gamma :to max-gamma :by gamma-step)
				(iterate:in :outer (iterate:collecting
						     (list* log-c
							    log-gamma
							    (let* ((params (libsvm:make-parameter
									    :c (exp log-c)
									    :gamma (exp log-gamma)))
								   (model (libsvm:train problem params)))
							      (quality model verification-set log-c log-gamma)))))))))
;;;;


(defun identify-hot-spots (confusion-matrix)
  "Given the c/gamma vs. quality matrix, identify regions of high performance. If the region is adjacent to the matrix border, extend the region to over the matrix. Return a list of (min-c max-c min-gamma max-gamma)."
  (declare (ignore confusion-matrix)))


