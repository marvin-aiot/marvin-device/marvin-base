;;;; Copyright (C) 2019 Eric Diethelm

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(in-package :marvin)

(defsection @marvin-aiot-manual (:title "Marvin Base Manual")
  "[![pipeline status](https://gitlab.com/marvin-aiot/marvin-device/marvin-base/badges/master/pipeline.svg)](https://gitlab.com/marvin-aiot/marvin-device/marvin-base/commits/master)"
  (@marvin-base-description section)
  (@marvin-base-installing section)
  (@marvin-base-example section)
  (@marvin-base-exported section)
  (@marvin-base-license section)
  (@marvin-base-contributing section))


(defsection @marvin-base-description (:title "Description")
  "What is Marvin AIoT and why is it Open Source?
")

(defsection @marvin-base-data-rep (:title "Data & Knowledge representation")
  "Ding, Frame & K-Lines")

(defsection @marvin-base-installing (:title "Installing marvin")
  "

### From source (Git)

### Using Docker

## Finalize installation

```lisp
(setup-marvin)
```
")

(defsection @marvin-base-example (:title "Working Example")
  "")

(defsection @marvin-base-exported (:title "Exported Symbols"))

(defsection @marvin-base-license (:title "License Information")
  "This library is released under GPLv3. Please refer to the [LICENSE](https://gitlab.com/marvin-aiot/marvin-device/marvin-base/blob/master/LICENSE 'License') to get the full licensing text.")

(defsection @marvin-aiot-contributing (:title "Contributing to this project")
  "Please refer to the [CONTRIBUTING](https://gitlab.com/marvin-aiot/marvin-device/marvin-base/blob/master/CONTRIBUTING.md 'Contributing') document for more information.")

(trivial-utilities:make-doc-updater :marvin-base @marvin-base-manual)

