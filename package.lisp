(uiop:define-package #:marvin-base
  (:nicknames :marvin)
  (:documentation "")
  (:use #:common-lisp)
  (:export #:initialize
	   #:shutdown
	   #:running?
	   #:show-wellcome-message))

