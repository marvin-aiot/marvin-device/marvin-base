;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-

;;;; Copyright (C) 2019 Eric Diethelm

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.


(defsystem :marvin-base
  :name "marvin-base"
  :description "Marvin: Artificial Intelligence of Things."
  :long-description ""
  :version "0.0.6"
  :author "Eric Diethelm <ediethelm@yahoo.com>"
  :licence "GPL-3.0-or-later"
  :in-order-to ((test-op (test-op :marvin-base/test)))
  :depends-on (:trivial-timer
	       :end-point
	       :ding
	       :longterm-memory
	       :working-memory
	       :rule-engine
	       :plugin-handler
	       :frame
	       :k-line
	       :action)
  :components ((:file "package")
	       (:file "marvin-base")))

(defsystem :marvin-base/prepare-test-suite
  :name "marvin-base/prepare-test-suite"
  :description "Test suite preparation for Marvin AIoT Base."
  :long-description ""
  :author "Eric Diethelm <ediethelm@yahoo.com>"
  :licence "GPL-3.0-or-later"
  :depends-on (:marvin-base
	       :fiveam)
  :components ((:file "test-marvin")))

(defsystem :marvin-base/test
  :name "marvin-base/test"
  :description "Test cases for Marvin AIoT Base."
  :long-description ""
  :author "Eric Diethelm <ediethelm@yahoo.com>"
  :licence "GPL-3.0-or-later"
  :depends-on (:marvin-base/prepare-test-suite
	       :longterm-memory/test
	       :working-memory/test
	       :rule-engine/test)
  :perform (test-op (o s) (uiop:symbol-call :fiveam  '#:run! :marvin-tests)))

