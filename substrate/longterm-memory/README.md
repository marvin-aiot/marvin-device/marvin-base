# Marvin Longterm Memory Manual

###### \[in package LONGTERM-MEMORY\]
[![pipeline status](https://gitlab.com/marvin-aiot/marvin/badges/master/pipeline.svg)](https://gitlab.com/marvin-aiot/marvin/commits/master)

## Description

## Installing trivial-continuation

## Working Example

## Exported Symbols

## License Information

This library is released under the GNU General Public License. Please refer to the [LICENSE](https://gitlab.com/marvin-aiot/marvin/blob/master/LICENSE "License") to get the full licensing text.

## Contributing to this project

Please refer to the [CONTRIBUTING](https://gitlab.com/marvin-aiot/marvin/blob/master/CONTRIBUTING "Contributing") document for more information.



