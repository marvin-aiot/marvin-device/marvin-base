(uiop:define-package #:longterm-memory
  (:documentation "")
  (:use #:common-lisp)
  (:export #:search-exhausted-condition 
	   #:search-params 
	   #:results 
	   #:visited 
	   #:blacklist 
	   #:depth 
	   #:depth-limit 
	   #:exhaustive 
	   #:ignore-inheritance
	   #:initialize-longterm-memory
	   #:shutdown-longterm-memory
	   #:get-next-ding-id
	   #:get-last-created-id
	   #:load-objects-by-class
	   #:save-object
	   #:get-specific-id))

