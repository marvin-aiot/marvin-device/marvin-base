;;;; Copyright (C) 2019 Eric Diethelm

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(in-package :longterm-memory)

(defsection @longterm-memory-manual (:title "longterm-memory Manual")
  "[![pipeline status](https://gitlab.com/marvin-aiot/marvin/badges/master/pipeline.svg)](https://gitlab.com/marvin-aiot/marvin/commits/master)"
  (longterm-memory asdf:system)
  (@longterm-memory-description section)
  (@longterm-memory-installing section)
  (@longterm-memory-example section)
  (@longterm-memory-exported section)
  (@longterm-memory-license section)
  (@longterm-memory-contributing section))


(defsection @longterm-memory-description (:title "Description")
  "This library is part of the [Marvin AIoT](https://gitlab.com/marvin-aiot/marvin 'Marvin AIoT'). For more information please visit the aforementioned project.")

(defsection @longterm-memory-installing (:title "Installing longterm-memory")
  "This project is not available via [QuickLisp](https://www.quicklisp.org/beta/ \"QuickLisp\") of download, but it still be loaded if the sources are copyed to your local-projects folder:

```bash
cd $HOME/quicklisp/local-projects
git clone https://gitlab.com/marvin-aiot/marvin.git
```

After the files are copied, we can use [QuickLisp](https://www.quicklisp.org/beta/ \"QuickLisp\") to load longterm-memory

```lisp
(ql:quickload :longterm-memory)
```
")

(defsection @longterm-memory-example (:title "Example")
  "")

(defsection @longterm-memory-exported (:title "Exported Symbols")
  (search-exhausted-condition condition)
  (generic-place-holder (reader search-exhausted-condition))
  (bindings (reader search-exhausted-condition))
  (search-params (reader search-exhausted-condition))
  (results (reader search-exhausted-condition))
  (visited (reader search-exhausted-condition))
  (blacklist (reader search-exhausted-condition))
  (depth (reader search-exhausted-condition))
  (depth-limit (reader search-exhausted-condition))
  (exhaustive (reader search-exhausted-condition))
  (ignore-inheritance (reader search-exhausted-condition))
  (initialize-longterm-memory function)
  (shutdown-longterm-memory function)
  (get-next-ding-id function)
  (get-last-created-id function)
  (load-objects-by-class function)
  (save-object function)
  (get-specific-id function))

(defsection @longterm-memory-license (:title "License Information")
  "This library is released under GPLv3. Please refer to the [LICENSE](https://gitlab.com/marvin-aiot/marvin/blob/master/LICENSE 'License') to get the full licensing text.")

(defsection @longterm-memory-contributing (:title "Contributing to this project")
  "Please refer to the [CONTRIBUTING](https://gitlab.com/marvin-aiot/marvin/blob/master/CONTRIBUTING.md 'Contributing') document for more information.")
