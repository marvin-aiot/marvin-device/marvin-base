;;;; Copyright (C) 2019 Eric Diethelm

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(in-package :longterm-memory)

(eval-when (:compile-toplevel :load-toplevel :execute) 
  (proclaim '(ftype (function () fixnum) get-next-ding-id))
  (proclaim '(ftype (function () fixnum) get-last-created-id))
  (proclaim '(ftype (function (fixnum) (or null ding:ding)) get-specific-id))
  (proclaim '(ftype (function (ding:ding) t) insert-object))
  (proclaim '(ftype (function (ding:ding) list) remove-non-persistent-eigenschaften))

  (proclaim '(inline get-next-ding-id get-last-created-id save-object load-object)))


(define-condition search-exhausted-condition (condition)
  ((generic-place-holder :initarg :generic-place-holder :reader generic-place-holder)
   (bindings :initarg :bindings :reader bindings)
   (search-params :initarg :search-params :reader search-params)
   (results-until-now :initarg :results :reader results)
   (visited :initarg :visited :reader visited)
   (blacklist :initarg :blacklist :reader blacklist)
   (depth :initarg :depth :reader depth)
   (depth-limit :initarg :depth-limit :reader depth-limit)
   (exhaustive :initarg :exhaustive :reader exhaustive)
   (ignore-inheritance :initarg :ignore-inheritance :reader ignore-inheritance)
   (error :initarg :error :reader throw-exception))
  (:report (lambda (condition stream)
	     (format stream "Search exhausted after ~a iterations while processing '~a'." (depth condition) (search-params condition))))
  (:documentation "The function 'find-objects' whent into a dead-end while searching for objects matching the given search parameters."))

(defun initialize-longterm-memory ()
  ""
  )

(defun shutdown-longterm-memory ()
  ""
  )

;; @REVIEW At the moment this function accepts a &key :limit to indicate the largest amount of results to deliver.
;;         Although this is meaningful, a way to continue loading the next block of results is missing.
(defun load-objects-by-class (klasse &key (limit 1000))
  (declare (type ding:ding-base klasse))

  (let* ((row (trivial-pooled-database:execute
	       (with-output-to-string (stream)
		 (format stream "SELECT ls.`DING-ID`, ls.ÄNDERUNGSDATUM, ls.KLASSE, ls.`P-LIST`, da.`UPDATE-CYCLES`, da.ASSOCIATIONS
 FROM `LangzeitSpeicher` as ls,
  (SELECT `DING-ID`
   FROM `LangzeitSpeicher`
   WHERE `KLASSE` = ~a) as ref
 LEFT JOIN DingAssociations as da
 ON ref.`DING-ID` = da.`DING-ID`
 WHERE ls.`DING-ID` = ref.`DING-ID`
 LIMIT ~a;" (ding:id klasse) limit)))))
    
    (loop for item in row
       collect (ding:json-deserialize
		(format nil "<\":ID\", ~a, \":ERSTELLUNGSDATUM\", ~a, \":KLASSE\", ~a, \":P-LIST\", ~a, \":UPDATE-CYCLES\", ~a, \":ASSOCIATIONS\", ~a>"
			(second (member :ding-id item))
			(second (member :änderungsdatum item))
			(with-output-to-string (stream)
			  (trivial-json-codec:serialize
			   (if (eq (second (member :klasse row)) -1)
			       nil
			       (make-instance 'ding:ding-reference :id (second (member :klasse item))))
			   stream))
			(second (member :p-list item))
			(if (second (member :update-cycles item))
			    (second (member :update-cycles item))
			    0)
			(second (member :associations item)))))))


(defun get-specific-id (id)
  (declare (type trivial-utilities:non-negative-fixnum id))

  (log:trace "Loading Ding [~a] from persistence." id)

  (let ((row (car (trivial-pooled-database:execute
		   (format nil
			   "SELECT ls.`DING-ID`, ls.ÄNDERUNGSDATUM, ls.KLASSE, ls.`P-LIST`, da.`UPDATE-CYCLES`, da.ASSOCIATIONS 
FROM LangzeitSpeicher as ls
left join DingAssociations as da
on ls.`DING-ID` = da.`DING-ID`
where ls.`DING-ID` = ~a
limit 1"
			   id)))))
    (unless (and row (listp row) (eq (length row) 12))
      (log:error "Could not load (ID ~a) from Langzeitspeicher." id)
      (return-from get-specific-id nil))

    (return-from get-specific-id
      (the (values ding:ding &optional)
	   (ding:json-deserialize
	    (format nil "<\":ID\", ~a, \":ERSTELLUNGSDATUM\", ~a, \":KLASSE\", ~a, \":P-LIST\", ~a, \":UPDATE-CYCLES\", ~a, \":ASSOCIATIONS\", ~a>"
		    (second (member :ding-id row))
		    (second (member :änderungsdatum row))
		    (with-output-to-string (stream)
		      (trivial-json-codec:serialize
		       (if (eq (second (member :klasse row)) -1)
			   nil
			   (make-instance 'ding:ding-reference :id (second (member :klasse row))))
		       stream))

		    (second (member :p-list row))
		    (second (member :update-cycles row))
		    (second (member :associations row))))))))

(defun save-object (object)
  ""
  (declare (type ding:ding object))
  (trivial-object-lock:with-object-lock-held (object :test #'trivial-utilities:equals)
    (insert-object object))

  (return-from save-object object))

(defun insert-object (ding)
  (declare (type ding:ding ding))

  (log:trace "Saving ding: ~a" ding)
  
  (trivial-pooled-database:within-transaction
    (when (eq 1 (logand (ding:dirty ding) 1))
      (if (ding:newly-created ding)
	  (trivial-pooled-database:insert "LangzeitSpeicher"
					  '(:DING-ID :ÄNDERUNGSDATUM :P-LIST :KLASSE)
					  (list (ding:id ding)
						(ding:activation-time ding)
						(trivial-json-codec:serialize-json (remove-non-persistent-eigenschaften ding))
						(if (ding:klasse ding)
						    (ding:id (ding:klasse ding))
						    -1)))
	  (trivial-pooled-database:update "LangzeitSpeicher"
					  '(:ÄNDERUNGSDATUM :P-LIST :KLASSE)
					  (list (ding:activation-time ding)
						(trivial-json-codec:serialize-json (remove-non-persistent-eigenschaften ding))
						(if (ding:klasse ding)
						    (ding:id (ding:klasse ding))
						    -1))
					  (format nil "`DING-ID` = ~a" (ding:id ding)))))

    (when (eq 2 (logand (ding:dirty ding) 2))
      (if (ding:newly-created ding)
	  (trivial-pooled-database:insert "DingAssociations"
					  '(:DING-ID :UPDATE-CYCLES :ASSOCIATIONS)
					  (list (ding:id ding)
						(ding:update-cycles ding)
						(trivial-json-codec:serialize-json (ding:associations ding))))
	  (trivial-pooled-database:update "DingAssociations"
					  '(:UPDATE-CYCLES :ASSOCIATIONS)
					  (list (ding:update-cycles ding)
						(trivial-json-codec:serialize-json (ding:associations ding)))
					  (format nil "`DING-ID` = ~a" (ding:id ding))))))

  (return-from insert-object (values)))

(defun remove-non-persistent-eigenschaften (ding)
  (declare (type ding:Ding ding))
  ;; Remove the items, that represent other DB columns
  (iterate:iterate
    (iterate:with results = (append
			     (trivial-utilities:collect-persistent-slots ding :slots-to-ignore '(ding:p-list ding:update-cycles ding:associations))
			     (copy-list (ding:p-list ding))))
    (iterate:for key in '(:id :activation-time :klasse))
    (remf results key)
    (iterate:finally (return results))))


