;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-

;;;; Copyright (C) 2019 Eric Diethelm

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(defsystem :longterm-memory
  :name "longterm-memory"
  :description ""
  :version "0.4.3"
  :author "Eric Diethelm <ediethelm@yahoo.com>"
  :licence "GPL-3.0-or-later"
  :in-order-to ((test-op (test-op :longterm-memory/test)))
  :depends-on (:trivial-utilities
	       :trivial-pooled-database
	       :piggyback-parameters
	       :ding)
  :components ((:file "package")
	       (:file "longterm-memory")))



(defsystem :longterm-memory/test
  :name "longterm-memory/test"
  :description "Unit Tests for the longterm-memory project."
  :author "Eric Diethelm <ediethelm@yahoo.com>"
  :licence "GPL-3.0-or-later"
  :depends-on (:longterm-memory fiveam)
  :perform (test-op (o s) (uiop:symbol-call :fiveam  '#:run! :longterm-memory-tests))
  :components ((:file "test-longterm-memory")))
