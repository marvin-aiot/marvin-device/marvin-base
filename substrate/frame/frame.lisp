;;;; Copyright (C) 2019 Eric Diethelm

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(in-package :frame)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defclass frame (ding:ding)
    ((k-lines :type list
	      :initform nil
	      :initarg :k-lines
	      :accessor k-lines)
     (processing-responses :type list
			   :initform nil
			   :initarg :processing-responses
			   :accessor processing-responses)
     (executing-response :type (or null ding:ding-reference plugin-base:plugin-response)
			 :initform nil
			 :initarg :executing-response
			 :accessor executing-response))))

(ding:register-ding-simplified "Frame" (find-class 'frame:frame))

