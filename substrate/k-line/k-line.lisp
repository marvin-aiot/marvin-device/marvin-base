;;;; Copyright (C) 2019 Eric Diethelm

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(in-package :k-line)

;;;;
;;
;; EXPECTATIONS: The class object for 'K-LINE' already esists in persistence with the id 3.
;;
;;;;

(eval-when (:compile-toplevel :load-toplevel :execute)
  (proclaim '(ftype (function (frame:frame k-line:k-line) null) remove-frame-from-kline))
  (proclaim '(ftype (function (k-line:k-line list frame:frame) boolean) replace-frames-from-kline))
  (proclaim '(ftype (function (frame:frame k-line:k-line) boolean) add-frame-to-kline)))

(defclass k-line (ding:ding)
  ((child-frames :type list
		 :accessor child-frames
		 :initarg :child-frames
		 :initform nil)
   (initiator-frame :type (or null ding:ding-base frame:frame)
		    :accessor initiator-frame
		    :initarg :initiator-frame
		    :initform nil)
   (result-frame :type (or null ding:ding-base frame:frame)
		 :accessor result-frame
		 :initarg :result-frame
		 :initform nil)
   (goal :type t
	 :accessor goal
	 :initarg :goal
	 :initform nil)
   (processee :type (or null ding:ding-base plugin-base:plugin)
	      :accessor processee
	      :initarg :processee
	      :initform nil)
   (continuation-point :type t
		       :accessor continuation-point
		       :initarg :continuation-point
		       :initform nil)
   (processing-responses :type list
			 :accessor processing-responses
			 :initarg :processing-responses
			 :initform nil)
   (executing-response :type (or null ding:ding-base plugin-base:plugin-response)
		       :accessor executing-response
		       :initarg :executing-response
		       :initform nil)))



(ding:register-ding-simplified "K-Line" (find-class 'k-line:k-line))

(defun remove-frame-from-kline (frame kline)
  (declare (type frame:frame frame)
	   (type k-line:k-line kline))

  (trivial-object-lock:with-object-lock-held (frame :property :k-lines :test #'trivial-utilities:equals)
    (trivial-object-lock:with-object-lock-held (kline :property :child-frames :test #'trivial-utilities:equals)
      (let ((k-lines (ding:eigenschaft frame :k-lines))
	    (child-frames (ding:eigenschaft kline :child-frames)))
	(declare (type list child-frames k-lines))
	(setf (child-frames kline) (delete frame child-frames))
	(setf (frame:k-lines frame) (delete kline k-lines)))))
  (return-from remove-frame-from-kline nil))

(defun replace-frames-from-kline (kline frames new-frame)
  "Removes the frames given in 'frames' (a list) from the K-Line and inserts 'new-frame' in their relative position. 
   The frames in 'frames' must all be :child-frames of 'kline' and must not be interleaved with frames not to remove."
  (declare (type frame:frame new-frame)
	   (type k-line:k-line kline)
	   (type list frames))
  (trivial-object-lock:with-object-lock-held (new-frame :property :k-lines :test #'trivial-utilities:equals)
    (trivial-object-lock:with-object-lock-held (kline :property :child-frames :test #'trivial-utilities:equals)
      (let ((child-frames (ding:eigenschaft kline :child-frames)))
	(declare (type list child-frames))
	
	(unless (and (every #'(lambda (x) (typep x 'frame:frame)) frames)
		     (null (member kline (the list (frame:k-lines new-frame))))
		     (every #'(lambda (x) (member kline (the list (frame:k-lines x)))) frames))
	  (log:error "Ignore this problem and return nil." "The passed arguments are not valid.")
	  (cerror "Ignore this problem and return nil." 'invalid-arguments)
	  (return-from replace-frames-from-kline nil))

	;; Check that frames are continguous in child-frames.
	;; The ordering is not important, but no other frames must appear in between.
	(unless (eq (1+ (- (position-if #'(lambda (elm) (declare (type frame:frame elm)) (member elm frames)) child-frames :from-end t)
			   (position-if #'(lambda (elm) (declare (type frame:frame elm)) (member elm frames)) child-frames)))
		    (length frames))
	  (log:error "The sequence of frames to replace is not contiguous in the K-Line.")
	  (cerror "Ignore and continue" "The sequence of frames to replace is not contiguous in the K-Line.")
	  (return-from replace-frames-from-kline nil))
	
	(loop for frame in frames
	   do (trivial-object-lock:with-object-lock-held (frame :property :k-lines :test #'trivial-utilities:equals)
		(setf (frame:k-lines frame) (delete kline (the list (frame:k-lines frame))))))

	(push kline (frame:k-lines new-frame))

	(let ((old-frames-but-one (butlast frames)))
	  (setf (child-frames kline) (delete-if #'(lambda (x) (member x old-frames-but-one :test #'trivial-utilities:equals))
						child-frames)))
	
	(setf (child-frames kline) (nsubstitute new-frame
						(car (last frames))
						child-frames)))))
  
  (return-from replace-frames-from-kline t))

;; @TODO Include an optional continuation
(defun add-frame-to-kline (frame kline)
  "Adds a 'frame' to the :child-frames of 'kline'. 'frame' must not already be a :child-frame, nor must 'kline' already be a :k-lines of 'frame'."
  (declare (type k-line:k-line kline)
	   (type frame:frame frame))

  (trivial-object-lock:with-object-lock-held (frame :property :k-lines :test #'trivial-utilities:equals)
    (trivial-object-lock:with-object-lock-held (kline :property :child-frames :test #'trivial-utilities:equals)
      (let ((k-lines (ding:eigenschaft frame :k-lines))
	    (child-frames (ding:eigenschaft kline :child-frames)))
	(unless (and (null (member kline k-lines :test #'trivial-utilities:equals))
		     (null (member frame child-frames :test #'trivial-utilities:equals)))
	  (log:error "Ignore this problem and return nil." "The passed arguments are not valid.")
	  (cerror "Ignore this problem and return nil." 'invalid-arguments)
	  (return-from add-frame-to-kline nil))

	(push frame (child-frames kline))
	(push kline (frame:k-lines frame)))))

  (return-from add-frame-to-kline t))

