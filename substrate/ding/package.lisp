(uiop:define-package #:ding
  (:documentation "")
  (:use #:common-lisp)
  (:import-from :trivial-utilities #:equals)
  (:export #:ding-base
	   #:ding
	   #:ding-reference
	   #:id
	   #:dirty
	   #:persistent
	   #:newly-created
	   #:erstellungsdatum
	   #:ablaufdatum
	   #:activation-time
	   #:klasse
	   #:update-cycles
	   #:associations
	   #:p-list
	   #:without-change-notification
	   #:fully-serialize
	   #:property-serializable
	   #:inheritance-ignored
	   #:ignoring-inheritance
	   #:serializable-properties
	   #:serialize
	   #:deserialize
	   #:json-deserialize
	   #:eigenschaften
	   #:eigenschaft
	   #:associations
	   #:with-eigenschaft-cache
	   #:surpress-change-notification
	   #:register-ding
	   #:register-ding-simplified
	   #:unregistered-class
	   #:get-class-for-id
	   #:get-id-for-class
	   #:initialize-ding
	   #:separate-by-slots))

