# Marvin Ding Manual

###### \[in package DING\]
[![pipeline status](https://gitlab.com/marvin-aiot/marvin-device/marvin-base/badges/master/pipeline.svg)](https://gitlab.com/marvin-aiot/marvin-device/marvin-base/commits/master)

## Description



## Installing ding



## Working Example



## Exported Symbols

- [class] DING-BASE

- [class] DING-REFERENCE DING-BASE

- [class] DING DING-BASE

- [generic-function] DING-BASE-P OBJ

- [generic-function] DING-REFERENCE-P OBJ

- [generic-function] DING-P OBJ

- [accessor] ID DING-BASE (:ID)

- [accessor] DIRTY DING (= NIL)

- [accessor] PERSISTENT DING (:PERSISTENT = NIL)

- [accessor] NEWLY-CREATED DING (= NIL)

- [accessor] ERSTELLUNGSDATUM DING (:ERSTELLUNGSDATUM)

- [accessor] ABLAUFDATUM DING

- [accessor] ACTIVATION-TIME DING

- [accessor] KLASSE DING (:KLASSE)

- [accessor] HEBBIAN DING (:HEBBIAN = (COPY-LIST '(:UPDATE-CYCLES 0 :ASSOCIATIONS NIL)))

- [accessor] P-LIST DING (:P-LIST = NIL)

- [macro] WITHOUT-CHANGE-NOTIFICATION &BODY BODY



- [macro] FULLY-SERIALIZE &BODY BODY



- [generic-function] PROPERTY-SERIALIZABLE DING PORPERTY



- [function] INHERITANCE-IGNORED 

- [macro] IGNORING-INHERITANCE &BODY BODY

- [function] SERIALIZABLE-PROPERTIES DING



- [method] TRIVIAL-JSON-CODEC:SERIALIZE (OBJ DING) STREAM

- [function] DESERIALIZE PARSED &AUX (INITARGS NIL)



- [function] JSON-DESERIALIZE JSON

- [function] EIGENSCHAFTEN DING



- [generic-function] EIGENSCHAFT OBJECT PROPERTY &KEY IGNORE-INHERITANCE

- [generic-function] ASSOCIATIONS OBJ

- [function] OBJECT-COMPARE OBJ1 OBJ2

    Compare objects, considering Ding and Ding-Reference types.

- [function] APPLY-FILTER OBJECT FILTER &KEY IGNORE-INHERITANCE

- [function] EIGENSCHAFT-CACHE 

- [macro] WITH-EIGENSCHAFT-CACHE &BODY BODY



- [function] SURPRESS-CHANGE-NOTIFICATION 

- [function] REGISTER-DING-CLASS ID CLASS





## License Information

This library is released under the GNU General Public License. Please refer to the [LICENSE](https://gitlab.com/marvin-aiot/marvin-device/marvin-base/blob/master/LICENSE "License") to get the full licensing text.

## Contributing to this project

Please refer to the [CONTRIBUTING](https://gitlab.com/marvin-aiot/marvin-device/marvin-base/blob/master/CONTRIBUTING "Contributing") document for more information.
