;;;; Copyright (C) 2019 Eric Diethelm

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(in-package :ding)

(defsection @ding-manual (:title "Marvin Ding Manual")
  "[![pipeline status](https://gitlab.com/marvin-aiot/marvin-device/marvin-base/badges/master/pipeline.svg)](https://gitlab.com/marvin-aiot/marvin-device/marvin-base/commits/master)"
  (@ding-description section)
  (@ding-exported section)
  (@ding-license section)
  (@ding-contributing section))

(defsection @ding-description (:title "Description")
  "

Concept of Eigenschaft

Change notification

Serialization of a Ding


")

(defsection @ding-exported (:title "Exported Symbols")
  (initialize-ding function)
  (ding-base class)
  (ding-reference class)
  (ding class)
  (id (accessor ding-base))
  (dirty (accessor ding))
  (persistent (accessor ding))
  (newly-created (accessor ding))
  (erstellungsdatum (accessor ding))
  (ablaufdatum (accessor ding))
  (activation-time (accessor ding))
  (klasse (accessor ding))
  (hebbian (accessor ding))
  (p-list (accessor ding))
  (without-change-notification macro)
  (fully-serialize macro)
  (property-serializable generic-function)
  (inheritance-ignored function)
  (ignoring-inheritance macro)
  (deserialize function)
  (json-deserialize function)
  (eigenschaften function)
  (eigenschaft generic-function)
  (associations generic-function)
  (equals generic-function)
  (eigenschaft-cache function)
  (with-eigenschaft-cache macro)
  (surpress-change-notification function)
  (register-ding function)
  (register-ding-simplified function)
  (get-id-for-class function)
  (get-class-for-id function)
  (unregistered-class condition))

(defsection @ding-license (:title "License Information")
  "This library is released under GPLv3. Please refer to the [LICENSE](https://gitlab.com/marvin-aiot/marvin-device/marvin-base/blob/master/LICENSE 'License') to get the full licensing text.")

(defsection @ding-contributing (:title "Contributing to this project")
  "Please refer to the [CONTRIBUTING](https://gitlab.com/marvin-aiot/marvin-device/marvin-base/blob/master/CONTRIBUTING 'Contributing') document for more information.")

