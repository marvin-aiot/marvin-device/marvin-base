;;;; Copyright (C) 2019 Eric Diethelm

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(in-package :ding)

(defclass Ding-Base ()
  ((id :accessor id
       :initarg :id
       :type trivial-utilities:non-negative-fixnum
       :documentation "The unique ID of the thing."))
  (:documentation "The base class to represent a Thing in the system."))

(defmethod print-object ((obj Ding-Base) out)
  (print-unreadable-object (obj out :type nil)
    (format out "[Ding ~A]" (id obj))))

(defclass Ding-Reference (Ding-Base)
  ()
  (:documentation ""))

(defclass Ding (Ding-Base)
  ((dirty :accessor dirty
	  :initform 0
	  :type trivial-utilities:non-negative-fixnum
	  :documentation "")
   (persistent :accessor persistent
	       :initarg :persistent
	       :initform nil
	       :type boolean
	       :documentation "")
   (newly-created :accessor newly-created
		  :initform nil
		  :type boolean
		  :documentation "")
   (erstellungsdatum :accessor erstellungsdatum
		     :initarg :erstellungsdatum
		     :type (unsigned-byte 62)
		     :documentation "")
   (ablaufdatum :accessor ablaufdatum
		:type (unsigned-byte 62)
		:initform 0
		:documentation "")
   (activation-time :accessor activation-time
		    :type (unsigned-byte 62)
		    :initform 0
		    :documentation "")
   (klasse :accessor klasse
	   :initarg :klasse
	   :type (or null Ding Ding-Reference)
	   :documentation "")
   (update-cycles :accessor update-cycles
		  :initarg :update-cycles
		  :initform 0
		  :type trivial-utilities:non-negative-fixnum
		  :documentation "")
   (associations :accessor associations
		 :initarg :associations
		 :initform nil
		 :type list
		 :documentation "")
   (p-list :accessor p-list
	   :initarg :p-list
	   :initform nil
	   :type list
	   :documentation ""))
  (:documentation ""))

(defvar *surpress-change-notification* nil "Inhibits creation of a Frame when modifying a Ding's properity")
(defvar *serialize-ding-as-dingref* t)
(defvar *ignore-inheritance* nil)
(defvar *eigenschaft-cache* nil)

(defparameter *class-id-mapping* `((0 . ,(find-class 'ding:ding)))) ;; @REVIEW This seems like a workaround...
(defparameter *registered-dings* nil)
(defparameter *direct-registration-possible* nil)

(defmacro without-change-notification (&body body) ;; @REVIEW Why is this here? It is not used in this file...
  "Execute the forms *BODY* without generating any change notifications."
  `(let ((*surpress-change-notification* t))
     ,@body))

(defun surpress-change-notification ()
  "Indicate whether a change notification shall be generated."
  *surpress-change-notification*)

(defmacro fully-serialize (&body body)
  "Indicate that during the execution of the forms *BODY* the serialization of Ding instances shall not be reduced to Ding-Reference format."
  `(let ((*serialize-ding-as-dingref* nil))
     ,@body))

(defun inheritance-ignored ()
  "Indicate whetheror not the class - and its superclasses - of a Ding instance shall be considered when searching for a Eigenschaft."
  *ignore-inheritance*)

(defmacro ignoring-inheritance (&body body)
  "Execute the forms *BODY* ignoring all superclasses when searching for a Eigenschaft."
  `(let ((*ignore-inheritance* t))
     ,@body))

(defun eigenschaft-cache ()
  *eigenschaft-cache*)

(defmacro with-eigenschaft-cache (&body body)
  ""
  `(progn
     (if (eigenschaft-cache)
	 (log:trace "Reusing an existing cache for Ding Eigenschaft.")
	 (log:trace "Initiating a new cache for Ding Eigenschaft."))
     (let ((*eigenschaft-cache* (or (eigenschaft-cache) (make-eigenschaft-cache-hash-table))))
       (cl-custom-hash-table::with-custom-hash-table
	 ,@body))))

(defun property-serializable (ding property)
  (when (typep property 'ding:ding-base)
    (return-from property-serializable t))

  (trivial-utilities:aif (car (member (intern (symbol-name property) "KEYWORD") (trivial-utilities:all-slots (class-of ding))
                                      :key #'(lambda (x) (intern (symbol-name (c2mop:slot-definition-name x)) "KEYWORD"))))
                         (when (c2mop:slot-definition-initargs it)
                           (return-from property-serializable t))
                         (return-from property-serializable t))

  (return-from property-serializable nil))

(defmethod trivial-json-codec:serialize ((obj Ding-Reference) stream)
  ""
  (declare (type stream stream))
  (format stream "#DING-~a?" (id obj)))

(defmethod trivial-json-codec:serialize ((obj Ding) stream)
  (declare (type stream stream))
  (if *serialize-ding-as-dingref*
      (format stream "#DING-~a?" (id obj))
      (let ((*serialize-ding-as-dingref* t))
	(princ #\< stream)
	(loop
	   for elm in (trivial-utilities:collect-persistent-slots obj)
	   with needs-comma = nil
	   when (slot-boundp obj (c2mop:slot-definition-name elm))
	   do (if needs-comma
		  (princ #\, stream)
		  (setf needs-comma t))
	     (log:info (car (c2mop:slot-definition-initargs elm)))
	     (trivial-json-codec:serialize (car (c2mop:slot-definition-initargs elm)) stream)
	     (princ #\, stream)

	   ;; @REVIEW We (blindly) expect (c2mop:slot-definition-name elm) to hold a function
	   ;;(trivial-json-codec:serialize (funcall (the function (c2mop:slot-definition-name elm)) obj) stream)
	     ;; @REVIEW Could we use slot-value instead of funcalling the name?
	     (trivial-json-codec:serialize (funcall (c2mop:slot-definition-name elm) obj) stream))

	(princ #\> stream))))

(defun read-ding-reference (json start-position read-table)
  (declare (type simple-string json)
	   (type fixnum start-position)
	   (ignore read-table))

  (multiple-value-bind (id end-pos)
      (trivial-json-codec:read-number json (+ (length "#DING-") start-position))
    (declare (type fixnum end-pos))

    (unless (and id (eq #\? (char json end-pos)))
      (error "Could not correctly read Ding reference ID at '~a'" (subseq json start-position (min (1- (length json)) (+ start-position 15)))))

      (return-from read-ding-reference (values
					(list :REF
					      id)
					(1+ end-pos)))))

(define-condition unregistered-class (error)
  ((klasse :initarg :klasse
	   :reader klasse
	   :initform nil)
   (id :initarg :id
       :reader id
       :initform nil))
  (:documentation "A condition signaling the error of trying to access a class (of its id) that was not registered via *register-ding* or *register-ding-simplified*")
  (:report
   (lambda (condition stream)
     (with-slots (id klasse) condition
       (let ((complement nil))
	 (when klasse
	   (setf complement (format nil " class ~a" klasse)))
	 (when id
	   (when complement
	     (setf complement (concatenate 'string complement " and")))
	   (setf complement (concatenate 'string complement (format nil " id ~a" id))))
	 (format stream "No class registration found for~a."
		 complement))))))

(defun json-deserialize (json)
  (deserialize (trivial-json-codec:deserialize-json json
						    :read-table (list (cons #\# #'read-ding-reference))
						    :constructors (list (cons :REF #'(lambda (args)
										       (make-instance 'ding-reference
												      :id (first args))))))))

(defun deserialize (parsed &aux (initargs nil))
  ""
  (unless parsed
    (return-from deserialize (make-instance 'Ding)))

  (let ((klasse (cadr (member :klasse parsed)))
	(p-list (getf parsed :p-list)))

    (let ((object-class nil)
	  (parent-class klasse))
      (if klasse
	  (labels ((get-class (x)
		     (handler-case (get-class-for-id (id x))
		       (unregistered-class (c)
			 (declare (ignore c))
			 nil))))
	    (iterate:iterate
	      (iterate:until (or (setf object-class (get-class parent-class))
				 (null (ding:eigenschaft parent-class :klasse))))
	      (setf parent-class (ding:eigenschaft parent-class :klasse))))
	  (setf object-class (find-class 'ding)))

      (unless object-class
	(log:error "No class defined for class id ~a." (id klasse))
	(cerror "Return nil" "No class defined for class id ~a." (id klasse))
	(return-from deserialize nil))

      (setf initargs
	    (append
	     (iterate:iterate
	       (iterate:with slots = (trivial-utilities:flatten (remove-if #'null
									   (mapcar #'c2mop:slot-definition-initargs
										   (trivial-utilities:all-slots object-class)))))
	       (iterate:for elm on p-list by #'cddr)
	       (if (member (car elm) slots)
		   (iterate:appending (list (car elm) (cadr elm)) into ding-items)
		   (iterate:appending (list (car elm) (cadr elm)) into p-list-items))
	       (iterate:finally (return (append (list :p-list p-list-items) ding-items))))
	     (iterate:iterate
	       (iterate:for elm on parsed by #'cddr)
	       (unless (eq :p-list (car elm))
		 (iterate:appending (list (car elm) (cadr elm)))))))

      (apply #'make-instance object-class initargs))))

(defun compile-expr (expr)
  (declare (type cons expr))
  (let ((search-params (iterate:iterate
			(iterate:for (eigenschaft value) on expr by #'cddr)
			(iterate:collect (list :eigenschaft :?_ eigenschaft value)))))
    (if (> (length search-params) 1)
	(push :and search-params)
	(setf search-params (car search-params)))

    (return-from compile-expr search-params)))

(defun create-if-inexsitent (plist find-fn create-fn)
  ""
  (declare (type cons plist)
	   (type function find-fn create-fn))

  (trivial-utilities:aif (funcall find-fn (compile-expr plist))
			 it
			 (progn
			   (log:info "Creating a Ding '~a' because it did not exist yet." plist)
			   (funcall create-fn (separate-by-slots plist)))))

(defun register-ding (registration-fn)
  ""
  (if *direct-registration-possible*
      (multiple-value-bind (id klass)
	  (funcall registration-fn)
	(register-ding-class id klass))
      (push registration-fn *registered-dings*))
  (values nil))

(defun register-ding-simplified (name klass)
  ""
  (ding:register-ding #'(lambda ()
			  (let ((find-object-symb (uiop:find-symbol* "FIND-DING" :marvin nil))
				(create-object-symb (uiop:find-symbol* "CREATE-OBJECT" :marvin nil)))
			    (assert find-object-symb)
			    (assert create-object-symb)

			    (let* ((find-object-fn (symbol-function find-object-symb))
				   (create-object-fn (symbol-function create-object-symb))
				   (name-prop (create-if-inexsitent '(:graphie "Name") find-object-fn create-object-fn)) ;; @REVIEW How to indicate the class of this ('Wort' id: 7)
				   (name-field (create-if-inexsitent `(:graphie ,name) find-object-fn create-object-fn))
				   (this (create-if-inexsitent `(,name-prop ,name-field) find-object-fn create-object-fn)))

			    (assert this)
			    (values (ding:id this) klass))))))

(defun register-ding-class (id class)
  ""
  (declare (type fixnum id))
  (trivial-utilities:awhen (assoc id *class-id-mapping*)
    (if (eq class (cdr it))
	(return-from register-ding-class nil)
	(error "Id ~a already in use for ~a" id (cdr it))))

  (push (cons id class) *class-id-mapping*))

(defun get-class-for-id (id)
  "Discover the class associated with a Ding ID."
  (declare (type fixnum id))
  (trivial-utilities:aif (cdr (assoc id *class-id-mapping*))
			 it
			 (error 'unregistered-class :id id)))

(defun get-id-for-class (klass)
  "Discover the Ding ID associated with a class."
  (declare (type class klass))
  (trivial-utilities:aif (car (rassoc klass *class-id-mapping*))
			 it
			 (error 'unregistered-class :klasse klass)))

(defun initialize-ding ()
  "Execute necessary preparations."
  (iterate:iterate
    (iterate:for ding-init-fn in *registered-dings*)
    (multiple-value-bind (id klass)
	(funcall ding-init-fn)
      (register-ding-class id klass)))
  (setf *direct-registration-possible* t))
      
(defun eigenschaften (ding)
  ""
  (declare (type Ding ding))

  ;; @TODO Include all slots as eigenschaften in plist
  (let* ((all (mapcan
	       #'(lambda (x) (and (slot-boundp ding x) (list (intern (symbol-name x) "KEYWORD") (slot-value ding x))))
	       (mapcar #'c2mop:slot-definition-name (trivial-utilities:all-slots (class-of ding)))))
	 (plist (getf all :p-list)))

    (remf all :p-list)
    (setf all (append all plist))

    (trivial-utilities:awhen (and (slot-boundp ding 'klasse) (typep (klasse ding) (find-class 'ding)) (klasse ding))
      (loop for x on (eigenschaften it) by #'cddr
	 unless (getf all (car x))
	 do (setf all (append (subseq x 0 2) all))))

    (return-from eigenschaften all)))

(defgeneric eigenschaft (object property &key ignore-inheritance)
  (:method (object property &key ignore-inheritance)
    (declare (ignore object property ignore-inheritance))
    (error "Some extention of the implementation of ding:eigenschaft is missing. Is working-memory loaded?")))

(defmethod eigenschaft :around (object property &key ignore-inheritance)
  (declare (ignore ignore-inheritance))
  (unless (and object property)
    (log:warn "Eigenschaft called with nil: Ding ~a Property ~a" object property)
    (cerror "Ignore and return" "Eigenschaft called with nil: Ding ~a Property ~a" object property) 
    (return-from eigenschaft (the (values t boolean) (values nil nil))))

  (when (eigenschaft-cache)
    (multiple-value-bind (v f)
	(gethash (list object property) (eigenschaft-cache))
      (when f
	(log:trace "Entry for ~a | ~a found in cache. Value is '~a'." object property v)
	(return-from eigenschaft (the (values t boolean)
				      (values (car v) (cadr v)))))))

  (multiple-value-bind (v f) (call-next-method)
    (when (eigenschaft-cache)
      (log:trace "Storing entry for ~a | ~a in cache. Value is '~a'." object property v)
      (setf (gethash (list object property) (eigenschaft-cache)) (list v f)))
    
    (return-from eigenschaft (the (values t boolean)
				  (values v f)))))

(let ((not-found (gensym))
      (nil-value (gensym)))
  (defmethod eigenschaft ((object ding) property &key ignore-inheritance)
    (multiple-value-bind (v f)
	(block cached-scope
	  ;; Check also if property is a slot
	  (trivial-utilities:aif (and (symbolp property)
				      (member (symbol-name property)
					      (mapcar #'c2mop:slot-definition-name (trivial-utilities:all-slots (class-of object)))
					      :key #'symbol-name
					      :test #'string=))
				 (return-from cached-scope (the (values t boolean &optional)
								(values  (if (slot-boundp object (car it))
									     (slot-value object (car it))
									     nil)
									 t))))
	  
	  (let ((value (loop for elm on (p-list object) by #'cddr
			  when (equals property (car elm))
			  do (return (cadr elm))
			  finally (return not-found))))

	    (when (eq not-found value)
	      (when (or ignore-inheritance (inheritance-ignored))
		(return-from cached-scope (the (values t boolean &optional) (values nil nil))))

	      (when (and (slot-boundp object 'klasse) (klasse object))
		(let ((parent-value
		       ;; @TODO :param of instance MUST be 3! Must use breadth-first instead of depth-first!
		       ;;
		       ;;  class (:id 5 :param 9)  class (:id 6 :param 7)     class (:id 7 :param 5)
		       ;;             \            /                          /
		       ;;             class (:id 12)      class (:id 13 :param 3)
		       ;;                   \              /
		       ;;                   instance (:id 17)

		       ;;(setf (get ding :klasse) (replace-all-references (get ding :klasse)))
		       
		       (multiple-value-bind (v f) (eigenschaft (klasse object) property :ignore-inheritance ignore-inheritance)
			 (declare (type boolean f))
			 (when f
			   (if (null v)
			       nil-value
			       v)))))

		  (return-from cached-scope
		    (the (values t boolean &optional)
			 (cond ((eq parent-value nil-value)
				(values nil t))
			       ((null parent-value)
				(values nil nil))
			       (t
				(values parent-value t)))))))
	      
	      (return-from cached-scope (the (values t boolean &optional) (values nil nil))))

	    (return-from cached-scope (the (values t boolean &optional) (values value t)))))

      (return-from eigenschaft
	(the (values t boolean &optional)
	     (values v f))))))

(defgeneric (setf eigenschaft) (value object property))

(defmethod (setf eigenschaft) (value (object ding:ding) property)
  (unless (and object property)
    (log:warn "Eigenschaft called with nil: Ding ~a Property ~a" object property)
    (cerror "Ignore and return" "Eigenschaft called with nil: Ding ~a Property ~a" object property) 
    (return-from eigenschaft (the (values t boolean) (values nil nil))))

  (unless (eq (eigenschaft object property :ignore-inheritance t) value)
    (let ((prop-hash (sxhash (eigenschaft object property :ignore-inheritance t))))
      (trivial-object-lock:with-object-lock-held (object :property property :test #'equals) ;; @REVIEW If I hold the lock for this object, this should not be a new lock
	(unless (eq prop-hash (sxhash (eigenschaft object property :ignore-inheritance t)))
	  (error "Eigenschaft to be updated was modified since lock was acquired. @TODO THIS MUST BE A CONDITION TO BE HANDELED!"))
	
	(let ((new-value (if (typep property 'ding:ding-base)
			     (setf (getf (p-list object) property) value)
			     (trivial-utilities:aif (member (symbol-name property)
							(mapcar #'c2mop:slot-definition-name (trivial-utilities:all-slots (class-of object)))
							:key #'symbol-name
							:test #'string=)
						(setf (slot-value object (car it)) value)
						(setf (getf (p-list object) property) value)))))
	  
	  ;; @TODO Update the value 'Modified-Date'
	  (when (eigenschaft-cache)
	    (log:trace "Updating entry for ~a | ~a in cache." object property)
	    (setf (gethash (list object property) (eigenschaft-cache)) (list new-value t)))
	  
	  (when (property-serializable object property)
	    (setf (dirty object) (logior
				  (dirty object)
				  (if (or (eq property :update-cycles)
					  (eq property :associations))
				      2
				      1))))

	  (return-from eigenschaft new-value))))))

(defmethod (setf eigenschaft) (value (object null) property)
  (error "Setting null object property '~a' to '~a'." property value))

(defmethod (setf eigenschaft) (value object (property null))
  (error "Setting null property of Ding (~a) to '~a'." (id object) value))

(defmethod equals ((obj1 ding) (obj2 ding) &key &allow-other-keys)
  "Compare objects, considering ding and ding-reference types."
  (the (values boolean &optional)
       (eq obj1 obj2)))

(declaim (inline equals-dings))
(defun equals-dings (obj1 obj2)
  (declare (type ding-base obj1 obj2))
  
  (the (values boolean &optional)
       (eq (id obj1) (id obj2))))

(defmethod equals ((obj1 ding) (obj2 ding-reference) &key &allow-other-keys)
  (equals-dings obj1 obj2))

(defmethod equals ((obj1 ding-reference) (obj2 ding) &key &allow-other-keys)
  (equals-dings obj1 obj2))

(defmethod equals ((obj1 ding-reference) (obj2 ding-reference) &key &allow-other-keys)
  (equals-dings obj1 obj2))

(defun sxhash-object (f)
  (declare (type (trivial-types:tuple Ding t)))
  (sxhash (+ (sxhash (id (car f))) (sxhash (cadr f)))))

(cl-custom-hash-table:define-custom-hash-table-constructor 
    make-eigenschaft-cache-hash-table
    :test equals
    :hash-function sxhash-object)

(defun separate-by-slots (eigenschaften)
  ""
  ;; @TODO We have lots of *reverse* calls here, just because we append 'at beginning'. If this is the best is questionable...
  (let* ((klasse (trivial-utilities:aif (member :klasse eigenschaften)
					(handler-case
					    (get-class-for-id (if (typep (cadr it) 'ding)
								  (id (cadr it))
								  (cadr it)))
					  (unregistered-class (c)
					    (declare (ignore c))
					    (find-class 'ding)))
					(find-class 'ding)))
	 (slots (trivial-utilities:all-slots klasse)))

    ;; Every *eigenschaft* for which no slot exists in the class push it to :p-list
    (iterate:iterate
      (iterate:for (e v) on eigenschaften by #'cddr)
      (if (eq e :p-list)
	  ;; Handle when the eigenschaft :p-list exists in eigenschaften
	  (iterate:appending (reverse v) into p-list at beginning)	  
	  (if (member e slots :key #'(lambda (x) (intern (symbol-name (c2mop:slot-definition-name x)) "KEYWORD")))
	      (iterate:appending (list v e) into in-slots at beginning)
	      (iterate:appending (list v e) into p-list at beginning)))
      (iterate:finally (return (append (reverse in-slots) '(:p-list) (list (reverse p-list))))))))
