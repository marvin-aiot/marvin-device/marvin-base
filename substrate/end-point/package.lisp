(uiop:define-package #:end-point
  (:documentation "")
  (:use #:common-lisp)
  (:export #:initialize-end-points
	   #:shutdown-end-points
	   #:add-http-end-point
	   #:add-ws-end-point
	   #:remove-http-end-point
	   #:remove-ws-end-point
	   #:resource
	   #:resource-name))

