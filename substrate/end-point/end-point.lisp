;;;; Copyright (C) 2020 Eric Diethelm

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(in-package :end-point)

(defvar *server* nil)
(defvar *http-resources* '())
(defvar *ws-resources* '())
(defvar *base-route* "api")

#|

Handles all API related logic

- OK   API Versions
- OK   Hooks for extension
- TODO Authentication



|#


;; --------------------- BEGIN Public Interface ---------------------

(defun initialize-end-points ()
  "Start the HTTP and WebSocket server to handle end point requests. Requires the 'END-POINT-PORT' (integer) and optionally 'END-POINT-ADDRESS' (string, default '\*') from the configuration."
  (log:info "Starting Marvin API.")
  (let ((port (piggyback-parameters:get-value :end-point-port
					      #-non-persistent-working-memory :file-first
					      #+non-persistent-working-memory :file-only))
	(address (piggyback-parameters:get-value :end-point-address
					      #-non-persistent-working-memory :file-first
					      #+non-persistent-working-memory :file-only)))
    (unless address
      (setf address "127.0.0.1"))

    (unless port
      (setf port 8000))
    
    (log:info "Starting Marvin API on ~a:~a." address port)
    (start-server address port)

    ;; Add a HTTP end-point :end-points
    (add-http-end-point :end-points
			"List all avaliable End Points."
			:v1
			:get
			#'list-end-points)

    
    (log:info "Marvin API started.")))

(defun shutdown-end-points ()
  "Stop the HTTP and WebSocket server that handles end point requests and clear all end-points (if not already done with *remove-\*-end-point*)."
  (log:info "Stopping Marvin API.")
  (stop-server)
  
  (remove-http-end-point :end-points :v1 :get)

  (when *ws-resources*
    (log:warn "Not all WebSocket end-points were removed before stopping the server. They will be force cleaned.")
    (log:debug "Following WS routes were not removed: ~{~a~^, ~}" (iterate:iterate
		    (iterate:for resource in *ws-resources*)
		    (iterate:collecting (route resource))))
    (setf *ws-resources* nil))

  (when *http-resources*
    (log:warn "Not all HTTP end-points were removed before stopping the server. They will be force cleaned.")
    (log:debug "Following HTTP routes were not removed: ~{~a~^, ~}" (iterate:iterate
		    (iterate:for resource in *http-resources*)
		    (iterate:collecting (route resource))))
    (setf *http-resources* nil))
  
  (log:info "Marvin API stopped."))

(defun add-ws-end-point (name description version http-method fn &key auth-required parents on-connect on-disconnect)
  "Register a new WebSocket end-point. The route is defined by '/api/{VERSION}/{PARENTS concatenated with slashes}/{NAME}'  
**NAME** - A keyword naming this end-point  
**DESCRIPTION** -  A short description of this end-point.
**VERSION** - A keyword defining the version of the API in the form ':V{integer}'  
**HTTP-METHOD** -  One of the following: :GET :POST :PUT :PATCH :DELETE
**FN** - The function to handle incomming requests. Receives two arguments: (END-POINT:RESOURCE HUNCHENTOOT:REQUEST)   
**AUTH-REQUIRED** - Boolean indicating whether access to this end-point needs authentication (**T**) or not (**NIL** *default*).  
**PARENTS** - A list of keywords defining a hierarchy in the generated route. Might be empty.  
**ON-CONNECT** - A function to handle new connecting users. Receives two arguments: (END-POINT:RESOURCE HUNCHENSOCKET:WEBSOCKET-CLIENT)  
**ON-DISCONNECT** - A function to handle users disconnecting. Receives two arguments: (END-POINT:RESOURCE HUNCHENSOCKET:WEBSOCKET-CLIENT)  "
  (declare (type keyword name version)
	   (type (member :get :post :put :patch :delete) http-method)
	   (type string description)
	   (type boolean auth-required)
	   (type list parents))

  (pushnew (make-instance 'ws-resource
			  :name name
			  :route (format nil "~(/~a/~a~{/~a~^~}/~a~)" *base-route* version parents name)
			  :version version
			  :http-method http-method
			  :parents parents
			  :description description
			  :auth-required auth-required
			  :handler-fn fn
			  :on-connect-fn on-connect
			  :on-disconnect-fn on-disconnect)
	   *ws-resources*)
  (log:info "Added new WS route for ~a '~a'." http-method (format nil "~(/~a/~a~{/~a~^~}/~a~)" *base-route* version parents name)))

(defun add-http-end-point (name description version http-method fn &key auth-required parents)
  "Register a new HTTP end-point. The route is defined by '/api/{VERSION}/{PARENTS concatenated with slashes}/{NAME}'  
**NAME** - A keyword naming this end-point  
**DESCRIPTION** - A short description of this end-point.
**VERSION** - A keyword defining the version of the API in the form ':V{integer}'  
**HTTP-METHOD** - One of the following: :GET :POST :PUT :PATCH :DELETE
**FN** - The function to handle incomming requests. Receives two arguments: (END-POINT:RESOURCE HUNCHENTOOT:REQUEST)   
**AUTH-REQUIRED** - Boolean indicating whether access to this end-point needs authentication (**T**) or not (**NIL** *default*).  
**PARENTS** - A list of keywords defining a hierarchy in the generated route. Might be empty."
  (declare (type keyword name version)
	   (type (member :get :post :put :patch :delete) http-method)
	   (type string description)
	   (type boolean auth-required)
	   (type list parents))

  (pushnew (make-instance 'http-resource
			  :name name
			  :route (format nil "~(/~a/~a~{/~a~^~}/~a~)" *base-route* version parents name)
			  :version version
			  :http-method http-method
			  :parents parents
			  :description description
			  :auth-required auth-required
			  :handler-fn fn)
	   *http-resources*)
  (log:info "Added new HTTP route for ~a '~a'." http-method (format nil "~(/~a/~a~{/~a~^~}/~a~)" *base-route* version parents name)))

(defun remove-ws-end-point (name version http-method &optional parents)
  "Remove an WebSocket end-point identified by *name*, *version*, *http-method* and *parents*."
  (setf *ws-resources* (delete-if #'(lambda (x)
				      (and (eq name (resource-name x))
					   (eq version (version x))
					   (eq http-method (http-method x))
					   (equal parents (parents x))))
				  *ws-resources*))
  t)

(defun remove-http-end-point (name version http-method &optional parents)
  "Remove an HTTP end-point identified by *name*, *version*, *http-method* and *parents*."
  (setf *http-resources* (delete-if #'(lambda (x)
					(and (eq name (resource-name x))
					     (eq version (version x))
					     (eq http-method (http-method x))
					     (equal parents (parents x))))
				    *http-resources*))
  t)

;; --------------------- END Public Interface ---------------------

(defclass resource ()
  ((name :initarg :name :initform (error "This resource needs a name!") :reader resource-name)
   (parents :initarg :parents :initform nil :reader parents)
   (route :initarg :route :initform (error "This resource needs a route!") :reader route)
   (version :initarg :version :initform (error "This resource needs a version!") :reader version)
   (description :initarg :description :initform (error "This resource needs a description!") :reader description)
   (auth-required :initarg :auth-required :initform nil :reader auth-required)
   (http-method :initarg :http-method :initform (error "This resource needs a HTTP method!") :reader http-method)
   (handler-fn :initarg :handler-fn :initform (error "This resource needs a message handler function!") :reader handler-fn)))

(defclass http-resource (resource)
  ())

(defclass ws-resource (resource hunchensocket:websocket-resource)
  ((on-connect-fn :initarg :on-connect-fn :initform (error "This resource needs an on-connect handler function!") :reader on-connect-fn)
   (on-disconnect-fn :initarg :on-disconnect-fn :initform (error "This resource needs an on-disconnect handler function!") :reader on-disconnect-fn))
  (:default-initargs :client-class 'user))

(defclass user (hunchensocket:websocket-client)
  ((name :initarg :user-agent :reader name :initform (error "Name this user!"))))

(defclass http-acceptor (hunchentoot:acceptor)
  ())

(defmethod hunchentoot:acceptor-dispatch-request ((acceptor http-acceptor) request)
  (if (string-equal "websocket" (cdr (assoc :upgrade (hunchentoot:headers-in request))))
      (call-next-method)
      (progn
	(setf (hunchentoot:header-out "Access-Control-Allow-Origin") "*") ;; Othervise has to be fully qualified: "http://127.0.0.1:8080"
	(setf (hunchentoot:content-type*) "application/json")
	(trivial-utilities:aif (find-http-resource request)
			       (funcall (handler-fn it) it request) ;; @TODO Extract parameters from the request
			       (call-next-method)))))

(defclass super-acceptor (http-acceptor
			  hunchensocket:websocket-acceptor)
  ())

(defun find-http-resource (request)
  (declare (type list *http-resources*))
  (let ((method (hunchentoot:request-method request))
	(script-name (hunchentoot:script-name request)))
    (find-if #'(lambda (x)
		 (and (eq method (http-method x))
		      (string-equal script-name (route x))))
	     *http-resources*)))

(defun find-ws-resource (request)
  (declare (type list *ws-resources*))
  (find (hunchentoot:script-name request) *ws-resources* :test #'string-equal :key #'route))

(defmethod hunchensocket:client-connected ((resource resource) user)
  (when (on-connect-fn resource)
    (funcall (on-connect-fn resource) resource user))
  
  (log:info "New client connection accepted: " user))

(defmethod hunchensocket:client-disconnected ((resource resource) user)
  (when (on-disconnect-fn resource)
    (funcall (on-disconnect-fn resource) resource user))
  
  (log:info "User disconnected: " user))

(defmethod hunchensocket:text-message-received ((resource resource) user message)
  (declare (type string message))
  (log:trace "text-message-received: '~a'" message)

  (when (string= message "ping")
    (log:trace "Got a Ping message!")
    (return-from hunchensocket:text-message-received))

  (when (handler-fn resource)
    (funcall (handler-fn resource) resource user message)))

(defun start-server (address port)
  (when *server*
    (log:warn "End Point server is already running! Will ignore this call.")
    (return-from start-server nil))
  
  (pushnew 'find-ws-resource hunchensocket:*websocket-dispatch-table*)
  (setf *server* (make-instance 'super-acceptor :address address :port port))
  (hunchentoot:start *server*)
  (return-from start-server t))

(defun stop-server ()
  (unless *server*
    (log:warn "End Point server is not running! Will ignore this call.")
    (return-from stop-server nil))
  
  (hunchentoot:stop *server*) ;; https://github.com/edicl/hunchentoot/pull/139
  (setf *server* nil)
  (return-from stop-server t))

(defun list-end-points (resource request)
  (declare (ignore resource request))
  (with-output-to-string (stream)
    (princ #\[ stream)
    (format stream "{ \"end-point-type\" : \"HTTP\",
\"end-points\" : [
")
    (iterate:iterate
      (iterate:for resource in *http-resources*)
      (unless (iterate:first-time-p)
	(princ #\, stream))
      (format stream
	      "{
  \"description\":\"~a\",
  \"route\":\"~a\",
  \"http-method\":\"~a\",
  \"version\":\"~a\",
  \"auth-required\":~a
}
"
	      (description resource)
	      (route resource)
	      (http-method resource)
	      (version resource)
	      (if (auth-required resource) "true" "false")))
    (princ #\] stream)
    (princ #\} stream)
    (princ #\, stream)
    (terpri stream)

    (format stream "{ \"end-point-type\" : \"WS\",
\"end-points\" : [
")
    (iterate:iterate
      (iterate:for resource in *ws-resources*)
      (unless (iterate:first-time-p)
	(princ #\, stream))
      (format stream
	      "{
  \"description\":\"~a\",
  \"route\":\"~a\",
  \"http-method\":\"~a\",
  \"version\":\"~a\",
  \"auth-required\":~a
}
"
	      (description resource)
	      (route resource)
	      (http-method resource)
	      (version resource)
	      (if (auth-required resource) "true" "false")))
    (princ #\] stream)
    (princ #\} stream)
    (princ #\] stream)))

