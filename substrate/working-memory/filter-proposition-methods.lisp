;;;; Copyright (C) 2019 Eric Diethelm

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(in-package :working-memory)

(trivial-filter:create-filter-method :and
  (declare (ignore backtracker))
  (let ((value bindings))
    (loop for elm on args
       do
	 (let ((val (apply-filter value (car elm) :ignore-inheritance ignore-inheritance :consider-revisions consider-revisions)))
	   (unless (trivial-continuation:result val)
	     (setf value nil)
	     (return))
	   (setf value (trivial-continuation:result val))))
    (make-instance 'trivial-continuation:continuation-result
		   :operation :terminate
		   :result value
		   :continuation nil)))

(create-filter-method :or
  (loop for elm on args
     do
       (let* ((bindings-copy (clone bindings))
	      (val (apply-filter bindings (car elm) :ignore-inheritance ignore-inheritance :consider-revisions consider-revisions)))
	 (when (trivial-continuation:result val)
	   (return (trivial-continuation:combine-continuations val
							       #'(lambda ()
								   (self bindings-copy (cdr elm) backtracker ignore-inheritance consider-revisions))))))
     finally (make-instance 'trivial-continuation:continuation-result
			    :operation :terminate
			    :result nil
			    :continuation nil)))

(create-filter-method :not
  (declare (ignore backtracker))
  (let ((val (apply-filter bindings (first args) :ignore-inheritance ignore-inheritance :consider-revisions consider-revisions)))
    (make-instance 'trivial-continuation:continuation-result
		   :operation :terminate
		   :result (if (trivial-continuation:result val) nil bindings)
		   :continuation (trivial-continuation:continuation val))))

(create-filter-method :eigenschaft
  ;; :eigenschaft expects 3 agruments:
  ;;  1. object
  ;;  2. property
  ;;  3. value
  ;; Each one can be a placeholder, but not all three simultameously.
  (declare (ignore backtracker))
  (let* ((obj (if (typep (first args) 'trivial-variable-bindings:place-holder) (trivial-variable-bindings:bound-variable-value (first args) bindings) (first args)))
	 (prop (if (typep (second args) 'trivial-variable-bindings:place-holder) (trivial-variable-bindings:bound-variable-value (second args) bindings) (second args)))
	 (value (eigenschaft obj prop :ignore-inheritance ignore-inheritance))
	 (ret bindings))
    (if (typep (third args) 'trivial-variable-bindings:place-holder)
	(setf (trivial-variable-bindings:bound-variable-value (third args) bindings) value)
	(unless (trivial-utilities:equals value (third args))
	    (setf ret nil)))
    
    (make-instance 'trivial-continuation:continuation-result
		   :operation :terminate
		   :result ret
		   :continuation nil)))

(create-filter-method :exist
  ;; :exist expects 3 agruments:
  ;;  1. object
  ;;  2. property
  ;;  3. result, a boolean (found/not found)
  ;; Each one can be a placeholder, but not all three simultameously.
  (declare (ignore backtracker))
  (let ((obj (if (typep (first args) 'trivial-variable-bindings:place-holder) (trivial-variable-bindings:bound-variable-value (first args) bindings) (first args)))
	(prop (if (typep (second args) 'trivial-variable-bindings:place-holder) (trivial-variable-bindings:bound-variable-value (second args) bindings) (second args))))
    (make-instance 'trivial-continuation:continuation-result
		   :operation :terminate
		   :result (if (eigenschaft obj prop :ignore-inheritance ignore-inheritance)
			       bindings
			       nil)
		   :continuation nil)))

(create-filter-method :contains
  ;; :contains expects 3 agruments:
  ;;  1. object
  ;;  2. list
  ;;  3. result, a boolean (found/not found)
  ;; Each one can be a placeholder, but not all three simultameously.
  (declare (ignore backtracker))
  (let ((obj (if (typep (first args) 'trivial-variable-bindings:place-holder) (trivial-variable-bindings:bound-variable-value (first args) bindings) (first args)))
	(container (if (typep (second args) 'trivial-variable-bindings:place-holder) (trivial-variable-bindings:bound-variable-value (second args) bindings) (second args))))
    (make-instance 'trivial-continuation:continuation-result
		   :operation :combine
		   :result (if (member obj container :test #'(lambda (x y) (trivial-utilities:equals x y :consider-revisions consider-revisions)))
			       bindings
			       nil)
		   ;; @TODO Combine continuations of val1 and val2 
		   :continuation nil)))

(create-filter-method :lt
  ;; :lt expects 3 agruments:
  ;;  1. value_1
  ;;  2. value_2
  ;;  3. result, a boolean
  ;; Each one can be a placeholder, but not all three simultameously.
  (declare (ignore backtracker))
  (let ((lhs (if (typep (first args) 'trivial-variable-bindings:place-holder) (trivial-variable-bindings:bound-variable-value (first args) bindings) (first args)))
	(rhs (if (typep (second args) 'trivial-variable-bindings:place-holder) (trivial-variable-bindings:bound-variable-value (second args) bindings) (second args)))
	(value bindings))
    
    (cond ((or (typep lhs 'trivial-variable-bindings:place-holder)
	       (typep rhs 'trivial-variable-bindings:place-holder))
	   (setf value nil)
	   (log:error "Cannot handle generic case of :le with '~a'" args))
	  ((typep (third args) 'trivial-variable-bindings:place-holder)
	   (setf (trivial-variable-bindings:bound-variable-value (third args) bindings) (< lhs rhs)))
	  (t
	   (unless (eq (< lhs rhs) (third args))
	     (setf value nil))))
    
    (make-instance 'trivial-continuation:continuation-result
		   :operation :combine
		   :result value
		   :continuation nil)))

(create-filter-method :le
  ;; :le expects 3 agruments:
  ;;  1. value_1
  ;;  2. value_2
  ;;  3. result, a boolean
  ;; Each one can be a placeholder, but not all three simultameously.
  (declare (ignore backtracker))
  (let ((lhs (if (typep (first args) 'trivial-variable-bindings:place-holder) (trivial-variable-bindings:bound-variable-value (first args) bindings) (first args)))
	(rhs (if (typep (second args) 'trivial-variable-bindings:place-holder) (trivial-variable-bindings:bound-variable-value (second args) bindings) (second args)))
	(value bindings))
    
    (cond ((or (typep lhs 'trivial-variable-bindings:place-holder)
	       (typep rhs 'trivial-variable-bindings:place-holder))
	   (setf value nil)
	   (log:error "Cannot handle generic case of :le with '~a'" args))
	  ((typep (third args) 'trivial-variable-bindings:place-holder)
	   (setf (trivial-variable-bindings:bound-variable-value (third args) bindings) (<= lhs rhs)))
	  (t
	   (unless (eq (<= lhs rhs) (third args))
	     (setf value nil))))
    
    (make-instance 'trivial-continuation:continuation-result
		   :operation :combine
		   :result value
		   :continuation nil)))

(create-filter-method :eq
  ;; :eq expects 2 agruments:
  ;;  1. value_1
  ;;  2. value_2
  ;; Each one can be a placeholder, but not all three simultameously.
  (declare (ignore backtracker))
  (let ((lhs (if (typep (first args) 'trivial-variable-bindings:place-holder) (trivial-variable-bindings:bound-variable-value (first args) bindings) (first args)))
	(rhs (if (typep (second args) 'trivial-variable-bindings:place-holder) (trivial-variable-bindings:bound-variable-value (second args) bindings) (second args)))
	(value bindings))
    (cond ((and (typep lhs 'trivial-variable-bindings:place-holder)
		(typep rhs 'trivial-variable-bindings:place-holder))
	   ;; @TODO Set bindings to ((lhs . rhs) (rhs . lhs))
	   (setf value nil)
	   (log:error "Cannot handle generic case of :eq with '~a'" args))
	  ((typep lhs 'trivial-variable-bindings:place-holder)
	   (setf (trivial-variable-bindings:bound-variable-value lhs bindings) rhs))
	  ((typep rhs 'trivial-variable-bindings:place-holder)
	   (setf (trivial-variable-bindings:bound-variable-value rhs bindings) lhs))
	  (t
	   (unless (trivial-utilities:equals lhs rhs)
	     (setf value nil))))
    
    (make-instance 'trivial-continuation:continuation-result
		   :operation :combine
		   :result value
		   :continuation nil)))

(create-filter-method :ge
  ;; :ge expects 3 agruments:
  ;;  1. value_1
  ;;  2. value_2
  ;;  3. result, a boolean
  ;; Each one can be a placeholder, but not all three simultameously.
  (declare (ignore backtracker))
  (let ((lhs (if (typep (first args) 'trivial-variable-bindings:place-holder) (trivial-variable-bindings:bound-variable-value (first args) bindings) (first args)))
	(rhs (if (typep (second args) 'trivial-variable-bindings:place-holder) (trivial-variable-bindings:bound-variable-value (second args) bindings) (second args)))
	(value bindings))
    
    (cond ((or (typep lhs 'trivial-variable-bindings:place-holder)
	       (typep rhs 'trivial-variable-bindings:place-holder))
	   (setf value nil)
	   (log:error "Cannot handle generic case of :le with '~a'" args))
	  ((typep (third args) 'trivial-variable-bindings:place-holder)
	   (setf (trivial-variable-bindings:bound-variable-value (third args) bindings) (>= lhs rhs)))
	  (t
	   (unless (eq (>= lhs rhs) (third args))
	     (setf value nil))))
    
    (make-instance 'trivial-continuation:continuation-result
		   :operation :combine
		   :result value
		   :continuation nil)))

(create-filter-method :gt
  ;; :gt expects 3 agruments:
  ;;  1. value_1
  ;;  2. value_2
  ;;  3. result, a boolean
  ;; Each one can be a placeholder, but not all three simultameously.
  (declare (ignore backtracker))
  (let ((lhs (if (typep (first args) 'trivial-variable-bindings:place-holder) (trivial-variable-bindings:bound-variable-value (first args) bindings) (first args)))
	(rhs (if (typep (second args) 'trivial-variable-bindings:place-holder) (trivial-variable-bindings:bound-variable-value (second args) bindings) (second args)))
	(value bindings))
    
    (cond ((or (typep lhs 'trivial-variable-bindings:place-holder)
	       (typep rhs 'trivial-variable-bindings:place-holder))
	   (setf value nil)
	   (log:error "Cannot handle generic case of :le with '~a'" args))
	  ((typep (third args) 'trivial-variable-bindings:place-holder)
	   (setf (trivial-variable-bindings:bound-variable-value (third args) bindings) (> lhs rhs)))
	  (t
	   (unless (eq (> lhs rhs) (third args))
	     (setf value nil))))
    
    (make-instance 'trivial-continuation:continuation-result
		   :operation :combine
		   :result value
		   :continuation nil)))

(create-filter-method :function
  (declare (ignore bindings backtracker))
  (log:warn "Could not apply to following filter: :function with '~a'" args)
  (make-instance 'trivial-continuation:continuation-result
		   :operation :combine
		   :result nil
		   :continuation nil))
