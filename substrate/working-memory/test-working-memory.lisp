;;;; Copyright (C) 2019 Eric Diethelm

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(in-package :working-memory)

(5am:def-suite :working-memory-tests :description "Marvin Working Memory tests" :in :marvin-tests)
(5am:in-suite :working-memory-tests)


(5am:test peprare-seed
  (marvin:with-clean-marvin
      (let ((x (make-instance 'ding:ding :id 0)))
	(setf (associations x) '((a . 1.5f0) (b . 0.9f0) (c . 1.2f0) (d . 5.1f0)))

	(5am:is (equal (prepare-seed x) '((2 D) (2 A) (2 C))))
	(5am:is (equal (prepare-seed x :to-ignore '(a)) '((2 D) (2 C)))))))

(5am:test find-object
  (marvin:with-clean-marvin
    ;; Test filter methods
    (let ((ding (find-ding '(:eigenschaft :?_ :graphie "Wort"))))
      (5am:is (eq (ding:id ding) 18)))))

