# Marvin Working Memory Manual

###### \[in package MARVIN-WORKING-MEMORY\]
[![pipeline status](https://gitlab.com/marvin-aiot/marvin/badges/master/pipeline.svg)](https://gitlab.com/marvin-aiot/marvin/commits/master)

## Description

This library is part of the [Marvin AIoT](https://gitlab.com/marvin-aiot/marvin/ "Marvin AIoT"). For more information please visit the aforementioned project. 

## Installing working-memory



## Working Example



## Exported Symbols

- [function] INITIALIZE-WORKING-MEMORY 

    Initializiert alle für den ArbeitsSpeicher notwendigen Elemente.

- [function] SHUTDOWN-WORKING-MEMORY 



- [function] ADD-OBJECT OBJECT

    Fügt das durch 'object' übergebene Objekt (einem Ding) dem ArbeitsSpeicher hinzu.

- [function] CREATE-OBJECT EIGENSCHAFTEN

- [function] FIND-OBJECT-BY-ID ID

- [function] GET-ALL-OBJECTS-IN-MEMORY 

- [function] GET-OBJECTS-OF-CLASS CLASS &KEY (IN-MEMORY-ONLY T)

- [function] FIND-OBJECT SEARCH-EXPRESSION &KEY (BLACKLIST NIL) (IGNORE-INHERITANCE NIL) (SEED NIL)

    Searches for memory objects for which the given search expression applies.
    Parameters:
       search-expression
       blacklist - A list of objects that must not be considered as valid result
       ignore-inheritance - 

## License Information

This library is released under the GNU General Public License. Please refer to the [LICENSE](https://gitlab.com/marvin-aiot/marvin/blob/master/LICENSE "License") to get the full licensing text.

## Contributing to this project

Please refer to the [CONTRIBUTING](https://gitlab.com/marvin-aiot/marvin/blob/master/CONTRIBUTING "Contributing") document for more information.
