;;;; Copyright (C) 2019 Eric Diethelm

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(in-package :working-memory)

(defsection @working-memory-manual (:title "Marvin Working Memory Manual")
  "[![pipeline status](https://gitlab.com/marvin-aiot/marvin/badges/master/pipeline.svg)](https://gitlab.com/marvin-aiot/marvin/commits/master)"
  (@working-memory-description section)
  (@working-memory-installing section)
  (@working-memory-example section)
  (@working-memory-exported section)
  (@working-memory-license section)
  (@working-memory-contributing section))


(defsection @working-memory-description (:title "Description")
  "This library is part of the [Marvin AIoT](https://gitlab.com/marvin-aiot/marvin 'Marvin AIoT'). For more information please visit the aforementioned project.")

(defsection @working-memory-installing (:title "Installing working-memory")
  "This project is not available via [QuickLisp](https://www.quicklisp.org/beta/ \"QuickLisp\") of download, but it still be loaded if the sources are copyed to your local-projects folder:

```bash
cd $HOME/quicklisp/local-projects
git clone https://gitlab.com/marvin-aiot/marvin.git
```

After the files are copied, we can use [QuickLisp](https://www.quicklisp.org/beta/ \"QuickLisp\") to load working-memory

```lisp
(ql:quickload :working-memory)
```
")

(defsection @working-memory-example (:title "Working Example")
  "")

(defsection @working-memory-exported (:title "Exported Symbols")
  (initialize-working-memory function)
  (shutdown-working-memory function)
  (add-object function)
  (create-object function)
  (find-object-by-id function)
  (find-object function)
  (find-by-params function)
  (find-objects-from-class function)
  (get-all-objects-in-memory function)
  (get-objects-of-class function))

(defsection @working-memory-license (:title "License Information")
  "This library is released under GPLv3. Please refer to the [LICENSE](https://gitlab.com/marvin-aiot/marvin/blob/master/LICENSE 'License') to get the full licensing text.")

(defsection @working-memory-contributing (:title "Contributing to this project")
  "Please refer to the [CONTRIBUTING](https://gitlab.com/marvin-aiot/marvin/blob/master/CONTRIBUTING.md 'Contributing') document for more information.")
