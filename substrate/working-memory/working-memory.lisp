;;;; Copyright (C) 2019 Eric Diethelm

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(in-package :working-memory)

(eval-when (:compile-toplevel :load-toplevel :execute) 
  (proclaim '(ftype (function () null) initialize-working-memory))
  (proclaim '(type single-float *activation-decay* *activation-threshold* *removal-threshold*))

  (proclaim '(ftype (function (ding:ding) (values)) propagate-activation))
  (proclaim '(ftype (function (fixnum) (or null ding:ding)) find-object-by-id))
  (proclaim '(ftype (function (fixnum t list cons) null) replace-by-reference))
  (proclaim '(ftype (function (fixnum) null) replace-all-by-reference))
  (proclaim '(ftype (function (ding:ding) null) save-to-langzeitspeicher))
  (proclaim '(ftype (function (ding:ding) null) release-object))
  (proclaim '(ftype (function () null) process-memory-release))

  (proclaim '(ftype (function (&key (:generic-place-holder t) (:bindings t) (:search-params list) (:blacklist list)
			       (:visited list)(:depth fixnum) (:depth-limit fixnum) (:ignore-inheritance boolean)
			       (:error boolean))
		     (values trivial-continuation:continuation-result &optional))
	      continue-using-last-created-ding-as-seed))
  
  (proclaim '(ftype (function (list &key (:default-placeholder t) (:ignore-inheritance boolean))
		     (values t &optional))
	      find-ding))
  
  (proclaim '(ftype (function (list &key (:ignore-inheritance boolean))
		     (values trivial-continuation:continuation-result &optional))
	      find-all-dings)))
  
;; @REVIEW I expect not to have more than a few hunderd elements in this container.
;;         Maybe a list would in this case have better performance.
;;         Note: Most of the uses are to loop over the full container anyhow.
(defvar *working-memory* nil)

(defvar *aktivierungsspeicher* nil)
(defvar *activation-decay* 1.0f0) ;; Activation decay defines how much of an elements activation in *aktivierungsspeicher* is reduced
                                  ;; in a unit of time (tbd). This may increase with more elements in the ArbeitsSpeicher.
(defvar *activation-threshold* 29.0f0) ;; Activation threshold (and the release threshold) depend on the number of
                                       ;; elements in the ArbeitsSpeicher.
(defvar *removal-threshold* 9.0f0)
(defvar *memory-release-thread-id* nil)
(defparameter +default-ding-lifetime+ 120) ;; seconds

#+non-persistent-working-memory
(defvar *latest-ding-id* 0) ;; @REVIEW This could be used always, but initialized with longterm-memory:get-next-ding-id
                            ;;         It would reduce the communication with the database server
                            ;;         But would require some logic too guarantee synchronization

(defvar *subscriptions* nil)

(defmethod ding:eigenschaft ((object ding:ding-reference) property &key ignore-inheritance)
  (return-from ding:eigenschaft
    (the (values t boolean &optional)
	 (case property
	   (:id (values (id object) t))
	   (t (ding:eigenschaft (find-object-by-id (ding:id object)) property :ignore-inheritance ignore-inheritance))))))

(defmethod (setf ding:eigenschaft) (value (object ding:ding-reference) property)
  (case property
    (:id (error "Trying to change the ID of a Ding (~a)." (ding:id object)))
    (t
     (setf (ding:eigenschaft (find-object-by-id (ding:id object)) property) value))))

(defmethod (setf ding:eigenschaft) :around (value (object ding:ding) property)
  (let ((diff nil)
	(result nil))
    (when (and (ding:property-serializable object property) (not (ding:surpress-change-notification)))
      (setf diff (remove-if #'(lambda (x) (eq (trivial-difference:operation x) :keep))
			    (trivial-difference:determine-difference value (eigenschaft object property)))))

    (setf result (call-next-method))

    (when diff
      (cognitron-base:with-learning-inhibited
	(notify-all-clients
	 (create-object (list :klasse (make-instance 'ding-reference :id 2)    ;; @REVIEW Id 2 refers to 'Frame', but this should not be hardcoded
			     :p-list (list :action :property-changed
					   :ding object
					   :property property
					   :difference-operations (mapcar #'(lambda (x)
									      (list
									       (trivial-difference:operation x)
									       :new (trivial-difference:new-value x)
									       :old (trivial-difference:old-value x)))
									  diff)))))))
    result))

(defun propagate-activation (object &aux (ding-to-activate nil))
  (declare (type ding:Ding object)
	   (type list ding-to-activate))

  ;; @REVIEW Hebbian Learning disabled.
  ;;(hebbian-learning:hebbian-learning object (get-all-objects-in-memory))
  
  (dolist (association (ding:eigenschaft object :associations))
    ;; Objects could be removed from *aktivierungsspeicher* between this and the next gethash, resulting in an error.
    (trivial-object-lock:with-object-lock-held (*aktivierungsspeicher* :test #'eq)
      (multiple-value-bind (value win) (gethash (ding:id (first association)) *aktivierungsspeicher*)
	(declare (type boolean win))
	;; @TODO Should we limit the activation of a Ding?
	(when (> 
	       (if (and win value)
		   (incf (the single-float (gethash (ding:id (first association)) *aktivierungsspeicher*)) (the single-float (second association)))
		   (setf (gethash (ding:id (first association)) *aktivierungsspeicher*) (the single-float (second association))))
	       (the single-float *activation-threshold*))

	  (push (first association) ding-to-activate)))))

  (dolist (ding ding-to-activate)
    (declare (type ding:ding-base ding))
    (log:trace "Ding ~a (:id ~a) was activated." ding (ding:id ding))
    (unless (find-object-by-id (ding:id ding))
      (log:error "No Ding with id ~a found!" (ding:id ding))))
  (values))

(defun replace-by-reference (object-id head tail parent)
  (declare (type fixnum object-id)
	   (type list tail)
	   (type cons parent))
  
  (cond ((null head)
	 (return-from replace-by-reference))

	((and
	  (atom head)
	  (typep head 'ding:ding)
	  (eq (ding:id head) object-id))
	 (nsubstitute (make-instance 'ding:ding-reference :id object-id) head parent))
	
	((listp head)
	 (let ((rec-head (car head))
	       (rest (cdr head)))
	   
	   (when (listp head)
	     (replace-by-reference object-id rec-head rest head)))))

  (replace-by-reference object-id (car tail) (cdr tail) parent))

(defun replace-all-by-reference (object-id)
  (declare (type fixnum object-id))
  
  (loop for value being the hash-values of *working-memory*
     do (loop for slot of-type c2mop:slot-definition  in (trivial-utilities:all-slots (class-of value))
	   when (slot-boundp value (c2mop:slot-definition-name slot))
	   do (let ((svalue (slot-value value (c2mop:slot-definition-name slot))))
		(typecase svalue
		  (cons
		   (replace-by-reference object-id (car svalue) (cdr svalue) svalue))
		  (ding:Ding
		   (when (eq (ding:id svalue) object-id)
		     (setf (slot-value value (c2mop:slot-definition-name slot))
			   (make-instance 'ding:ding-reference :id object-id)))))))))

(defun save-to-langzeitspeicher (object)
  (declare (type ding:ding object))
  (when (> (ding:dirty object) 0)
    (trivial-object-lock:with-object-lock-held (object :test #'trivial-utilities:equals)
      (longterm-memory:save-object object)
      (setf (ding:newly-created object) nil)
      (setf (ding:dirty object) 0)))
  (return-from save-to-langzeitspeicher))

(defun release-object (obj)
  (declare (type ding:ding obj))

  #+non-persistent-working-memory
  (declare (ignore obj))

  #-non-persistent-working-memory
  (progn
    (save-to-langzeitspeicher obj)

    (trivial-object-lock:with-object-lock-held (*working-memory* :test #'eq)
      (replace-all-by-reference (ding:id obj))
      (remhash (ding:id obj) *working-memory*))

    (trivial-object-lock:with-object-lock-held (*aktivierungsspeicher* :test #'eq)
      (remhash (ding:id obj) *aktivierungsspeicher*))

    (log:debug "The Ding (:id ~a) was released." (ding:id obj)))
  (return-from release-object))

(defun process-memory-release ()
  (loop
     for ding-id being the hash-keys in *aktivierungsspeicher*
     using (hash-value value)
     with num-saved = 0
     do (trivial-object-lock:with-object-lock-held (*working-memory* :test #'eq)
	  (let ((ding (gethash ding-id *working-memory*)))
	    (declare (type fixnum num-saved)
		     (type single-float value))

	    ;; Save and re-index in memory
	    (when (and ding
		       (or (ding:newly-created ding)
			   (and (< num-saved 10) (> (ding:dirty ding) 0))))
	      (log:trace "Saving (:id ~a) - ~a" (ding:id ding) num-saved)
	      (save-to-langzeitspeicher ding)
	      (incf num-saved))

	    (unless (and ding
			 (ding:persistent ding))
	      (let ((decayed-value (max 0.0f0 (- value (the single-float *activation-decay*)))))
		(declare (type single-float decayed-value))
		(if (< decayed-value (the single-float *removal-threshold*))
		    (progn
		      (when ding
			(release-object ding))
		      (trivial-object-lock:with-object-lock-held (*aktivierungsspeicher* :test #'eq)
			(remhash ding-id *aktivierungsspeicher*)))
		    (trivial-object-lock:with-object-lock-held (*aktivierungsspeicher* :test #'eq)
		      (setf (gethash ding-id *aktivierungsspeicher*) decayed-value)))))))))

(defun prepare-seed (ding &key to-ignore seed (top 20) (threshold 0.7f0) (weight 1))
  (unless ding
    (return-from prepare-seed seed))

  (delete-duplicates ;; We realy want to remove duplicates after sorting, so that we remove the ones with lower rating
   (sort
    (the list
	 (append
	  (mapcar #'(lambda (x) (list (if (typep x 'ding:Ding) (* weight 10) weight) x))
		  (delete-duplicates
		   (mapcar #'car
			   (let ((assocs (sort
					  (remove-if #'(lambda (x) (or (< (cadr x) threshold)
								       (member (car x) to-ignore :test #'trivial-utilities:equals)))
						     (ding:associations ding))
					  #'>
					  :key #'cadr)))
			     (subseq assocs 0 (min (length assocs) top))))))

	  ;; @TODO Extract all Dings from slots other than Associations, for now we just focus on P-LIST
	  (mapcar #'(lambda (x) (list (if (typep x 'ding:Ding) (* weight 10) weight) x))
		  (remove-if #'(lambda (x) (or (not (typep x 'ding:ding-base))
					       (member x to-ignore :test #'trivial-utilities:equals)))
			     (trivial-utilities:flatten (slot-value ding 'ding:p-list))))
	  
	  (remove-if #'(lambda (x) (member (cadr x) to-ignore :test #'trivial-utilities:equals)) seed)))
    #'> :key #'first)
   :key #'second :test #'trivial-utilities:equals))


(let ((next-ding-id-to-create 0)
      (last-ding-id-created 0))
  (declare (type (integer 0 *) next-ding-id-to-create last-ding-id-created))

  (defun initialise-ding-id ()
    (let ((max-id (trivial-pooled-database:execute-function :GET_LAST_CREATED_ID)))
      (setf next-ding-id-to-create (1+ max-id)
	    last-ding-id-created max-id)))

  (defun get-next-ding-id ()
    ""
    (the (values (integer 0 *) &optional)
	 (prog1 next-ding-id-to-create
	   (incf next-ding-id-to-create))))

  (defun update-last-created-id (id)
    (when (> id last-ding-id-created)
      (setf last-ding-id-created id)))

  (defun get-last-created-id ()
    ""
    (the (values (integer 0 *) &optional) last-ding-id-created)))

(defun initialize-working-memory ()
  "Initializiert alle für den ArbeitsSpeicher notwendigen Elemente."
  (setf *working-memory*
	#+sbcl
	(make-hash-table :synchronized t)
	#+ccl
	(make-hash-table :shared t))
  (setf *aktivierungsspeicher*
	#+sbcl
	(make-hash-table :synchronized t)
	#+ccl
	(make-hash-table :shared t))

  ;; When working in non-persistent mode we need to fill the in-memory database with some objects.
  #+non-persistent-working-memory
  (fill-memory)

  #-non-persistent-working-memory
  (initialise-ding-id)

  (unless (find-object-by-id 0)
    (log:warn "Could not load Ding.")
    (warn "Could not load Ding."))

  (unless (find-object-by-id 1)
    (log:warn "Could not load myself.")
    (warn "Could not load myself."))

  (setf *memory-release-thread-id*
	(trivial-monitored-thread:make-monitored-thread (:name "Working-Memory-Release-Thread" :sleep-duration 5 :max-restarts 5)
							#-non-persistent-working-memory (process-memory-release)
							#+non-persistent-working-memory #'identity))

  #+non-persistent-working-memory
  (log:warn "ATENTION: Working-Memory is running in non-persistent mode!")

  (end-point:add-http-end-point :ding
			     "Get the properties of a Ding identified by ID."
			     :v1
			     :get
			     #'get-ding-endpoint
			     :parents '(:working-memory))

  (end-point:add-ws-end-point :ding
			   "Get notifications about changes on a Ding. At first the whole Ding is sent."
			   :v1
			   :get
			   #'subscribe-ding-endpoint
			   :parents '(:working-memory)
			   :on-disconnect #'client-disconnected)

  (log:info "Working Memory initializied.")
  (return-from initialize-working-memory))

(defun shutdown-working-memory ()
  ""
  (end-point:remove-http-end-point :ding
				   :v1
				   :get
				   '(:working-memory))

  (end-point:remove-ws-end-point :ding
				 :v1
				 :get
				 '(:working-memory))

  (when *memory-release-thread-id*
    (trivial-monitored-thread:stop-thread *memory-release-thread-id*)
    (trivial-monitored-thread:join-thread *memory-release-thread-id*)
    (setf *memory-release-thread-id* nil))

  ;; @TODO Force write of all Dings in memory

  (setf *working-memory* nil)
  (setf *aktivierungsspeicher* nil))

(defun add-object (object)
  "Fügt das durch 'object' übergebene Objekt (einem Ding) dem ArbeitsSpeicher hinzu."
  (declare (type Ding object)
	   (type (unsigned-byte 62) +default-ding-lifetime+))

  (unless (and (slot-boundp object 'id)
	       (id object))
    (log:warn "Can not add a Ding without an id.")
    (return-from add-object object))
  
  (setf (activation-time object) (get-universal-time))

  (log:debug "Creating object Id ~a" (id object))
  (unless (and
	   (slot-boundp object 'ablaufdatum)
	   (ablaufdatum object)
	   (<= (the (unsigned-byte 62) (get-universal-time))
	       (the (unsigned-byte 62) (ablaufdatum object))))
    (setf (ablaufdatum object)
	  (the (unsigned-byte 62) (+ (the (unsigned-byte 62) (get-universal-time))
				     +default-ding-lifetime+))))

  (let ((dbg (loop for value being the hash-values of *working-memory*
		when (eq (id value) (id object))
		collect value)))
    (unless (null dbg)
      (break "Same :id already in working memory!")))
  
  (prog1
      (trivial-object-lock:with-object-lock-held (*working-memory* :test #'eq)
	(the Ding (setf (gethash (ding:id object) *working-memory*)
			object)))
    
    ;; *aktivierungsspeicher* hält die Aktivierungswerte auch für (noch) nicht aktive Dinger, während
    ;; *aktivierungs-heap* nur aktive Dinger hält.
    (trivial-object-lock:with-object-lock-held (*aktivierungsspeicher* :test #'eq)
      (setf (gethash (ding:id object) *aktivierungsspeicher*) *activation-threshold*))
    
    (update-last-created-id (ding:id object))

    ;; @TODO Could use a prioritized queue (cl-heap) based on some 'time to live' value

    (unless (ding:newly-created object)
      (propagate-activation object))

    ;; @REVIEW Deactivated Hebbian Learning for the time being. Learning must be disabled at startup.
    ;;(Marvin.Cognitron:hebbian-learning object)
    
    (plugin-handler:notify-plugins object)))

(defun create-object (eigenschaften)
  (declare (type list eigenschaften))

  (let ((dings (deserialize eigenschaften)))
    (unless (and (slot-boundp dings 'klasse)
		 (klasse dings))
      (setf (klasse dings) (make-instance 'ding-reference :id 0)))
    
    (unless (and (slot-boundp dings 'erstellungsdatum)
		 (erstellungsdatum dings))
      (setf (erstellungsdatum dings) (get-universal-time)))

    ;; @REVIEW It seem to happen, that longterm-memory:get-last-created-id is called (from a context of search)
    ;;         before working-memory:add-object had time to run. As such a missing ID error occurs.
    (trivial-object-lock:with-object-lock-held (*working-memory* :test #'eq)

      ;; @TODO Check if :id is already in use.
      (unless (slot-boundp dings 'id)
	(setf (id dings)
	      #+non-persistent-working-memory
	      (incf *latest-ding-id*)
	      #-non-persistent-working-memory
	      (get-next-ding-id))
	(setf (newly-created dings) t)
	(log:debug "New object ~a now has id ~a" dings (id dings)))

      (setf (dirty dings) 3) ;; We must not use (setf eigenschaften) because it triggers a property-changed Frame
      (add-object dings))
    
    (return-from create-object (the Ding dings))))

(defun find-object-by-id (id)
  (declare (type fixnum id))

  (trivial-object-lock:with-object-lock-held (*working-memory* :test #'eq)
    (let ((ding
	   (loop
	      for obj of-type ding:ding-base being the hash-values of *working-memory*
	      when (eq (ding:id obj) id)
	      do (return obj))))

      #-non-persistent-working-memory
      (unless ding
	(handler-case
	    (setf ding (add-object (longterm-memory:get-specific-id id)))
	  (unregistered-class (c) (log:error "Could not load (~a) from memory because: ~a" id c))
	  (error (c) (log:error "Error from Longterm Memory: ~a" c))))
      
      (return-from find-object-by-id
	(the (values (or null Ding) &optional) (values ding))))))

(defun get-all-objects-in-memory ()
  (loop for value being the hash-values of *working-memory*
     collect value))

(defun get-objects-of-class (class &key (in-memory-only t))
  (declare (type (or Ding Ding-reference) class))
  (let ((loaded (loop for value being the hash-values of *working-memory*
		   when (trivial-utilities:equals (klasse value) class)
		   collect value)))
    (when in-memory-only
      (return-from get-objects-of-class loaded))

    #+non-persistent-working-memory
    (return-from get-objects-of-class loaded)
    #-non-persistent-working-memory
    (append loaded (mapcar #'add-object (remove-if #'(lambda (x) (member (ding:id x) loaded :key #'ding:id)) (longterm-memory:load-objects-by-class class))))))

(defun find-all-dings (filter &key (ignore-inheritance t))
  (declare (type list filter)
	   (type boolean ignore-inheritance))
  (the (values trivial-continuation:continuation-result &optional)
       (trivial-filter:query filter
			     :ignore-inheritance ignore-inheritance)))

(defun find-ding (filter &key (default-placeholder :?_) (ignore-inheritance t))
  (declare (type list filter)
	   (type boolean ignore-inheritance))
  (labels ((contains-generic-placeholder (l)
	     (cond ((null l)
		    nil)
		   ((consp (car l))
		    (or (contains-generic-placeholder (car l))
			(contains-generic-placeholder (cdr l))))
		   ((eq (car l) default-placeholder)
		    t)
		   (t
		    (contains-generic-placeholder (cdr l))))))

    (unless (contains-generic-placeholder filter)
      (error "Search expression did not contain the :~a placeholder. Expression: '~a'" default-placeholder filter)))

  (let ((result (find-all-dings filter
				:ignore-inheritance ignore-inheritance)))
    (declare (type trivial-continuation:continuation-result result))
    (trivial-utilities:aif (and (trivial-continuation:result result)
				(trivial-variable-bindings:bound-variable-value
				 (make-instance 'trivial-variable-bindings:place-holder
						:name (subseq (symbol-name default-placeholder) 1))
				 (trivial-continuation:result result)))
			     (return-from find-ding it)
			     (return-from find-ding nil))))

(define-condition query-failure/invalid-object-property (trivial-filter:query-failure)
  ()
  (:report (lambda (condition stream)
	     (format stream "The given object does not have the property in '~A' with the given bindings '~A'."
		     (trivial-filter:proposition condition)
		     (trivial-filter:bindings condition)))))

(trivial-continuation:cc/define search-eigenschaft
    (prop value &key bindings seed visited depth depth-limit ignore-inheritance error)
  (declare (type list seed visited)
	   (type fixnum depth depth-limit)
	   (type boolean ignore-inheritance error))

  (let ((head (cadar seed)))
    (when (>= depth depth-limit)
      (log:warn "Giving up search for '~a : ~a' after ~a nodes." prop value depth)
      (trivial-continuation:cc/terminate))

    (unless head
      #+non-persistent-working-memory
      (trivial-continuation:cc/terminate)
      #-non-persistent-working-memory
      (let ((new-head (let ((latest-id (get-last-created-id)))
			(declare (type (or null fixnum) latest-id))
			(loop for i of-type fixnum from 0 to 15
			   while (> (- latest-id i) 0)
			   unless (member (the fixnum (- latest-id i)) visited :key #'ding:id)
			   do (trivial-utilities:awhen (find-object-by-id (the fixnum (- latest-id i)))
				(return it))))))
	(if (null new-head)
	    (trivial-continuation:cc/terminate)
	    (trivial-continuation:cc/return
	     (search-eigenschaft prop value
				   :bindings bindings
				   :seed (list (list 0 new-head))
				   :ignore-inheritance ignore-inheritance
				   :visited visited
				   :depth depth
				   :depth-limit depth-limit
				   :error error)))))

    (setf seed (cdr seed))

    (unless (member head visited :test #'trivial-utilities:equals)
      (let ((head-loaded nil))
	(when (typep head 'ding:Ding-Reference)
	  (trivial-object-lock:with-object-lock-held (*working-memory* :test #'eq)
	    (loop
	       for obj of-type ding:ding-base being the hash-values of *working-memory*
	       when (eq (ding:id obj) (ding:id head))
	       do
		 (progn
		   (setf head obj)
		   (setf head-loaded t)
		   (return))))

	  (unless head-loaded
	    #+non-persistent-working-memory
	    (error "The ding ~a is only a Ding-Reference." (ding:id head))
	    #-non-persistent-working-memory
	    (handler-case
		(setf head (longterm-memory:get-specific-id (ding:id head)))
	      (unregistered-class (c) (log:error "Could not load (~a) from memory because: ~a" (ding:id head) c))
	      (error (c) (log:error "Error from Longterm Memory: ~a" c)))))

	(when head
	  (push head visited)

	  (when (trivial-utilities:equals (ding:eigenschaft head prop :ignore-inheritance ignore-inheritance) value)
	    (unless head-loaded
	      (setf head (find-object-by-id (ding:id head))))

	    (setf seed (prepare-seed head :to-ignore visited :seed seed :weight 100))
	    (setf (gethash (ding:id head) *aktivierungsspeicher*) *activation-threshold*)

	    (trivial-continuation:cc/create-return
	     head
	     #'(lambda () (search-eigenschaft prop value
				      :bindings bindings
				      :seed seed
				      :ignore-inheritance ignore-inheritance
				      :visited visited
				      :depth 0
				      :depth-limit depth-limit
				      :error error)))))))

    (trivial-continuation:cc/return (search-eigenschaft prop value
							:bindings bindings
							:seed (prepare-seed head :to-ignore visited :seed seed :weight 1)
							:ignore-inheritance ignore-inheritance
							:visited visited
							:depth (1+ depth)
							:depth-limit depth-limit
							:error error))))

(trivial-filter:create-filter-method :eigenschaft
  ;; :eigenschaft expects 3 agruments:
  ;;  1. object
  ;;  2. property
  ;;  3. value
  ;; Each one can be a placeholder, but not all three simultameously.
  (trivial-utilities:extract-additional-keys (((ignore-inheritance t))
					      keys)
    ;; Overwrite any globally defined :ignore-inheritance with one defined locally in the search parameter
    (trivial-utilities:extract-additional-keys (((ignore-inheritance ignore-inheritance))
						(fourth args))

	(let ((obj (trivial-filter:extract-value (first args) bindings))
	      (prop (trivial-filter:extract-value (second args) bindings))
	      (value (trivial-filter:extract-value (third args) bindings))
	      (result bindings))

 	  (cond ((and (typep obj 'trivial-variable-bindings:place-holder)
		      (not (typep prop 'trivial-variable-bindings:place-holder))
		      (not (typep value 'trivial-variable-bindings:place-holder)))
		 
		 (if (eq :id prop)
		     (progn
		       (setf result (trivial-utilities:clone bindings))
		       (setf (trivial-variable-bindings:bound-variable-value obj result)
			     (find-object-by-id value)))
		     (let ((seed nil))
		       (when (eq :klasse prop)
			 (setf seed (mapcar #'(lambda (x) (list 250 x))
					    (get-objects-of-class value :in-memory-only nil))))  ;; @TODO This could load a limited number of objects and have a continuation to load more

		       (setf result (trivial-utilities:clone bindings))
		       (let ((ding (with-eigenschaft-cache
				     (search-eigenschaft prop value
							 :bindings result
							 :seed (if seed
								   seed
								   (loop for value being the hash-values of *working-memory*
								      collect (list 10 value)))
							 :ignore-inheritance ignore-inheritance
							 :depth 0
							 :depth-limit 250
							 :error nil))))
			 (if (trivial-continuation:result ding)
			     (progn
			       (setf (trivial-variable-bindings:bound-variable-value obj result) (trivial-continuation:result ding))
			       (trivial-continuation:cc/create-return result
								      (let ((original-bindings (trivial-utilities:clone result))
									    (original-ding (trivial-utilities:clone ding)))
									(trivial-continuation:cc/define-lambda ()
									  (unless (trivial-continuation:continuation original-ding)
									    (trivial-continuation:cc/terminate))

									  (trivial-continuation:cc/continue original-ding)
									  (if (trivial-continuation:result original-ding)
									      (let ((new-bindings (trivial-utilities:clone original-bindings)))
										(setf (trivial-variable-bindings:bound-variable-value obj new-bindings)
										      (trivial-continuation:result original-ding))
										(trivial-continuation:cc/create-return new-bindings
														       #'self))
									      (trivial-continuation:cc/terminate))))))
			     (trivial-continuation:cc/terminate))))))
		((and (not (typep obj 'trivial-variable-bindings:place-holder))
		      (not (typep prop 'trivial-variable-bindings:place-holder))
		      (typep value 'trivial-variable-bindings:place-holder))
                 (if (or (slot-exists-p obj prop)
                         (member prop (slot-value obj 'p-list)))
                     (progn
		       (setf result (trivial-utilities:clone bindings))
		       (setf (trivial-variable-bindings:bound-variable-value value result)
		             (ding:eigenschaft obj prop :ignore-inheritance ignore-inheritance)))
                     ;; If prop is not a valid property of obj an exception must be raised
                     (progn
                       (signal 'query-failure/invalid-object-property
                               :proposition (list :eigenschaft args)
	                       :bindings (trivial-utilities:clone bindings)
	                       :continuation backtracker))))
		((and (not (typep obj 'trivial-variable-bindings:place-holder))
		      (not (typep prop 'trivial-variable-bindings:place-holder))
		      (not (typep value 'trivial-variable-bindings:place-holder)))
		 (cond ((or (null obj)
			    (null prop))
			(setf result nil))
		       ((not (trivial-utilities:equals
			      value
			      (ding:eigenschaft obj prop :ignore-inheritance ignore-inheritance)))
			(setf result nil))))
		(t
		 (log:warn "Invalid call of (:eigenschaft ~a ~a ~a)" obj prop value)
		 (setf result nil)))

	  (make-instance 'trivial-continuation:continuation-result
			 :operation :terminate
			 :result result
			 :continuation nil)))))

(trivial-filter:create-filter-method :exist
  ;; :exist expects 3 agruments:
  ;;  1. object
  ;;  2. property
  ;;  3. result, a boolean (found/not found)
  ;; Each one can be a placeholder, but not all three simultameously.
  (declare (ignore backtracker))
  (trivial-utilities:extract-additional-keys (((global-ignore-inheritance nil))
					      keys)
    ;; Overwrite any globally defined :ignore-inheritance with one defined locally in the search parameter
    (trivial-utilities:extract-additional-keys (((ignore-inheritance global-ignore-inheritance))
						(fourth args))
      (let ((obj (if (typep (first args) 'trivial-variable-bindings:place-holder) (trivial-variable-bindings:bound-variable-value (first args) bindings) (first args)))
	    (prop (if (typep (second args) 'trivial-variable-bindings:place-holder) (trivial-variable-bindings:bound-variable-value (second args) bindings) (second args)))
	    (result (if (typep (third args) 'trivial-variable-bindings:place-holder) (trivial-variable-bindings:bound-variable-value (third args) bindings) (third args))))

	;; @TODO This is not finished yet
	(declare (ignore result))
	
	(make-instance 'trivial-continuation:continuation-result
		       :operation :terminate
		       :result (if (eigenschaft obj prop :ignore-inheritance ignore-inheritance)
				   bindings
				   nil)
		       :continuation nil)))))



(trivial-filter:set-filter-cost '(:eigenschaft :_ :_ :*) 40)
(trivial-filter:set-filter-cost '(:eigenschaft :? :id :_) 25)
(trivial-filter:set-filter-cost '(:eigenschaft :? :klasse :_) 50)
(trivial-filter:set-filter-cost '(:eigenschaft :? :* :*) 500)
(trivial-filter:set-filter-cost '(:eigenschaft :_ :_ :*) 20)
(trivial-filter:set-filter-cost '(:eigenschaft :_ :? :_) 100)
(trivial-filter:set-filter-cost '(:eigenschaft :_ :? :?) 1000)
(trivial-filter:set-filter-cost '(:eigenschaft :? :_ :_) 1000)
(trivial-filter:set-filter-cost '(:eigenschaft :* :* :*) 10000)

(trivial-filter:set-filter-cost '(:exist :_ :_ :_) 40)
(trivial-filter:set-filter-cost '(:exist :_ :? :_) 100)
(trivial-filter:set-filter-cost '(:exist :? :_ :_) 1000)
(trivial-filter:set-filter-cost '(:exist :? :? :_) 10000)

#+non-persistent-working-memory
(defun fill-memory ()
  (let (ding self frame kline pluginbase pluginresponse action wort wname
	wself wbeschreibung weinheit wding wframe wkline wpluginbase
	wpluginresponse waction wwort)

    (setf ding (add-object (make-instance 'ding:ding :id 0 :klasse nil :persistent t
					  :erstellungsdatum (get-universal-time))))

    (setf self (create-object (list :id 1 :klasse ding :p-list '(:device-id 1) :persistent t)))
    (setf wort (create-object (list :id 7 :klasse ding)))

    (setf wname (create-object (list :id 8 :klasse wort :p-list '(:graphie "Name"))))
    (setf wself (create-object (list :id 9 :klasse wort :p-list '(:graphie "Marvin"))))
    (setf wbeschreibung (create-object (list :id 10 :klasse wort :p-list '(:graphie "Beschreibung"))))
    (setf weinheit (create-object (list :id 11 :klasse wort :p-list '(:graphie "Einheit"))))
    (setf wding (create-object (list :id 12 :klasse wort :p-list '(:graphie "Ding"))))
    (setf wframe (create-object (list :id 13 :klasse wort :p-list '(:graphie "Frame"))))
    (setf wkline (create-object (list :id 14 :klasse wort :p-list '(:graphie "K-Line"))))
    (setf wpluginbase (create-object (list :id 15 :klasse wort :p-list '(:graphie "PluginBase"))))
    (setf wpluginresponse (create-object (list :id 16 :klasse wort :p-list '(:graphie "Plugin-Response"))))
    (setf waction (create-object (list :id 17 :klasse wort :p-list '(:graphie "Action"))))
    (setf wwort (create-object (list :id 18 :klasse wort :p-list '(:graphie "Wort"))))

    (setf frame (create-object (list :id 2 :klasse ding :p-list (list wname wframe))))
    (setf kline (create-object (list :id 3 :klasse ding :p-list (list wname wkline))))
    (setf pluginbase (create-object (list :id 4 :klasse ding :p-list (list wname wpluginbase))))
    (setf pluginresponse (create-object (list :id 5 :klasse ding :p-list (list wname wpluginresponse))))
    (setf action (create-object (list :id 6 :klasse ding :p-list (list wname waction))))

    (without-change-notification
      (setf (ding:eigenschaft wort wname) wwort)
      (setf (ding:eigenschaft ding wname) wding)
      (setf (ding:eigenschaft self wname) wself)

      (setf (ding:associations self) `((,frame 1.0f0)
				       (,kline 1.0f0)
				       (,pluginbase 1.0f0)
				       (,pluginresponse 1.0f0)
				       (,action 1.0f0)
				       (,wort 1.0f0)
				       (,wname 1.0f0)
				       (,wself 1.0f0)))

      (setf (ding:associations wself) `((,self 1.0f0)))
      (setf *latest-ding-id* 18))))


;; ------------------------------- BEGIN Handling HTTP & WS routes -------------------------------


(defclass client ()
  ((dings :initarg :dings
	  :accessor client-dings)
   (user :initarg :user
	 :reader user)))

(defclass subscription ()
  ((clients :initarg :clients
	    :accessor clients)
   (resource :initarg :resource
	     :reader resource)))

(defclass ding-subscription-json ()
  ((action :initarg :action :reader action :type string)
   (id :initarg :id :reader id :type fixnum)))

(c2mop:ensure-finalized (find-class 'ding-subscription-json))

(defun notify-all-clients (ding)
  (let ((subscription (car (member :ding *subscriptions* :key #'(lambda (x) (end-point:resource-name (resource x)))))))
    (unless subscription
      (return-from notify-all-clients))

    (trivial-utilities:awhen
	(iterate:iterate
	  (iterate:for client in (clients subscription))
	  (trivial-utilities:awhen (find (ding:id ding)
					 (client-dings client))
	    (iterate:collect (user client))))
      (let ((value-json (serialize-ding ding)))
	(iterate:iterate
	  (iterate:for recipient in it)
	  (hunchensocket:send-text-message recipient value-json))))))

(defun serialize-ding (ding)
  (flet ((make-format (obj)
	   (if (typep obj 'ding:ding-base)
	       (format nil "\"~a\"" (trivial-json-codec:serialize-json obj))
	       (trivial-json-codec:serialize-json obj))))
    (let ((properties
	   (append
	    (iterate:iterate
	      (iterate:for slot in (trivial-utilities:all-slots (class-of ding)))
	      (iterate:for slot-name = (c2mop:slot-definition-name slot))
	      (when (and (not (eq slot-name 'ding:p-list))
			 (slot-boundp ding slot-name))
		(iterate:appending (list slot-name (slot-value ding slot-name)))))
	    (ding:p-list ding))))
      (with-output-to-string (stream)
	(princ #\{ stream)
	(iterate:iterate
	  (iterate:for (prop val) on properties by #'cddr)
	  (unless (iterate:first-time-p)
	    (princ #\, stream))
	  (format stream "~a:~a"
		  (make-format (trivial-json-codec:serialize-json prop))
		  (make-format (trivial-json-codec:serialize-json val))))
	(princ #\} stream)))))

(defun get-ding-endpoint (resource request)
  (declare (ignore resource))
  ;; @TODO This code is unsafe! And returns no errors (no id, id not string, id no integer as string, no Ding found ...)
  (let ((ding (find-object-by-id (parse-integer (hunchentoot:get-parameter "id" request)))))
    (serialize-ding ding)))

(defun client-disconnected (resource user)
  (log:info "User disconnected: " user)
  (let ((subscription (car (member ::ding *subscriptions* :key #'(lambda (x) (end-point:resource-name (resource x)))))))
    (unless (eq resource (resource subscription))
      (break "The resources do not match!"))

    (trivial-object-lock:with-object-lock-held (*subscriptions* :test #'eq)
      (let ((subscription (car (member resource *subscriptions* :key #'resource))))
	(when (member user (clients subscription) :key #'user)
	  (setf (clients subscription) (delete user (clients subscription) :key #'user))
	  (unless (clients subscription)
	    (setf *subscriptions* (delete subscription *subscriptions*))))))))

(defun subscribe-ding-endpoint (resource user message)
  (let ((json (trivial-json-codec:deserialize-json message :class (find-class 'ding-subscription-json))))
    (unless (action json)
      (log:error "Parsing error '~a'." message)
      (return-from subscribe-ding-endpoint))

    (cond ((string-equal (action json) "subscribe")
	   (subscribe resource user json))
	  ((string-equal (action json) "unsubscribe")
	   (unsubscribe user json))
	  (t (log:error "Invalid action '~a'" (action json))))))

(defun subscribe (resource user json)
  (trivial-object-lock:with-object-lock-held (*subscriptions* :test #'eq)
    (let ((subscription (car (member :messages *subscriptions* :key #'(lambda (x) (end-point:resource-name (resource x))))))
	  (ding-id (id json)))
      (if subscription
	  (trivial-utilities:aif (car (member user (clients subscription) :key #'user))
				 (if (find ding-id (client-dings it))
				     (log:error "User ~a already subscribed to Ding ID ~a." user ding-id)
				     (push ding-id (client-dings it)))
				 (push (make-instance 'client
						      :user user
						      :dings (list ding-id))
				       (clients subscription)))

	  (push (make-instance 'subscription
			       :resource resource
			       :clients (list (make-instance 'client
							     :user user
							     :dings (list ding-id))))
		*subscriptions*))

      (hunchensocket:send-text-message user (serialize-ding (find-object-by-id ding-id))))))

(defun unsubscribe (user json)
  (trivial-object-lock:with-object-lock-held (*subscriptions* :test #'eq)
    (let ((subscription (car (member :messages *subscriptions* :key #'(lambda (x) (end-point:resource-name (resource x))))))
	  (ding-id (id json)))
      (if subscription
	  (trivial-utilities:aif (car (member user (clients subscription) :key #'user))
				 (unless (setf (client-dings it)
					       (delete ding-id (client-dings it)))
				   (unless (setf (clients subscription)
						 (delete user (clients subscription) :key #'user))
				     (setf *subscriptions*
					   (delete :messages *subscriptions* :key #'(lambda (x) (end-point:resource-name (resource x)))))))
				 (log:error "User ~a was not subscribed to Ding ID ~a." user ding-id))
	  (log:error "No Subscription for :ding was found.")))))

;; ------------------------------- END Handling HTTP & WS routes -------------------------------
