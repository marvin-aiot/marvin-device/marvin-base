
(uiop:define-package #:working-memory
  (:documentation "")
  (:use #:common-lisp
	#:ding)
  (:import-from :trivial-utilities #:equals #:clone)
  (:export #:initialize-working-memory
	   #:shutdown-working-memory
	   #:add-object
	   #:create-object
	   #:release-object
	   #:find-object-by-id
	   #:get-all-objects-in-memory
	   #:get-objects-of-class
	   #:find-by-params
	   #:find-objects-from-class
	   #:find-all-dings
           #:find-ding
           #:query-failure/invalid-object-property))

