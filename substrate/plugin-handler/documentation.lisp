;;;; Copyright (C) 2019 Eric Diethelm

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(in-package :plugin-handler)

(defsection @plugin-handler-manual (:title "Marvin Plugin Handler Manual")
  "[![pipeline status](https://gitlab.com/marvin-aiot/marvin/badges/master/pipeline.svg)](https://gitlab.com/marvin-aiot/marvin/commits/master)"
  (@plugin-handler-description section)
  (@plugin-handler-installing section)
  (@plugin-handler-license section)
  (@plugin-handler-contributing section))

(defsection @plugin-handler-description (:title "Description")
  "This library is part of the [Marvin AIoT](https://gitlab.com/marvin-aiot/marvin 'Marvin AIoT'). For more information please visit the aforementioned project.")

(defsection @plugin-handler-installing (:title "Installing plugin-handler")
  "This project is not available via [QuickLisp](https://www.quicklisp.org/beta/ \"QuickLisp\") of download, but it still be loaded if the sources are copyed to your local-projects folder:

```bash
cd $HOME/quicklisp/local-projects
git clone https://gitlab.com/marvin-aiot/marvin.git
```

After the files are copied, we can use [QuickLisp](https://www.quicklisp.org/beta/ \"QuickLisp\") to load plugin-handler

```lisp
(ql:quickload :plugin-handler)
```
")

(defsection @plugin-handler-license (:title "License Information")
  "This library is released under GPLv3. Please refer to the [LICENSE](https://gitlab.com/marvin-aiot/marvin/blob/master/LICENSE 'License') to get the full licensing text.")

(defsection @plugin-handler-contributing (:title "Contributing to this project")
  "Please refer to the [CONTRIBUTING](https://gitlab.com/marvin-aiot/marvin/blob/master/CONTRIBUTING 'Contributing') document for more information.")

