(uiop:define-package #:plugin-handler
  (:documentation "")
  (:use #:common-lisp)
  (:export #:register-plugin
	   #:notify-plugins
	   #:initialize-plugins
	   #:install-plugin
	   #:start-plugins
	   #:stop-plugins))

