;;;; Copyright (C) 2019 Eric Diethelm

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(in-package :plugin-handler)

(eval-when (:compile-toplevel :load-toplevel :execute) 
  (proclaim '(type list *registered-plugins*))

  (proclaim '(ftype (function (plugin-base:plugin) null) register-plugin))
  (proclaim '(ftype (function (plugin-base:plugin-response) single-float) calculate-plugin-response-fitness))
  (proclaim '(ftype (function (plugin-base:plugin ding:ding list) null) notify-plugin))
  (proclaim '(ftype (function (ding:Ding) null) notify-plugins))

  (proclaim '(inline process-continuation calculate-plugin-response-fitness notify-plugin)))


(defvar *inhibit-plugin-notification* nil)
(defvar *registered-plugins* nil)
(defvar *loaded-plugins* nil)
(defvar *new-frame-available* (bordeaux-threads:make-condition-variable :Name "new-frame-available"))

(defmacro with-inhibited-plugin-notification (&body body)
  `(let ((*inhibit-plugin-notification* t))
     ,@body))

(defun calculate-plugin-response-fitness (response)
  (declare (type plugin-base:plugin-response response))
  (the single-float (* (/ (the single-float 1.0) (the single-float (plugin-base:cost response))) (the single-float (plugin-base:fit response)))))

(defun notify-plugin (plugin ding args)
  (declare (type plugin-base:plugin plugin)
	   (type ding:ding ding))

  (unless *inhibit-plugin-notification*
    (chanl:send (plugin-base:plugin-queue plugin) (list :ding ding :args args) :blockp nil))
  nil)

(defun notify-plugins (ding)
  "Called by ArbeitsSpeicher every time a new Ding is created."
  (declare (type ding:ding ding))

  (when *inhibit-plugin-notification*
    (return-from notify-plugins))
  
  ;; Is ding a frame and (eigenschaft :k-lines) is not null?
  (when (and (typep ding 'frame:frame)
	     (frame:k-lines ding))
    (progn
  ;;;;;; For every linked K-Line
      (dolist (k-line (frame:k-lines ding))
	(declare (type k-line:k-line k-line))
	
  ;;;;;;; Is K-Line waiting for a response
	(trivial-utilities:aif (k-line:continuation-point k-line)
  ;;;;;;;; Process continuation
	    (funcall (the function it) ding)
  ;;;;;; Remove K-Line from ding
	    (k-line:remove-frame-from-kline ding k-line)))))

  ;; Call 'plugin-analyze-ding' on every registered plugin and collect responses
  ;; Sort the responses
  
  (let ((responses (sort
		    (the list
			 (with-inhibited-plugin-notification
			     (loop for plugin in *registered-plugins*
				collect (plugin-base:analyze plugin ding))))
		    #'<
		    :key #'calculate-plugin-response-fitness)))
    

  ;; @TODO Call suppressors to filter responses
    
  ;; @TODO Separate valid responses (:resolution :possible) from the invalid
    ;;(setf responses (partition #'(lambda (x) (eq (resolution x) :possible)) responses))
    (setf responses (remove-if #'(lambda (x) (eq (plugin-base:resolution x) :not-possible)) responses))
    
  ;; Are any results available?
    (if responses
  ;;; Yes!
	;; @REVIEW We want to reduce the load on the DB (Issue #59).
	;;(progn
	;;  (when (typep ding 'frame:Frame)
	;;    (Marvin.Univers:without-change-notification
  ;;;; Store all responses (valid and censored) in ding
	;; @REVIEW (Issue #59) This is the moment to create a __combined__ PluginResponse info
	;;      (setf (plugin-base:processing-responses ding) (cdr responses))
	;;      (setf (plugin-base:executing-response ding) (car responses))))
	  
  ;;;; Notify plugin with best result about this ding (passing also the :args of the result itself)
	  (notify-plugin (plugin-base:plugin (car responses)) ding (plugin-base:args (car responses)));;)
  ;;; No!
	(progn
  ;;;; Handle 'dead end' (How?)
	  (log:debug "No results for processing ~a (:id ~a) found." ding (ding:id ding)))))

  nil)


;; Response:
;; (:plugin Plugin :cost float :fit float :resolution {yes possibly no} :suppressed {yes no} :args list) 

(defun register-plugin (plugin)
  (declare (type plugin-base:plugin plugin))
  (push plugin *registered-plugins*)
  nil)

(defun initialize-plugins ()
  (load-all-plugins)
  (setf *registered-plugins* nil)
  (iterate:iterate
    (iterate:for plugin in *loaded-plugins*)
    (register-plugin (plugin-base:initialize-plugin plugin))))

(defun start-plugins ()
  (loop for plugin in *registered-plugins*
     do (handler-case (plugin-base:start-plugin plugin)
	  (condition (c) (let* ((find-object-fn (uiop:find-symbol* "FIND-DING" :marvin nil))
				(name (funcall find-object-fn `(:and (:eigenschaft :?name :graphie "Name")
								     (:eigenschaft ,plugin :?name :?plugin-name)
								     (:eigenschaft :?plugin-name :graphie :?_)))))
			   (log:error "An error occured while starting the plugin '~a'. Original error: ~a" name c))))))

(defun stop-plugins ()
  (loop for plugin in *registered-plugins*
     do (handler-case (plugin-base:stop-plugin plugin)
	  (condition (c) (let* ((find-object-fn (uiop:find-symbol* "FIND-DING" :marvin nil))
				(name (funcall find-object-fn `(:and (:eigenschaft :?name :graphie "Name")
								     (:eigenschaft ,plugin :?name :?plugin-name)
								     (:eigenschaft :?plugin-name :graphie :?_)))))
			   (log:error "An error occured while stopping the plugin '~a'. Original error: ~a" name c))))))

(defun load-plugin (plugin)
  (log:info "Loading plugin '~a'." plugin)
  (handler-case
      (handler-bind ((error
                      (lambda (c)
			(declare (ignorable c))
		        (let ((restart (find-restart 'continue)))
			  (when restart (invoke-restart restart))))))
	(ql:quickload plugin)
	(push plugin *loaded-plugins*)
	t)
    (error (c) (progn
		     (log:warn "Could not load plugin '~a'. Original error: ~a" plugin c)
		     nil))))

(defun load-all-plugins ()
  (setf *loaded-plugins* nil)
  (let ((plugins (piggyback-parameters:get-value :enabled-plugins
						 #+non-persistent-working-memory :file-only
						 #-non-persistent-working-memory :database-first)))
    (if plugins
	(when (some #'null (loop for plugin in plugins
			      collect (prog1
					  (load-plugin plugin)
					#+non-persistent-working-memory
					(plugin-base:install-plugin plugin *standard-input* *standard-output*))))
	  (log:error "At least one plugin could not be loaded. Please check the previous log entries for more details."))
	(log:warn "No plugins enabled. Are we missing something?")))
  (return-from load-all-plugins))

(defun install-plugin (plugin &key (input-stream *standard-input*) (output-stream *standard-output*) (force nil))
  "Install the plugin defined by *PLUGIN* calling its *first-time-initialization* function, adding it to the configuration parameter *:ENABLED-PLUGINS* and starting it.  
*plugin* -  
*input-stream* -  
*output-stream* -  
*force* - When non-nil forces complete re-installation, including downloading sources"

  (let ((system (getf plugin :project-defsys)))
    (log:info "Starting installation of '~a' (~a)." (getf plugin :project-name) system)

    ;; Check if we should reset the repository for this plugin
    (trivial-utilities:awhen (and force (asdf:find-system system nil))
      (trivial-gitlab-api:revert-project system nil (asdf:system-source-directory it) :is-project-path t))

    ;; Check the plugin is loadable -> it exists for ASDF
    (unless (asdf:find-system system nil)
      (trivial-gitlab-api:clone-project "marvin-aiot/marvin-device/plugins"
					(string system)
					""
					(uiop:pathname-parent-directory-pathname
					 (asdf:system-source-directory :marvin-base))))

    ;; Load the plugin
    (unless (load-plugin system)
      (log:error "Loading the plugin '~a' (~a) failed. Please check the previous log entries for more details." (getf plugin :project-name) system)
      (return-from install-plugin))

    ;; Check the plugin is not yet configured in DB
    (when (and (not force)
	       (or (member system (piggyback-parameters:get-value :installed-plugins :database-first))
		   (member system (piggyback-parameters:get-value :enabled-plugins :database-first))))
      (log:info "The plugin '~a' seems to be already installed. Not installing it again (use ':force t' to re-install it)" (getf plugin :project-name))
      (return-from install-plugin))

    ;; Call plugin-base:install-plugin on it
    (handler-case (progn
		    (plugin-base:install-plugin system input-stream output-stream)
		    (pushnew system (piggyback-parameters:get-value :installed-plugins :database-only))
		    (pushnew system (piggyback-parameters:get-value :enabled-plugins :database-only))
		    (return-from install-plugin t))
      (condition (c) (progn
		       (log:error "Could not install plugin '~a'. Original error: ~a" (getf plugin :project-name) c)
		       (return-from install-plugin nil)))))

  ;; To be executed later:
  ;; Load the plugin via load-plugin
  ;; Start plugin
  )
