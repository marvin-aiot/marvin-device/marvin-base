;;;; Copyright (C) 2019 Eric Diethelm

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.




#|

Change Request "Separate Plugin, Agent and Extension"

The code that is loaded and run is an Agent
The additional code, that is run during installation, is an Extension
The Archive containing an Agent and/or an Extension is a Plugin

class Extension
 - version
 - install function

class Agent
 - version
 - start
 - stop
 - process


Extensions have to be installed (only once) and are tracked in :installed_extensions
Agents only have to be loaded and are tracked in :enabled_agents

Agents might check that some Extensions are installed.


|#


(in-package :plugin-base)

(defclass plugin (ding:ding)
  ((thread-id :type integer
	      :accessor plugin-thread-id
	      :initform -1
	      :documentation "The id of the processing thread of this plugin (in combination with *trivial-monitored-thread*).")
   (thread-sleep-duration :type float
			  :initarg :thread-sleep-duration
			  :reader plugin-thread-sleep-duration
			  :initform 0.001f0
			  :documentation "")
   (thread-max-restarts :type integer
			:initarg :thread-max-restarts
			:reader plugin-thread-max-restarts
			:initform 0
			:documentation "The maximal number of times the processing thread of this plugin might be restarted before declaring it as dead (in combination with *trivial-monitored-thread*).")
   (queue :type chanl:bounded-channel
	  :reader plugin-queue
	  :initform (make-instance 'chanl:bounded-channel :size 5)
	  :documentation "A message queue to send objects for the plugin to process."))
  (:documentation "Plugins make it possible to extend the functionality of Marvin."))

(ding:register-ding-simplified "PluginBase" (find-class 'plugin-base:plugin))

(defgeneric analyze (plugin ding)
  (:documentation "This offers the opportunity for the plugin to decide if *DING* is relevant for it and should be processed further. Must return a *plugin-respose*."))

(defgeneric start-plugin (plugin)
  (:documentation "Initialize all internal plugin logic, threads and states, if necessary."))

(defgeneric stop-plugin (plugin)
  (:documentation "Shutdown all activities of the plugin and release resources."))

(defgeneric initialize-plugin (plugin)
  (:documentation ""))

(defgeneric install-plugin (plugin istream ostream)
  (:documentation "Execute all necessary installation steps before using this plugin for the first time (e.g. persistent objects 'Ding', dependencies etc.). The plugin must be ASDF loadable before calling this."))

(defclass plugin-response ()
  ((plugin :type (or ding:ding-reference plugin)
	   :reader plugin
	   :initarg :plugin
	   :documentation "The plugin which is responding.")
   (cost :type single-float
	 :reader cost
	 :initarg :cost
	 :documentation "The estimated cost of (future) processing.")
   (fit :type single-float
	:reader fit
	:initarg :fit
	:documentation "The estimated fit, indicating how well the analysed objects matches this plugins' purpose.")
   (resolution :type (member :possible :not-possible)
	       :reader resolution
	       :initarg :resolution
	       :documentation "Decision whether this plugin should be notified for further processing.")
   (args :type t
	 :reader args
	 :initarg :args
	 :initform nil
	 :documentation "Plugin specific data associated to the object. Useful to keep calculation results or states from the analyse step."))
  (:documentation "Defines an object containing information related to a plugin's analysis of a new object."))

(define-condition missing-plugin (error)
  ((plugin-name :initarg :name
		:reader plugin-name)))

(defun find-plugin (name &optional (base-class-name "PluginBase"))
  (declare (type string name base-class-name))
  (let* ((find-object-fn (uiop:find-symbol* "FIND-DING" :marvin nil))
	 (name-field (funcall find-object-fn '(:eigenschaft :?_ :graphie "Name")))
	 (wort-class (funcall find-object-fn `(:and (:eigenschaft :?1 :graphie "Wort")
						     (:eigenschaft :?_ ,name-field  :?1))))
	 (plugin-base (funcall find-object-fn `(:and (:eigenschaft :?1 :klasse ,wort-class)
						     (:eigenschaft :?1 :graphie ,base-class-name)
						     (:eigenschaft :?_ ,name-field :?1))))
	 (plugin-name (funcall find-object-fn `(:and (:eigenschaft :?_ :klasse ,wort-class)
						     (:eigenschaft :?_ :graphie ,name))))
	 (this (funcall find-object-fn `(:and (:eigenschaft :?_ :klasse ,plugin-base)
					      (:eigenschaft :?_ ,name-field ,plugin-name)))))

    (unless this
      (error 'missing-plugin :name plugin-name))

    (return-from find-plugin this)))

(defun find/create-plugin (name &optional (base-class-name "PluginBase"))
  (declare (type string name base-class-name))
  (let* ((find-object-fn (uiop:find-symbol* "FIND-DING" :marvin nil))
	 (name-field (funcall find-object-fn '(:eigenschaft :?_ :graphie "Name")))
	 (wort-class (funcall find-object-fn `(:and (:eigenschaft :?1 :graphie "Wort")
						     (:eigenschaft :?_ ,name-field  :?1))))
	 (plugin-base (funcall find-object-fn `(:and (:eigenschaft :?1 :klasse ,wort-class)
						     (:eigenschaft :?1 :graphie ,base-class-name)
						     (:eigenschaft :?_ ,name-field :?1))))
	 (plugin-name (funcall find-object-fn `(:and (:eigenschaft :?_ :klasse ,wort-class)
						     (:eigenschaft :?_ :graphie ,name))))
	 (this (funcall find-object-fn `(:and (:eigenschaft :?_ :klasse ,plugin-base)
					      (:eigenschaft :?_ ,name-field ,plugin-name)))))

    (unless this
      (assert name-field)
      (assert plugin-base)

      (let ((create-object-fn (uiop:find-symbol* "CREATE-OBJECT" :marvin nil)))
	(log:info "Creating the plugin '~a' because it does not exist yet." name)
	(unless plugin-name
	  (setf plugin-name (funcall create-object-fn `(:p-list (:graphie ,name) :klasse ,wort-class))))
	(setf this (funcall create-object-fn `(:p-list (,name-field ,plugin-name) :klasse ,plugin-base)))))

    (return-from find/create-plugin this)))

(defun define-method (method-definition package plugin-class-name name-root)
  (let ((method-keyword (car method-definition))
	(method-body (cdr method-definition)))
    (ecase method-keyword
      (:install-plugin
       `(defmethod ,(intern (symbol-name method-keyword) "PLUGIN-BASE")
	    ((,(intern "PLUGIN" package) (eql ,(intern name-root "KEYWORD"))) ,(intern "ISTREAM" package) ,(intern "OSTREAM" package))
	  (declare (ignorable ,(intern "PLUGIN" package) ,(intern "ISTREAM" package) ,(intern "OSTREAM" package)))
	  (log:info ,(concatenate 'string "Installing " plugin-class-name))
	  (let ((,(intern "THIS" package) nil)
		(,(intern "THIS-BASE" package) (plugin-base:find/create-plugin ,plugin-class-name)))
	      #+non-persistent-working-memory
	      (ding:register-ding #'(lambda ()
				      (values (ding:id (plugin-base:find-plugin ,plugin-class-name))
					      (find-class ',(intern (string-upcase plugin-class-name) package)))))

               (setf ,(intern "THIS" package)
                     (plugin-base:find/create-plugin ,(concatenate 'string name-root "-INSTANCE") ,plugin-class-name))

	       (trivial-utilities:aif (,(intern "FIND-DING" "MARVIN") `(:and (:eigenschaft :?name-field :graphie "Name")
									     (:eigenschaft :?name :graphie "Marvin")
									     (:eigenschaft :?_ :?name-field :?name)))
				      (pushnew (list ,(intern "THIS-BASE" package) 1.0f0)
					       (ding:eigenschaft ,(intern "IT" package) :associations)
					       :test #'(lambda (x y) (trivial-utilities:equals (car x) (car y))))
				      (log:error "Could not load the 'Marvin' Ding. No associations could be created; this will have a negative impact on this Agent."))

	       (pushnew (list ,(intern "THIS" package) 1.0f0)
					       (ding:eigenschaft ,(intern "THIS-BASE" package) :associations)
					       :test #'(lambda (x y) (trivial-utilities:equals (car x) (car y))))

	       (,(intern "RELEASE-OBJECT" "MARVIN") ,(intern "THIS-BASE" package))

	    (block ,(intern (symbol-name method-keyword) package)
	      ,@method-body)
	    ;; @REVIEW This is needed because the real class will only be declared in :initialize-plugin
	    ;;         But this also make it problematic when working in non-persistent mode!
	    (,(intern "RELEASE-OBJECT" "MARVIN") ,(intern "THIS" package))
	    (return-from ,(intern (symbol-name method-keyword) "PLUGIN-BASE") (values ())))))
      (:initialize-plugin
       `(defmethod ,(intern (symbol-name method-keyword) "PLUGIN-BASE")
	    ((,(intern "PLUGIN" package) (eql ,(intern name-root "KEYWORD"))))
	  (log:info ,(concatenate 'string "Initializing " plugin-class-name))
	  #-non-persistent-working-memory
	  (ding:register-ding #'(lambda ()
				  (let* ((,(intern "THIS" package) (plugin-base:find-plugin ,plugin-class-name))
					 (,(intern "THIS-ID" package) (ding:id ,(intern "THIS" package))))
				    (,(intern "RELEASE-OBJECT" "MARVIN") ,(intern "THIS" package))
				    (values ,(intern "THIS-ID" package) (find-class ',(intern (string-upcase plugin-class-name) package))))))
	  (block ,(intern (symbol-name method-keyword) package)
	    ,@method-body)
	  (return-from ,(intern (symbol-name method-keyword) "PLUGIN-BASE") (plugin-base:find-plugin ,(concatenate 'string name-root "-INSTANCE") ,plugin-class-name))))
      (:process
       `(defun ,(intern (symbol-name method-keyword) package)
	    (,(intern "PLUGIN" package))
	  (trivial-utilities:awhen (chanl:recv (plugin-base:plugin-queue ,(intern "PLUGIN" package)) :blockp nil)
	    (let ((,(intern "DING" package) (getf ,(intern "IT" package) :ding))
		  (,(intern "ARGUMENTS" package) (getf ,(intern "IT" package) :args)))
	      (declare (ignorable ,(intern "DING" package) ,(intern "ARGUMENTS" package)))
	      (block ,(intern (symbol-name method-keyword) package)
		,@method-body)))))
      (:start-plugin
       `(defmethod ,(intern (symbol-name method-keyword) "PLUGIN-BASE")
	    ((,(intern "PLUGIN" package) ,(intern (string-upcase plugin-class-name) package)))

	  (log:info ,(concatenate 'string "Starting " plugin-class-name))
	  (unless (eq -1 (plugin-base:plugin-thread-id ,(intern "PLUGIN" package)))
	    (log:warn "The plugin is already running! Stop it before trying to start.")
	    (return-from ,(intern (symbol-name method-keyword) "MARVIN") (values nil)))

	  (setf (plugin-base:plugin-thread-id ,(intern "PLUGIN" package))
		(trivial-monitored-thread:make-monitored-thread (:name ,(concatenate 'string name-root "-INSTANCE-THREAD")
								 :sleep-duration (plugin-base:plugin-thread-sleep-duration ,(intern "PLUGIN" package))
								 :max-restarts (plugin-base:plugin-thread-max-restarts ,(intern "PLUGIN" package)))
		  #'(lambda () (,(intern "PROCESS" package) ,(intern "PLUGIN" package)))))
	  (block ,(intern (symbol-name method-keyword) package)
	    ,@method-body)
	  (return-from ,(intern (symbol-name method-keyword) "PLUGIN-BASE") (values nil))))
      (:stop-plugin
       `(defmethod ,(intern (symbol-name method-keyword) "PLUGIN-BASE")
	    ((,(intern "PLUGIN" package) ,(intern (string-upcase plugin-class-name) package)))
	  (trivial-utilities:awhen (plugin-thread-id ,(intern "PLUGIN" package))
	    (trivial-monitored-thread:stop-thread ,(intern "IT" package))
	    (trivial-monitored-thread:join-thread ,(intern "IT" package))
	    (setf (plugin-thread-id ,(intern "PLUGIN" package)) -1))
	  (block ,(intern (symbol-name method-keyword) package)
	    ,@method-body)
	  (log:info ,(concatenate 'string "Stopped " plugin-class-name))
	  (return-from ,(intern (symbol-name method-keyword) "PLUGIN-BASE") (values nil))))
      (:analyze
       `(defmethod ,(intern (symbol-name method-keyword) "PLUGIN-BASE")
	    ((,(intern "PLUGIN" package) ,(intern (string-upcase plugin-class-name) package)) ,(intern "DING" package))
	  (block ,(intern (symbol-name method-keyword) package)
	    ,@method-body))))))

(defmacro define-plugin% (symb-str symb-ext package slot-defs initargs method-defs)
  `(let* ((name-root (subseq ,symb-str 0 ,symb-ext))
	  (plugin-class-name (string-upcase ,symb-str))
	  (method-definition-order '(:install-plugin :initialize-plugin :process :analyze :start-plugin :stop-plugin))
	  (ordered-method-definitions (mapcar #'(lambda (x) (cons x (cdar (member x ',method-defs :key #'car)))) method-definition-order)))

     `(progn
	(defclass ,(intern (string-upcase plugin-class-name) ,package) (plugin-base:plugin)
	  ,',slot-defs
	  ,(when ',initargs
	     `(:default-initargs ,@',initargs)))

	,@(mapcar #'(lambda (method-def)
		      (define-method method-def ,package plugin-class-name name-root))
		  ordered-method-definitions)

	(export ',(intern (string-upcase plugin-class-name) ,package))
	(values ()))))

(defmacro define-plugin (name slot-defs initargs method-defs)
  `(let* ((symb-str (string-upcase ,(string name)))
	  (symb-ext (search "-PLUGIN" symb-str))
	  (package (symbol-package (intern symb-str))))

     (unless symb-ext
       (error "The name of the plugin ~a *MUST* end with '-plugin'!" symb-str))

     (eval
      (define-plugin% symb-str symb-ext package ,slot-defs ,initargs ,method-defs))))

