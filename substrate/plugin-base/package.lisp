(uiop:define-package #:plugin-base
  (:documentation "")
  (:use #:common-lisp)
  (:export #:plugin
	   #:plugin-queue
	   #:plugin-thread-id
	   #:plugin-thread-sleep-duration
	   #:plugin-thread-max-restarts
	   #:install-plugin
	   #:initialize-plugin
	   #:analyze
	   #:start-plugin
	   #:stop-plugin
	   #:plugin-response
	   #:plugin
	   #:cost
	   #:fit
	   #:resolution
	   #:args
	   #:find-plugin
	   #:find/create-plugin
	   #:define-plugin))

