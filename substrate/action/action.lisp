;;;; Copyright (C) 2019 Eric Diethelm

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(in-package :action)


(defclass action (ding:ding)
  ((prerequisites :initarg :prerequisites
		  :type list)
   (func :initarg :func
	 :type trivial-action:action)))

(defun act-on (frame action)
  ;; Verify prerequisites
  
  )

(defmethod trivial-action:action-name ((obj action))
  (trivial-action:action-name (slot-value obj 'func)))

(defmethod trivial-action:action-documentation ((obj action))
  (trivial-action:action-documentation (slot-value obj 'func)))

(ding:register-ding-simplified "Action" (find-class 'action))

